<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('resize_img'))
{
    function resize_img($array){

     	$CI 							= get_instance();
		$configresize['image_library'] 	= 'gd2';
		$configresize['source_image'] 	= $array['source_image'];
		$configresize['new_image'] 		= $array['new_image'];
		$configresize['create_thumb'] 	= TRUE;
		$configresize['thumb_marker'] 	= "";
		$configresize['maintain_ratio'] = TRUE;
		$configresize['width']         	= $array['width'];
		$CI->load->library('image_lib');
		$CI->image_lib->initialize($configresize);
		if (!$CI->image_lib->resize())
		{
		   $notif_resize = ', Tetapi gagal resize ('.$CI->image_lib->display_errors('','').')';
		}else{
		   $notif_resize = 'Dan berhasil resize';
			   $CI->image_lib->clear();
		}
		return $notif_resize;
    }
}
