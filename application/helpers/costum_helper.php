<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('terbilang'))
{
	function terbilang ($angka) {
		$angka = (float)$angka;
		$bilangan = array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas');

		if ($angka < 12) {
			return $bilangan[$angka];
		} else if ($angka < 20) {
			return $bilangan[$angka - 10] . ' Belas';
		} else if ($angka < 100) {
			$hasil_bagi = (int)($angka / 10);
			$hasil_mod = $angka % 10;
			return trim(sprintf('%s Puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
		} else if ($angka < 200) { 
			return sprintf('Seratus %s', terbilang($angka - 100));
		} else if ($angka < 1000) { 
			$hasil_bagi = (int)($angka / 100); $hasil_mod = $angka % 100; return trim(sprintf('%s Ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
		} else if ($angka < 2000) { return trim(sprintf('Seribu %s', terbilang($angka - 1000)));
		} else if ($angka < 1000000) { 
			$hasil_bagi = (int)($angka / 1000); $hasil_mod = $angka % 1000; return sprintf('%s Ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
		} else if ($angka < 1000000000) { 
			$hasil_bagi = (int)($angka / 1000000); $hasil_mod = $angka % 1000000; return trim(sprintf('%s Juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else if ($angka < 1000000000000) { 
			$hasil_bagi = (int)($angka / 1000000000); $hasil_mod = fmod($angka, 1000000000); return trim(sprintf('%s Milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else if ($angka < 1000000000000000) { 
			$hasil_bagi = $angka / 1000000000000; $hasil_mod = fmod($angka, 1000000000000); return trim(sprintf('%s Triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else {
		return 'Data Salah';
		}
	}
}

if ( ! function_exists('cek_hargakhusus'))
{
	function cek_hargakhusus($tgl, $id_paket, $r){
		$CI = get_instance();
		$CI->load->model('page_model');
		$q = $CI->page_model->hargakhusus($tgl, $id_paket);
		if(count($q) > 0){
			$q->id_jadwal 		= $r->id_jadwal;
			$q->kuota 			= $r->kuota;
			$q->jumlah_jamaah 	= $r->jumlah_jamaah;
			$q->hargakhusus = 1;
			return $q;
		}else{
			return $r;
		}
	}
}


if ( ! function_exists('img_url'))
{
    function img_url($url = null){
    $CI = get_instance();
    if($CI->input->ip_address() == "127.0.0.1" || $CI->input->ip_address() == "::1"){
      $img_url = 'https://img.wahanahajiumrah.com/';
      // $img_url = 'http://manage.wahana.dev/gudang/upload/';
    }else{
      $img_url = "https://img.wahanahajiumrah.com/";
    }
      return $img_url.@$url;
    }
}

if ( ! function_exists('romawi'))
{
	function romawi() {
	    $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
	    $bulan = $array_bulan[date('n')];

	    return "$bulan";
	}
}

if ( ! function_exists('titiknol'))
{
function titiknol($a) {

	$q = $a.".0";
     return $q;

}
}


if ( ! function_exists('api_youtube'))
{
    function api_youtube($id_video){
			/*$opts = array(
				 "ssl"=>array(
				        "verify_peer"=>false,
				        "verify_peer_name"=>false
				    ),
			  'http'=>array(
			    'method'=>"GET")
			);
			$arrContextOptions = stream_context_create($opts); 

    	$str = file_get_contents('https://www.googleapis.com/youtube/v3/videos?id='.$id_video.'&key=AIzaSyDOyCZi1hZeUPS4qef533wE0Eae5LIoYYk&part=snippet,statistics&fields=items(id,snippet,statistics)', false, $arrContextOptions);
    	$json = json_decode($str, true);
    	return $json;*/
		$url = 'https://www.googleapis.com/youtube/v3/videos?id='.$id_video.'&key=AIzaSyDOyCZi1hZeUPS4qef533wE0Eae5LIoYYk&part=snippet,statistics&fields=items(id,snippet,statistics)';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response);
		return objToArray($response_a);
    }
}
 
if ( ! function_exists('objToArray'))
{
    function objToArray($obj, &$arr = null){
    	 if(!is_object($obj) && !is_array($obj)){
        $arr = $obj;
        return $arr;
    }

    foreach ($obj as $key => $value)
    {
        if (!empty($value))
        {
            $arr[$key] = array();
            objToArray($value, $arr[$key]);
        }
        else
        {
            $arr[$key] = $value;
        }
    }
    return $arr;
    }
}
 
if ( ! function_exists('api_sms'))
{
    function api_sms($hp,$sms_message){
		$url = 'http://103.16.199.187/masking/send.php?username=wahana1&password=wahana123&hp='.$hp.'&message='.urlencode($sms_message);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response);
		return $response_a;
    }
}


if ( ! function_exists('gambar_duta'))
{
function gambar_duta($a) {

	// $url = "http://duta.wahana.dev/gudang/images/".$a;
	$url = "http://duta.wahana.dev/gudang/images/".$a;
     return $url;

}
}

if ( ! function_exists('cleanData'))
{
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
}
if ( ! function_exists('cek_gambar'))
{
function cek_gambar($img) {
        if($img == ""){
        	$img = base_url('assets/upload/default.jpg');
        }else{
        	$img = base_url('assets/upload/'.$img);
        }
        return $img;
}
}
if ( ! function_exists('formattanggal'))
{
function formattanggal($a) {
        @$pecah = explode('/', $a);
        @$a = $pecah[2].'-'.$pecah[1].'-'.$pecah[0];
     return @$a;

}
}

if ( ! function_exists('format_rupiah'))
{
    function format_rupiah($angka){
      $rupiah= @number_format($angka,0,',',',').".00";
      return 'Rp. '.$rupiah;
      //var_dump($rupiah);
    }
}

if ( ! function_exists('rupiah'))
{
     function rupiah($angka){
      $rupiah= @number_format($angka,0,',','.').".-";
      return 'Rp. '.$rupiah;
      //var_dump($rupiah);
    }
}
if ( ! function_exists('rp'))
{
     function rp($angka){
      $rupiah= @number_format($angka,0,',','.');
      return $rupiah;
      //var_dump($rupiah);
    }
}
if ( ! function_exists('excelDateToDate'))
{
	function excelDateToDate($readDate){
	    $phpexcepDate = $readDate-25569; //to offset to Unix epoch
	    $x = strtotime("+$phpexcepDate days", mktime(0,0,0,1,1,1970));
	    return  date("Y-m-d",$x);
	}
}

if ( ! function_exists('tgl_indo'))
{
	function tgl_indo($tgl)
	{
		$ubah = gmdate(@$tgl, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tanggal = @$pecah[2];
		$bulan = bulan(@$pecah[1]);
		$tahun = @$pecah[0];
		return $tanggal.' '.$bulan.' '.$tahun;
	}
}


if ( ! function_exists('bulan'))
{
	function bulan($bln)
	{
		switch ($bln)
		{
			case 1:
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}
}

function makeInt($angka)

{

if ($angka < -0.0000001)

{

return ceil($angka-0.0000001);

}

else

{

return floor($angka+0.0000001);

}

}

function tgl_arab($tanggal)

{

$array_bulan = array("Muharram", "Safar", "Rabiul Awwal", "Rabiul Akhir",

"Jumadil Awwal","Jumadil Akhir", "Rajab", "Sya’ban",

"Ramadhan","Syawwal", "Zulqaidah", "Zulhijjah");

$date = makeInt(substr($tanggal,8,2));

$month = makeInt(substr($tanggal,5,2));

$year = makeInt(substr($tanggal,0,4));

if (($year>1582)||(($year == "1582") && ($month > 10))||(($year == "1582") && ($month=="10")&&($date >14)))

{

$jd = makeInt((1461*($year+4800+makeInt(($month-14)/12)))/4)+

makeInt((367*($month-2-12*(makeInt(($month-14)/12))))/12)-

makeInt( (3*(makeInt(($year+4900+makeInt(($month-14)/12))/100))) /4)+

$date-32075;

}

else

{

$jd = 367*$year-makeInt((7*($year+5001+makeInt(($month-9)/7)))/4)+

makeInt((275*$month)/9)+$date+1729777;

}

$wd = $jd%7;

$l = $jd-1948440+10632;

$n=makeInt(($l-1)/10631);

$l=$l-10631*$n+354;

$z=(makeInt((10985-$l)/5316))*(makeInt((50*$l)/17719))+(makeInt($l/5670))*(makeInt((43*$l)/15238));

$l=$l-(makeInt((30-$z)/15))*(makeInt((17719*$z)/50))-(makeInt($z/16))*(makeInt((15238*$z)/43))+29;

$m=makeInt((24*$l)/709);

$d=$l-makeInt((709*$m)/24);

$y=30*$n+$z-30;

$g = $m-1;

$final = "$d $array_bulan[$g] $y H";

return $final;

}
function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i') {
    if (trim ($timestamp) == '')
    {
            $timestamp = time ();
    }
    elseif (!ctype_digit ($timestamp))
    {
        $timestamp = strtotime ($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace ("/S/", "", $date_format);
    $pattern = array (
        '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
        '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
        '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
        '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
        '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
        '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
        '/April/','/June/','/July/','/August/','/September/','/October/',
        '/November/','/December/',
    );
    $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
        'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
        'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
        'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
        'Oktober','November','Desember',
    );
    $date = date ($date_format, $timestamp);
    $date = preg_replace ($pattern, $replace, $date);
    $date = "{$date}";
    return $date;
} 

if ( ! function_exists('formatParserTgl')){
	function formatParserTgl($tgl){
	        $pecah         			= explode("-", $tgl);
	        $tanggal_format			= @$pecah[2]."-".@$pecah[1]."-".@$pecah[0];
	        return $tanggal_format;
	}
}
