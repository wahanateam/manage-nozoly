<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

		public function login($value='')
		{
			$pass = md5(sha1(@$value['password']));
			$this->db->where('username', @$value['username']);
			$this->db->where('password', $pass);
			$query = $this->db->join('tb_level','tb_level.id_level = tb_admin.id_level','left')
							  ->get('tb_admin');
			return array('data' => $query->result(),
						 'num'	=> $query->num_rows());
		}

}

/* End of file auth_model.php */
/* Location: ./application/models/auth_model.php */