<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

   var $table = 'tampil_admin';
   var $column_search = array('nama'); //set column field
   var $column_order = array(null,'nama','username','nama_level','status',null);
    private function _get_datatables_query()
    {
        $this->db->from($this->table);
 
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_admin','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function get_delete($id)
    {
        $query = $this->db->where('id_admin',$id)
                          ->get('tb_admin');
        
        $result = $query->row();

        return $result;
    }
	public function get_id($id)
	{

		$query = $this->db->where('id_admin',$id)
					      ->select('id_admin,nama,username,status,gambar,id_level,id_jamaah,id_duta')
						  ->get('tampil_admin');
		
		$result = $query->row_array();
        
        unset($result['gambar']);
		// $result['gambar'] = base_url('gudang/upload/admin/'.$query->row_array()['gambar']);
  //       $result['gambar_asli'] = $query->row_array()['gambar'];
        $tes = file_exists(FCPATH."gudang/upload/admin/".$query->row_array()['gambar']);
        
        if ($tes == 1) {
            $result['gambar'] = base_url('gudang/upload/admin/'.$query->row_array()['gambar']);
            // $result['gambar'] = "AYA.jpg";
        }else{
            $result['gambar'] = base_url('gudang/upload/no_image.jpg');
            // $result['gambar'] = "TEU_AYA.jpg";
        }

        return $result;
	}
	public function get_id_input($data)
	{
		$query = $this->db->where('id_admin',$data['id_admin'])
						  ->get('tb_admin');
		
		return $query->row();
	}
    public function get_level()
    {
        $query = $this->db->get('tb_level');
        return $query->result();
    }
    public function get_jamaah()
    {
        $query = $this->db->order_by('nama_lengkap','asc')->get('tb_jamaah');
        return $query->result();
    }
    public function get_duta()
    {
        $query = $this->db->order_by('nama_lengkap','asc')->get('tb_duta');
        return $query->result();
    }
	public function input($data,$gambar = NULL,$notif = NULL)
	{
		if (@$gambar != NULL) {
			$datagambar = array('gambar' => @$gambar );
		}
		else{
			
		}

		$datainput = array('id_level'   => $data['id_level'],
                           'nama' 		=> $data['nama'],
						   'username' 	=> $data['username'],
						   'password' 	=> md5(sha1($data['password'])),
						   'status' 	=> $data['status']);


        if ($gambar != NULL) {
        	$datamasuk = array_merge($datainput,$datagambar);
        }else{
        	$datamasuk = $datainput;
        }
		$query = $this->db->insert('tb_admin',$datamasuk);

		if ($query) {
        	return array('status'	=>	'1',
						 'messages'	=>	'Administrator Berhasil diinput '.$notif);
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Administrator Tidak Berhasil diinput '.$notif);
		}
	}
	public function update($data,$gambar = NULL,$notif = NULL)
	{
		if (@$gambar != NULL) {
			$datagambar = array('gambar' => @$gambar );
		}
		else{
			
		}

		if (@$data['password'] != "" || @$data['password'] != NULL) {
		$datainput['password']	= md5(sha1($data['password']));
		}
        $datainput['id_level']  = $data['id_level'];
        $datainput['id_duta']   = $data['id_duta'];
        $datainput['id_jamaah'] = $data['id_jamaah'];
		$datainput['nama'] 		= $data['nama'];
		$datainput['username']	= $data['username'];
		$datainput['status']	= $data['status'];


		$this->db->where('id_admin',$data['id_admin']);

        if ($gambar != NULL) {
        	$datamasuk = array_merge($datainput,$datagambar);
        }else{
        	$datamasuk = $datainput;
        }
        
		$query = $this->db->update('tb_admin',$datamasuk);


        $newdata = array(
        'id_admin'          => @$data['id_admin'],
        'nama'              => @$data['nama'],
        'username'          => @$data['username'],
        // 'gambar'            => @$gambar,
        'logged_in'         => TRUE);

        if (@$gambar != NULL) {
            $newdata['gambar'] = @$gambar;
        }


		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Administrator Berhasil diupdate '.$notif,
                         'url'      =>  base_url('page/master/admin'),
						 'session'	=>  $newdata);
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Administrator Tidak Berhasil diupdate '.$notif);
		}

	}
	public function delete($id)
	{
		$this->db->where('id_admin',$id);

		$query = $this->db->delete('tb_admin');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Administrator Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Administrator Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */