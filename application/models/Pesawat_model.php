<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesawat_model extends CI_Model {

   var $table = 'tb_pesawat';
   var $column_search = array('nama_pesawat'); //set column field
   var $column_order = array(null,'nama_pesawat',null,'keterangan',null);
   
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);

        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_pesawat','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function get_delete($id)
    {
        $query = $this->db->where('id_pesawat',$id)
                          ->get('tb_pesawat');
        
        return $query->row();
    }
	public function get_id($id)
	{
		$query = $this->db->where('id_pesawat',$id)
						  ->get('tb_pesawat');
		
		$result = $query->row_array();
        


        unset($result['logo']);
		$result['logo_edit'] = base_url('gudang/upload/pesawat/'.$query->row_array()['logo']);

        $tes = file_exists(FCPATH."gudang/upload/pesawat/".$query->row_array()['logo']);
        
        if ($tes == 1) {
            $result['logo'] = base_url('gudang/upload/pesawat/'.$query->row_array()['logo']);
            // $result['gambar'] = "AYA.jpg";
        }else{
            $result['logo'] = base_url('gudang/upload/no_image.jpg');
            // $result['gambar'] = "TEU_AYA.jpg";
        }
        return $result;
	}
    public function get_tambahan($id)
    {
        $query = $this->db->where('id_pesawat',$id)
                          ->get('tb_pesawat');
        
        $result = $query->row_array();

        return $result;
    }
	public function get_id_input($data)
	{
		$query = $this->db->where('id_pesawat',$data['id_pesawat'])
						  ->get('tb_pesawat');
		
		return $query->row();
	}
	public function input($data,$gambar = NULL,$logo= NULL,$notif = null)
	{
		if ($logo != NULL ) {
			$datagambar = array('logo' 	 => $logo );
		}

		$datainput = array('id_gambar_group' => $gambar,
                           'nama_pesawat' => $data['nama_pesawat'],
    					   'keterangan'   => $data['keterangan']);

        if ($logo != NULL) {
        	$datamasuk = array_merge($datainput,$datagambar);
        }else{
        	$datamasuk = $datainput;
        }


		$query = $this->db->insert('tb_pesawat',$datamasuk);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Pesawat Berhasil diinput '.$notif);
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Pesawat Tidak Berhasil diinput '.$notif);
		}
	}
	public function update($data,$logo= NULL,$notif= NULL)
	{

		$datainput = array('nama_pesawat'    => $data['nama_pesawat'],
    					   'keterangan'      => $data['keterangan']);

        if ($logo != NULL) {
            $datagambar = array('logo'   => $logo );
        	$datamasuk = array_merge($datainput,$datagambar);
        }else{
        	$datamasuk = $datainput;
        }


		$this->db->where('id_pesawat',$data['id_pesawat']);
		$query = $this->db->update('tb_pesawat',$datamasuk);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Pesawat Berhasil diupdate'.$notif);
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Pesawat Tidak Berhasil diupdate'.$notif);
		}
	}


	public function delete($id)
	{
		$this->db->where('id_pesawat',$id);

		$query = $this->db->delete('tb_pesawat');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Pesawat Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Pesawat Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Pesawat_model.php */
/* Location: ./application/models/Pesawat_model.php */