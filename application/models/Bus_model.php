<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bus_model extends CI_Model {

   var $table = 'tb_bus';
   var $column_search = array('nama_bus'); //set column field
   var $column_order = array(null,'nama_bus',null,'keterangan',null);
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_bus','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function get_delete($id)
    {
        $query = $this->db->where('id_bus',$id)
                          ->get('tb_bus');
        
        $result = $query->row();

        return $result;
    }
	public function get_id($id)
	{
		$query = $this->db->where('id_bus',$id)
						  ->get('tb_bus');
		$result = $query->row_array();
 
        return $result;
	}
	public function get_id_input($data)
	{
		$query = $this->db->where('id_bus',$data['id_bus'])
						  ->get('tb_bus');
		
		return $query->row();
	}
	public function input($data,$id_gambar_group = NULL,$notif = null)
	{
		$datainput = array('id_gambar_group' => $id_gambar_group,
                           'nama_bus'        => $data['nama_bus'],
    					   'keterangan'      => $data['keterangan']);
        
		$query = $this->db->insert('tb_bus',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Bus Berhasil diinput'.$notif);
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Bus Tidak Berhasil diinput'.$notif);
		}
	}
	public function update($data,$notif = null)
	{
		$datainput = array('nama_bus'    => $data['nama_bus'],
    					   'keterangan'  => $data['keterangan']);


		$this->db->where('id_bus',$data['id_bus']);
		$query = $this->db->update('tb_bus',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Bus Berhasil diupdate'.$notif);

		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Bus Tidak Berhasil diupdate'.$notif);
		}
	}
	public function delete($id)
	{
		$this->db->where('id_bus',$id);

		$query = $this->db->delete('tb_bus');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Bus Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Bus Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Bus_model.php */
/* Location: ./application/models/Bus_model.php */