<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gambar_model extends CI_Model {

	public function input_gambar_group($data)
	{
		$data 	= array('nama_group' => $data);
		$query 	= $this->db->insert('tb_gambar_group', $data);
		$id_gambar_group = $this->db->insert_id();

		return $id_gambar_group;
	}
	public function input_gambar($data,$id)
	{
		$datainput 	= array('id_gambar_group' => $id,
							'gambar' 		  => $data);
		
		$this->db->insert('tb_gambar', $datainput);

	}
    public function get_gambar_solo($id)
    {
        $query = $this->db->where('id_gambar_group',$id)
                          ->limit(1)
                          ->order_by('id_gambar','desc')
                          ->get('tb_gambar');
                          
        return $query->result();
    }
    public function get_gambar_by_id($id)
    {
        $query = $this->db->where('id_gambar_group',$id)
                             ->get('tb_gambar');
        return $query->result();
    }
	public function get_by_id_group($id)
	{
		$query = $this->db->where('id_gambar_group',$id)
					         ->get('tb_gambar');
		return $query->result();
	}
	public function get_delete($id)
	{
		$query = $this->db->where('id_gambar',$id)
					         ->get('tb_gambar');
		return $query->row();
	}
	public function delete_by_group($id)
	{
		$query = $this->db->where('id_gambar_group',$id)
					      ->delete('tb_gambar');

				 $this->db->where('id_gambar_group',$id)
					      ->delete('tb_gambar_group');
		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Gambar Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Gambar Tidak Berhasil dihapus.');
		}
	}
	public function delete($id)
	{
		$query = $this->db->where('id_gambar',$id)
					      ->delete('tb_gambar');
		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Gambar Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Gambar Tidak Berhasil dihapus.');
		}
	}


}

/* End of file Gambar_model.php */
/* Location: ./application/models/Gambar_model.php */