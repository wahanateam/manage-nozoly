<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perlengkapan_model extends CI_Model {

   var $table = 'tb_perlengkapan';
   var $column_search = array('nama_perlengkapan'); //set column field
   var $column_order = array(null,'nama_perlengkapan',null);
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_perlengkapan','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	public function get_id($id)
	{
		$query = $this->db->where('id_perlengkapan',$id)
						  ->get('tb_perlengkapan');
		
		return $query->row();
	}
	public function input($data)
	{
		$datainput = array(    'nama_perlengkapan'      => $data['nama_perlengkapan'],
                           'tas'                    => @$data['tas'] == NULL ? '0' : $data['tas'],
                           'koper_kabin'            => @$data['koper_kabin'] == NULL ? '0' : @$data['koper_kabin'],
                           'koper_bagasi'           => @$data['koper_bagasi'] == NULL ? '0' : @$data['koper_bagasi'],
                           'jaket'                  => @$data['jaket'] == NULL ? '0' : @$data['jaket'],
                           'baju_koko'              => @$data['baju_koko'] == NULL ? '0' : @$data['baju_koko'],
                           'ihrom'                  => @$data['ihrom'] == NULL ? '0' : @$data['ihrom'],
                           'abaya'                  => @$data['abaya'] == NULL ? '0' : @$data['abaya'],
                           'buku_doa'               => @$data['buku_doa'] == NULL ? '0' : @$data['buku_doa'],
                           'buku_panduan'           => @$data['buku_panduan'] == NULL ? '0' : @$data['buku_panduan'],
                           'buku_itinerary'         => @$data['buku_itinerary'] == NULL ? '0' : @$data['buku_itinerary']);

		$query = $this->db->insert('tb_perlengkapan',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Perlengkapan Berhasil diinput.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Perlengkapan Tidak Berhasil diinput.');
		}
	}
	public function update($data)
	{
        $datainput = array('nama_perlengkapan'      => $data['nama_perlengkapan'],
                           'tas'                    => @$data['tas'],
                           'koper_kabin'            => @$data['koper_kabin'],
                           'koper_bagasi'           => @$data['koper_bagasi'],
                           'jaket'                  => @$data['jaket'],
                           'baju_koko'              => @$data['baju_koko'],
                           'ihrom'                  => @$data['ihrom'],
                           'abaya'                  => @$data['abaya'],
                           'buku_doa'               => @$data['buku_doa'],
                           'buku_panduan'           => @$data['buku_panduan'],
                           'buku_itinerary'         => @$data['buku_itinerary']);

		$this->db->where('id_perlengkapan',$data['id_perlengkapan']);
		
		$query = $this->db->update('tb_perlengkapan',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Perlengkapan Berhasil diupdate.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Perlengkapan Tidak Berhasil diupdate.');
		}
	}
	public function delete($id)
	{
		$this->db->where('id_perlengkapan',$id);

		$query = $this->db->delete('tb_perlengkapan');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Perlengkapan Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Perlengkapan Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Perlengkapan_model.php */
/* Location: ./application/models/Perlengkapan_model.php */