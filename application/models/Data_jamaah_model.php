<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_jamaah_model extends CI_Model {

   var $table = 'tampil_jamaah';
   var $column_search = array('nama_lengkap','tanggal'); //set column field
   var $column_order = array(null,'nama_lengkap','tanggal',null);
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }elseif (@$_GET['tanggal']){
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like('tanggal', @$_GET['tanggal']);
                }
                else
                {
                    $this->db->or_like('tanggal', @$_GET['tanggal']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }elseif (@$_GET['nama_paket']){
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like('nama_paket', @$_GET['nama_paket']);
                }
                else
                {
                    $this->db->or_like('nama_paket', @$_GET['nama_paket']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }elseif (@$_GET['nama_paket'] && @$_GET['tanggal']){
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like(['nama_paket', @$_GET['nama_paket'],'tanggal', @$_GET['tanggal']]);
                }
                else
                {
                    $this->db->or_like(['nama_paket', @$_GET['nama_paket'],'tanggal', @$_GET['tanggal']]);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_jamaah','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	public function get_id($id)
	{
		$query = $this->db->where('id_jamaah',$id)
						  ->get('tb_jamaah');
		
		return $query->row();
	}
    public function get_edit_jadwal($id)
    {
        $query = $this->db->where('id_jamaah',$id)
                          ->get('tampil_jamaah');
        
        return $query->row();
    }

    public function get_jadwal($id)
    {
        $query = $this->db->where('id_paket',$id)
                          ->get('tampil_jadwal');
        
        return $query->result();
    }

    public function cek_prov($id)
    {
        $query = $this->db->where('id_kab',$id)
                          ->get('kabupaten');
        
        return $query->row();
    }

    public function get_history_pembayaran($id)
    {

        $query = $this->db->where('id_keberangkatan',$id)
                          ->get('tampil_history_pembayaran');
        
        return $query->result();
    }

    public function get_jadwalimport()
    {
        $query = $this->db->group_by('tanggal')
                          ->get('tb_jadwal');
        
        return $query->result();
    }
   public function get_paket()
    {
        $query = $this->db->select('id_paket,nama_paket,nama_type')->get('tb_paket');
        
        return $query->result();
    }
   public function get_provinsi()
    {
        $query = $this->db->get('provinsi');
        
        return $query->result();
    }
   public function get_travel()
    {
        $query = $this->db->get('tb_travel');
        
        return $query->result();
    }
    public function get_kabupaten($id)
    {
        if ($id == null || $id == '') {
            $query = $this->db->get('kabupaten');
        }else{
            $query = $this->db->where('id_prov',$id)
                          ->get('kabupaten');
        }
        
        
        return $query->result();
    }

    public function cek_diskon($id,$kode)
    {
        $query = $this->db->where('id_paket',$id)
                          ->where('kode',$kode)
                          ->get('tampil_diskon');
        
        return array('row'         => $query->row(), 
                     'num_rows'    => $query->num_rows());
    }

    public function get_cicilan($id)
    {
        $query = $this->db->order_by('tanggal','desc')
                         ->where('id_jamaah',$id)
                          ->get('tampil_cicilan');
        
        return array('data' => $query->row(),
                     'history' => $query->result());
    }

    public function usd()
    {
        $query = $this->db->get('tb_mata_uang');
        
        return $query->row();
    }
    public function get_dokumen($id)
    {
        $q = $this->db->where('id_jamaah',$id)->get('tb_dokumen')->row();
        if(@$q->id_jamaah){

            $query = $this->db->where('id_jamaah',$id)->get('tb_dokumen');
            $result = $query->row_array();

            unset($result['ktp']);
            unset($result['kk']);
            unset($result['passport']);
            unset($result['buku_kuning']);
            unset($result['akte_lahir']);
            unset($result['buku_nikah']);
            unset($result['foto']);

            $ktp = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['ktp']);
            $kk = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['kk']);
            $passport = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['passport']);
            $buku_kuning = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['buku_kuning']);
            $akte_lahir = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['akte_lahir']);
            $buku_nikah = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['buku_nikah']);
            $foto = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['foto']);
            $ktp_ortu = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['ktp_ortu']);
            $ijazah = file_exists(FCPATH."gudang/upload/dokumen/".$query->row_array()['ijazah']);
            
            if ($ktp == 1 ) {
                $result['ktp'] = base_url('gudang/upload/dokumen/'.$query->row_array()['ktp']);
            }else{
                $result['ktp'] = base_url('gudang/upload/no_image.jpg');
            }

            if ($kk == 1 ) {
                $result['kk'] = base_url('gudang/upload/dokumen/'.$query->row_array()['kk']);
            }else{
                $result['kk'] = base_url('gudang/upload/no_image.jpg');
            }

            if ($passport == 1 ) {
                $result['passport'] = base_url('gudang/upload/dokumen/'.$query->row_array()['passport']);
            }else{
                $result['passport'] = base_url('gudang/upload/no_image.jpg');
            }


            if ($buku_kuning == 1 ) {
                $result['buku_kuning'] = base_url('gudang/upload/dokumen/'.$query->row_array()['buku_kuning']);
            }else{
                $result['buku_kuning'] = base_url('gudang/upload/no_image.jpg');
            }

            if ($akte_lahir == 1 ) {
                $result['akte_lahir'] = base_url('gudang/upload/dokumen/'.$query->row_array()['akte_lahir']);
            }else{
                $result['akte_lahir'] = base_url('gudang/upload/no_image.jpg');
            }


            if ($buku_nikah == 1 ) {
                $result['buku_nikah'] = base_url('gudang/upload/dokumen/'.$query->row_array()['buku_nikah']);
            }else{
                $result['buku_nikah'] = base_url('gudang/upload/no_image.jpg');
            }

            if ($foto == 1 ) {
                $result['foto'] = base_url('gudang/upload/dokumen/'.$query->row_array()['foto']);
            }else{
                $result['foto'] = base_url('gudang/upload/no_image.jpg');
            }

            if ($ktp_ortu == 1 ) {
                $result['ktp_ortu'] = base_url('gudang/upload/dokumen/'.$query->row_array()['ktp_ortu']);
            }else{
                $result['ktp_ortu'] = base_url('gudang/upload/no_image.jpg');
            }

            if ($ijazah == 1 ) {
                $result['ijazah'] = base_url('gudang/upload/dokumen/'.$query->row_array()['ijazah']);
            }else{
                $result['ijazah'] = base_url('gudang/upload/no_image.jpg');
            }

            return $result;
        }else{
            $data = ["id_jamaah" => $id];
            $this->db->insert('tb_dokumen', $data);
        }
    }
    public function input($data)
    {
        $tanggal       = formatParserTgl(@$data['tanggal_lahir_pasangan']);
        $tanggallahir   = formatParserTgl(@$data['tanggal_lahir']);
        $masa_berlaku   = formatParserTgl(@$data['masa_berlaku']);

        $datainput         = array('id_jadwal'                  => @$data['id_jadwal'],
                                'id_kab'                        => @$data['id_kab'],
                                'id_order_detail'               => @$data['id_order_detail'],
                                'id_travel'                     => @$data['id_travel'],
                                'nama_lengkap'                  => @$data['nama_lengkap'],
                                'nama_panggilan'                => @$data['nama_panggilan'],
                                'nama_passport'                 => @$data['nama_passport'],
                                'nama_ayah'                     => @$data['nama_ayah'],
                                'jenis_kelamin'                 => @$data['jenis_kelamin'],
                                'no_passport'                   => @$data['no_passport'],
                                'tempat_lahir'                  => @$data['tempat_lahir'],
                                'tanggal_lahir'                 => @$tanggallahir,
                                'email'                         => @$data['email'],
                                'no_telp'                       => @$data['no_telp'],
                                'jenis_kartu_identitas'         => @$data['jenis_kartu_identitas'],
                                'nomor_kartu_identitas'         => @$data['nomor_kartu_identitas'],
                                'alamat_rumah'                  => @$data['alamat_rumah'],
                                'alamat_kantor'                 => @$data['alamat_kantor'],
                                'status_perkawinan'             => @$data['status_perkawinan'],
                                'kewarganegaraan'               => @$data['kewarganegaraan'],
                                'pendidikan_terakhir'           => @$data['pendidikan_terakhir'],
                                'jumlah_tanggungan'             => @$data['jumlah_tanggungan'],
                                'jenis_pekerjaan'               => @$data['jenis_pekerjaan'],
                                'status_pekerjaan'              => @$data['status_pekerjaan'],
                                'size_baju'                     => @$data['size_baju'],
                                'nama_kerabat'                  => @$data['nama_kerabat'],
                                'alamat_kerabat'                => @$data['alamat_kerabat'],
                                'no_hp_kerabat'                 => @$data['no_hp_kerabat'],
                                'email_kerabat'                 => @$data['email'],
                                'nama_pasangan'                 => @$data['nama_pasangan'],
                                'tempat_lahir_pasangan'         => @$data['tempat_lahir_pasangan'],
                                'tanggal_lahir_pasangan'        => @$tanggal,
                                'kewarganegaraan_pasangan'      => @$data['kewarganegaraan_pasangan'],
                                'jenis_kartu_pasangan'          => @$data['jenis_kartu_pasangan'],
                                'nomor_kartu_pasangan'          => @$data['nomor_kartu_pasangan'],
                                'alamat_pasangan'               => @$data['alamat_pasangan'], 
                                'tipe_paket'                    => @$data['tipe_paket'],
                                'keterangan_mahram'             => @$data['keterangan_mahram'],
                                'masa_berlaku'                  => @$masa_berlaku,
                                'status_keluarga'               => @$data['status_keluarga'],
                                'tempat_dikeluarkan'            => @$data['tempat_dikeluarkan']);
    
        $query = $this->db->insert('tb_jamaah',$datainput);
        $id_jamaah = $this->db->insert_id();

        if ($query) {

            $get_tagihan = $this->db->where('id_jadwal',@$data['id_jadwal'])->select(@$data['tipe_paket'].' as tagihan')->get('all_paket')->row();
    
            $data = ['id_jamaah'    => @$id_jamaah,
                     'id_jadwal'    => @$data['id_jadwal'],
                     'tipe_kamar'   => @$data['tipe_paket'],
                     'tagihan'      => @$get_tagihan->tagihan,
                     'status'       => 'Jamaah'
                    ];
                    
            $query  = $this->db->insert('tb_keberangkatan',$data);

            $datamasuk = array('id_jamaah' => $id_jamaah);
            $this->db->insert('tb_dokumen', $datamasuk);

            return array('status'       =>    '1',
                         'messages'     =>    'Jamaah Berhasil diinput.');
        }else{
            return array('status'       =>    '0',
                         'messages'     =>    'Jamaah Tidak Berhasil diinput.');
        }
    }
    public function update_status($id,$id_order_detail)
    {
        $datainput = array('status'     => '1');

        $this->db->where('id_jamaah',$id);      
        $query = $this->db->update('tb_jamaah',$datainput);

        $dataorder = array('status'     => 'Batal');

        $this->db->where('id_order_detail',$id_order_detail);      
        $query = $this->db->update('tb_order_detail',$dataorder);

        if ($query) {
            return array('status'   =>  '1',
                         'messages' =>  'Status Berhasil diupdate.');
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Status Tidak Berhasil diupdate.');
        }
    }
    
	public function update($data)
    {
        $tanggal       = formatParserTgl(@$data['tanggal_lahir_pasangan']);
        $tanggallahir   = formatParserTgl(@$data['tanggal_lahir']);
        $masa_berlaku   = formatParserTgl(@$data['masa_berlaku']);

        $datainput      = array('id_jadwal'                 => @$data['id_jadwal'],
                                'id_kab'                    => @$data['id_kab'],
                                'id_travel'                 => @$data['id_travel'],
                                'nama_lengkap'              => @$data['nama_lengkap'],
                                'nama_panggilan'            => @$data['nama_panggilan'],
                                'nama_passport'             => @$data['nama_passport'],
                                'nama_ayah'                 => @$data['nama_ayah'],
                                'jenis_kelamin'             => @$data['jenis_kelamin'],
                                'no_passport'               => @$data['no_passport'],
                                'tempat_lahir'              => @$data['tempat_lahir'],
                                'tanggal_lahir'             => @$tanggallahir,
                                'email'                     => @$data['email'],
                                'no_telp'                   => @$data['no_telp'],
                                'jenis_kartu_identitas'     => @$data['jenis_kartu_identitas'],
                                'nomor_kartu_identitas'     => @$data['nomor_kartu_identitas'],
                                'alamat_rumah'              => @$data['alamat_rumah'],
                                'alamat_kantor'             => @$data['alamat_kantor'],
                                'status_perkawinan'         => @$data['status_perkawinan'],
                                'kewarganegaraan'           => @$data['kewarganegaraan'],
                                'pendidikan_terakhir'       => @$data['pendidikan_terakhir'],
                                'jumlah_tanggungan'         => @$data['jumlah_tanggungan'],
                                'jenis_pekerjaan'           => @$data['jenis_pekerjaan'],
                                'status_pekerjaan'          => @$data['status_pekerjaan'],
                                'size_baju'                 => @$data['size_baju'],
                                'nama_kerabat'              => @$data['nama_kerabat'],
                                'alamat_kerabat'            => @$data['alamat_kerabat'],
                                'no_hp_kerabat'             => @$data['no_hp_kerabat'],
                                'email_kerabat'             => @$data['email'],
                                'nama_pasangan'             => @$data['nama_pasangan'],
                                'tempat_lahir_pasangan'     => @$data['tempat_lahir_pasangan'],
                                'tanggal_lahir_pasangan'    => @$tanggal,
                                'kewarganegaraan_pasangan'  => @$data['kewarganegaraan_pasangan'],
                                'jenis_kartu_pasangan'      => @$data['jenis_kartu_pasangan'],
                                'nomor_kartu_pasangan'      => @$data['nomor_kartu_pasangan'],
                                'alamat_pasangan'           => @$data['alamat_pasangan'],
                                'alamat_pasangan'           => @$data['alamat_pasangan'], 
                                'tipe_paket'                => @$data['tipe_paket'],
                                'keterangan_mahram'         => @$data['keterangan_mahram'],
                                'masa_berlaku'              => @$masa_berlaku,
                                'status_keluarga'           => @$data['status_keluarga'],
                                'tempat_dikeluarkan'        => @$data['tempat_dikeluarkan']);

        $this->db->where('id_jamaah',$data['id_jamaah']);
        $query = $this->db->update('tb_jamaah',$datainput);
        if ($query) {

            $get_tagihan = $this->db->where('id_jadwal',@$data['id_jadwal'])->select(@$data['tipe_paket'].' as tagihan')->get('all_paket')->row();

            $data_keberangkatan = ['id_jamaah'    => @$data['id_jamaah'],
                                   'tipe_kamar'   => @$data['tipe_paket'],
                                   'tagihan'      => @$get_tagihan->tagihan,
                                  ];
                    
            $query  = $this->db->where('id_jamaah',@$data['id_jamaah'])->update('tb_keberangkatan',$data_keberangkatan);


            return array('status'   =>  '1',
                         'messages' =>  'Data Jamaah Berhasil diupdate.');
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Data Jamaah Tidak Berhasil diupdate.');
        }
    }

    public function cek_jadwal($tgl,$nama)
    {
        $query = $this->db->where('tanggal',$tgl)
                          ->where('nama_paket',$nama)
                          ->get('tampil_jadwal');
        
        return array('row'         => $query->row(), 
                     'num_rows'    => $query->num_rows());
    }
    public function import_jamaah($data,$jadwal)
    {      
    $tipe_paket = ""; 
        for ($i=3; $i < count($data); $i++) { 
            $tanggal = @$data[$i]["H"];
            if (@$data[$i]["C"] != NULL) {

            $j = $this->db->where('tanggal',$jadwal)
                              ->where('nama_paket',@$data[$i]["AE"])
                              ->get('tampil_jadwal')->row();

                            if($data[$i]["AF"] != "" || $data[$i]["AF"] != null){
                                $tipe_paket = "DOUBLE";
                            }elseif($data[$i]["AG"] != "" || $data[$i]["AG"] != null){
                                $tipe_paket = "TRIPLE";
                            }elseif($data[$i]["AH"] != "" || $data[$i]["AH"] != null){
                                $tipe_paket = "QUAD";
                            }
            
            $datainput  = array(
                                "id_jadwal"                     => $j->id_jadwal,
                                "nama_lengkap"                  => @$data[$i]["C"],
                                "nama_passport"                 => @$data[$i]["C"],
                                "nama_panggilan"                => @$data[$i]["D"],
                                "nama_ayah"                     => @$data[$i]["E"],
                                "jenis_kelamin"                 => @$data[$i]["B"],
                                "no_passport"                   => @$data[$i]["Q"],
                                "masa_berlaku"                  => excelDateToDate(@$data[$i]["S"]),
                                "tempat_lahir"                  => @$data[$i]["G"],
                                "tanggal_lahir"                 => excelDateToDate(@$data[$i]["H"]),
                                "email"                         => @$data[$i]["O"],
                                "no_telp"                       => @$data[$i]["N"],
                                "status"                        => "0",
                                "jenis_kartu_identitas"         => null,
                                "alamat_rumah"                  => @$data[$i]["M"],
                                "alamat_kantor"                 => null,
                                "nomor_kartu_identitas"         => null,
                                "status_perkawinan"             => null,
                                "kewarganegaraan"               => "INDONESIA",
                                "pendidikan_terakhir"           => @$data[$i]["L"],
                                "jumlah_tanggungan"             => null,
                                "jenis_pekerjaan"               => @$data[$i]["K"],
                                "status_pekerjaan"              => null,
                                "size_baju"                     => @$data[$i]["AL"],
                                "nama_kerabat"                  => null,
                                "alamat_kerabat"                => null,
                                "no_hp_kerabat"                 => null,
                                "email_kerabat"                 => null,
                                "nama_pasangan"                 => null,
                                "tempat_lahir_pasangan"         => null,
                                "tanggal_lahir_pasangan"        => null,
                                "kewarganegaraan_pasangan"      => null,
                                "jenis_kartu_pasangan"          => null,
                                "nomor_kartu_pasangan"          => null,
                                "alamat_pasangan"               => null,
                                'tipe_paket'                    => $tipe_paket,
                                'keterangan_mahram'             => @$data[$i]["J"],
                                'tempat_dikeluarkan'            => @$data[$i]["R"]);
            $query      = $this->db->insert('tb_jamaah',$datainput);
            }
        }
        // print_r($data);
        // return $data;

    }

    public function update_status_pembayaran($data,$status)
    {
        $datainput = array("status" => $status);
        $this->db->where('id_order_detail',$data['id_order_detail']);
        $query = $this->db->update('tb_order_detail',$datainput);
    }
    public function update_jadwal($data)
    {
        $datainput      = array('id_jadwal'      => @$data['id_jadwal_baru']);

        $this->db->where('id_jamaah',$data['id_jamaah']);
        $query = $this->db->update('tb_keberangkatan',$datainput);

        if ($query) {
            $datainputx = array('id_jamaah'             => @$data['id_jamaah'],
                                'id_jadwal'             => @$data['id_jadwal_baru'],
                                'id_jadwal_sebelumnya'  => @$data['id_jadwal_sebelumnya'],
                                'tanggal'               => date('Y-m-d'));

            $query = $this->db->insert('tb_history_edit_jadwal',$datainputx);

            return array('status'   =>  '1',
                         'url'      =>  base_url()."page/jamaah_v2/data_jamaah/edit_jadwal/".$data['id_jamaah'],
                         'messages' =>  'Data Jamaah Berhasil diupdate.');
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Data Jamaah Tidak Berhasil diupdate.');
        }
    }
    public function input_dokumen($data,$field,$id)
    {
        $datainput      = array($field         => @$data);

        $this->db->where('id_jamaah',$id);
        $query = $this->db->update('tb_dokumen',$datainput);
        if ($query) {
            return array('status'   =>  '1',
                         'messages' =>  'Dokumen Berhasil diupdate.',
                         'url'      =>  base_url()."page/jamaah_v2/data_jamaah/dokumen/".$id);
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Dokumen Tidak Berhasil diupdate.');
        }
    }
    public function get_jadwal_jamaah()
    {
        $query = $this->db->group_by('tanggal')
                          ->order_by('tanggal','desc')
                          ->get('tampil_jamaah');
        
        return $query->result();
    }
    public function get_paket_jamaah()
    {
        $query = $this->db->group_by('nama_paket')
                          ->order_by('nama_paket','asc')
                          ->get('tampil_jamaah');
        
        return $query->result();
    }
    public function cek_cicilan($id)
    {
        $query  = $this->db->where('id_jamaah',$id)
                           ->get('tb_cicilan');

        return array('num_rows' => $query->num_rows(),
                     'result' => $query->result());
    }
    public function insert_cicilan($data)
    {
        if (@$data['id_duta'] == null || @$data['id_duta'] == "null" )  {

            $datainput = array('id_jamaah' => $data['id_jamaah'],
                               'nominal'   => clean($data['nominal_cicilan']),
                               'tanggal'   => date("Y-m-d"));

            $query = $this->db->insert('tb_cicilan',$datainput);

        }else{
            $tgl_skrg       = date('Y-m-d');
            $resultfee      = $this->db->where('id_duta',@$data['id_duta'])->get('tampil_fee_duta')->row();

            $resultpromo    = $this->db->where('id_level_duta',@$resultfee->id_level_duta)
                                       ->where('tgl_mulai_promo <=',$tgl_skrg)
                                       ->where('tgl_akhir_promo >=',$tgl_skrg)
                                       ->get('tb_promo');

            if ($resultpromo->num_rows() == 0) {

                        $datacicilan = array('id_jamaah'        => @$data['id_jamaah'],
                                             'id_duta'          => @$data['id_duta'],
                                             'nominal'          => clean($data['nominal_cicilan']),
                                             'tanggal'          => date('Y-m-d'),
                                             'persentase_fee'   => $resultfee->fee);
        
                     $query =   $this->db->insert('tb_cicilan',$datacicilan);

            }else{
                        $resultpromorow = $resultpromo->row();

                        $datacicilan = array('id_jamaah'        => @$data['id_jamaah'],
                                             'id_duta'          => @$data['id_duta'],
                                             'nominal'          => clean($data['nominal_cicilan']),
                                             'tanggal'          => date('Y-m-d'),
                                             'persentase_fee'   => $resultpromorow->fee_promo);
        
                     $query =   $this->db->insert('tb_cicilan',$datacicilan);
             }


        }

        if ($query) {
            return array('status'   =>  '1',
                         'messages' =>  'Cicilan Berhasil diinput.',
                         'url'      =>  base_url()."page/jamaah/data_jamaah/cicilan/".$data['id_jamaah']);
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Cicilan Tidak Berhasil diinput.');
        }
    }

    public function ubah_alumni()
    {

        $d          = date("Y-m-d");
        $datainput  = array('status' => 'Alumni');
        $q          = $this->db->where('tanggal <',$d)->get('tampil_jamaah')->result();

        foreach ($q as $r) {
            $query = $this->db->where('id_keberangkatan',$r->id_keberangkatan)
                              ->update('tb_keberangkatan',$datainput);
        }

        if (@$query) {
            return array('status'   =>  '1',
                         'messages' =>  'Ubah Alumni Berhasil.');
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Ubah Alumni Tidak Berhasil.');
        }

    }

    public function history_edit_jadwal($id)
    {
        $this->db->where('id_jamaah',$id);

        $query = $this->db->order_by('id_history_edit_jadwal','desc')
                          ->get('tampil_history_jadwal');
        return $query->result();
    }
	public function delete($id)
	{
		$this->db->where('id_jamaah',$id);

		$query = $this->db->delete('tb_keberangkatan');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Jamaah Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Jamaah Tidak Berhasil dihapus.');
		}
	}
    public function cekhistory($id)
    {
        $this->db->where('id_jamaah',$id);

        $query = $this->db->get('tb_history_edit_jadwal');
        return $query->num_rows();
    }
    public function cekdokumen($id)
    {
        $this->db->where('id_jamaah',$id);

        $query = $this->db->get('tb_dokumen');
        return $query->num_rows();
    }
    public function cekcicilan($id)
    {
        $this->db->where('id_jamaah',$id);

        $query = $this->db->get('tb_cicilan');
        return $query->num_rows();
    }
}

/* End of file Data_jamaah_model.php */
/* Location: ./application/models/Data_jamaah_model.php */  