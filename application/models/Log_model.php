<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends CI_Model {

   var $table = 'tampil_log';
   var $column_search = array('keterangan','nama'); //set column field
   var $column_order = array(null,'nama','keterangan','tanggal');
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_log','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	public function get_id($id)
	{
		$query = $this->db->where('id_log',$id)
						  ->get('tb_log');
		
		return $query->row();
	}

	public function input($id_admin,$keterangan,$status)
	{
        if (@$status != null) {
            $s = $status;
        }else{
            $s = "0";
        }

		$datainput = array('id_admin'   => $id_admin,
                           'keterangan' => $keterangan,
                           'tanggal'    => date('Y-m-d H:i:s'),
                           'status'     => $s
                       );

		$query = $this->db->insert('tb_log',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Log Berhasil diinput.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Log Tidak Berhasil diinput.');
		}
	}

	public function delete($id)
	{
		$this->db->where('id_log',$id);

		$query = $this->db->delete('tb_log');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Log Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Log Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Log_model.php */
/* Location: ./application/models/Log_model.php */