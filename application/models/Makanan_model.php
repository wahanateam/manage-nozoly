<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Makanan_model extends CI_Model {

   var $table = 'tb_makanan';
   var $column_search = array('makanan'); //set column field
   var $column_order = array(null,'makanan',null);
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);

        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_makanan','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	public function get_id($id)
	{
		$query = $this->db->where('id_makanan',$id)
						  ->get('tb_makanan');
		
		return $query->row_array();
	}
	public function input($data,$id_gambar_group = NULL,$notif = null)
	{
		$datainput = array('makanan' => $data['makanan'],
                           'id_gambar_group' => $id_gambar_group);

		$query = $this->db->insert('tb_makanan',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Makanan Berhasil diinput.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Makanan Tidak Berhasil diinput.');
		}
	}
	public function update($data,$notif = null)
	{
		$datainput = array('makanan' => $data['makanan']);

		$this->db->where('id_makanan',$data['id_makanan']);
		
		$query = $this->db->update('tb_makanan',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Makanan Berhasil diupdate.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Makanan Tidak Berhasil diupdate.');
		}
	}

	public function delete($id)
	{
		$this->db->where('id_makanan',$id);

		$query = $this->db->delete('tb_makanan');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Makanan Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Makanan Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Makanan_model.php */
/* Location: ./application/models/Makanan_model.php */