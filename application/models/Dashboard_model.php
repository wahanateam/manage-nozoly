<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

  
  private function get_submenu($id_submenu='',$id)
  {
      $query = $this->db
                    ->where('id_sub',$id_submenu)
                    ->where('id_level',$id)
                    ->order_by('urutan','ASC')
                    ->get('menu_admin');
      return $query->result_array();
  }
  public function cari_menu($id,$cari)
  {
      $query = $this->db
                    ->where('id_sub','0')
                    ->where('id_level',$id)
                    ->like('nama_menu',$cari)
                    ->order_by('urutan','ASC')
                    ->get('menu_admin');

      foreach ($query->result() as $r) {
        $array = array( "nama_menu"         => $r->nama_menu,
                        "judul_menu"        => $r->judul_menu,
                        "link"              => $r->link,
                        "icon"              => $r->icon,
                        "sub"               => $this->get_submenu($r->id_menu_admin,$id));
        $looping_data[] = (array) $array;     
      }
      return @$looping_data;
  }
  


  public function get_menu($id)
  {
      $query = $this->db
                    ->where('id_sub','0')
                    ->where('id_level',$id)
                    ->order_by('urutan','ASC')
                    ->get('menu_admin');

      foreach ($query->result() as $r) {
        $array = array( "nama_menu"         => $r->nama_menu,
                        "judul_menu"        => $r->judul_menu,
                        "link"              => $r->link,
                        "icon"              => $r->icon,
                        "sub"               => $this->get_submenu($r->id_menu_admin,$id));
        $looping_data[] = (array) $array;     
      }
      return @$looping_data;
  }
  public function check_akses_menu($path='',$id)
  {
    $query = $this->db
                      ->where('id_level',$id)
                      ->where("(link = '".$path."' or link_tambahan = '".$path."')")
                      ->get('menu_admin');
    return $query->num_rows();
  }
    public function get_log()
    {
        $query = $this->db->where("status",'1')
                          ->limit('8')
                          ->order_by('tanggal','desc')
                          ->get('tampil_log');

        return array('row' => $query->result(),'num_rows' => $query->num_rows() );
    }
}

/* End of file Dashboard_model.php */
/* Location: ./application/models/Dashboard_model.php */