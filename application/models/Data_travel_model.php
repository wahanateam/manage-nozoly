<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_travel_model extends CI_Model {

   var $table = 'tb_travel';
   var $column_search = array('nama_travel','alamat'); //set column field
   var $column_order = array(null,'nama_travel','alamat','status',null);
    private function _get_datatables_query()
    {
        $this->db->from($this->table);
 
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_travel','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function get_delete($id)
    {
        $query = $this->db->where('id_travel',$id)
                          ->get('tb_travel');
        
        $result = $query->row();

        return $result;
    }
	public function get_id($id)
	{

		$query = $this->db->where('id_travel',$id)
					      ->select('id_travel, nama_travel, alamat, status')
						  ->get('tb_travel');
		
		$result = $query->row_array();

        return $result;
	}
	public function get_id_input($data)
	{
		$query = $this->db->where('id_travel',$data['id_travel'])
						  ->get('tb_travel');
		
		return $query->row();
	}
    public function input($data)
    {
        $datainput = array('nama_travel' => $data['nama_travel'],
                           'alamat'      => $data['alamat'],
                           'status'      => $data['status']
                        );

        $query = $this->db->insert('tb_travel',$datainput);

        if ($query) {
            return array('status'   =>  '1',
                         'messages' =>  'Data Travel Berhasil diinput.');
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Data Travel Tidak Berhasil diinput.');
        }
    }
    public function update($data)
	{
        $datainput = array('nama_travel' => $data['nama_travel'],
                           'alamat'      => $data['alamat'],
                           'status'      => $data['status']
                        );

        $this->db->where('id_travel', $data['id_travel']);
        
        $query = $this->db->update('tb_travel',$datainput);

        if ($query) {
            return array('status'   =>  '1',
                         'messages' =>  'Data Travel Berhasil diupdate.');
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Data Travel Tidak Berhasil diupdate.');
        }
    }
	public function delete($id)
	{
		$this->db->where('id_travel',$id);

		$query = $this->db->delete('tb_travel');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Administrator Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Administrator Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */