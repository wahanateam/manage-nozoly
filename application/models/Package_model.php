<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package_model extends CI_Model {
   var $table = 'tb_paket';
   var $column_search = array('nama_paket'); //set column field
   var $column_order = array(null,'nama_paket',null);
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
        
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
 
        // $i = 0;
        //     foreach ($this->column_search as $item) // loop column
        // {
        //     if(@$_POST['search']['value']) // if datatable send POST for search
        //     {
                 
        //         if($i===0) // first loop
        //         {
        //             $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
        //             $this->db->like($item, @$_POST['search']['value']);
        //         }
        //         else
        //         {
        //             $this->db->or_like($item, @$_POST['search']['value']);
        //         }
 
        //         if(count($this->column_search) - 1 == $i) //last loop
        //             $this->db->group_end(); //close bracket
        //     }
        //     $i++;
        // }
         
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_paket','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


	public function get_bus()
	{
		$query = $this->db->get('select_bus');
		return $query->result();
	}
	public function get_bus_hasil($id = null)
	{
		$query = $this->db
					  ->where('id_bus',$id)
					  ->get('select_bus');
		return $query->row();
	}
	public function get_maskapai_hasil($id = null)
	{
		$query = $this->db
					  ->where('id_pesawat',$id)
					  ->get('select_pesawat');
		return $query->row();
	}
	public function get_pesawat()
	{
		$query = $this->db->get('select_pesawat');
		return $query->result();
	}


	public function get_hotel($id_kategori = null)
	{
		$this->db->where('id_kategori',$id_kategori);
		$query = $this->db->get('select_hotel');
		return $query->result();
	}
	public function get_all_hotel()
	{
		$query = $this->db->get('select_hotel');
		return $query->result();
	}	
	public function get_all_hotel_hasil($id,$daerah_hotel = null)
	{
		$this->db->where('id_hotel',$id);
		$query = $this->db->get('select_hotel');
		return $query->row();
	}
	
	public function get_hotel_hasil($id_kategori = null,$id,$daerah_hotel = null)
	{
		$this->db->where('id_kategori',$id_kategori);
		$this->db->where('id_hotel',$id);/*
		$this->db->select("id_hotel as id_hotel_$daerah_hotel,
						   nama_hotel as nama_hotel_$daerah_hotel,
						   gambar_hotel as gambar_hotel_$daerah_hotel");*/
		$query = $this->db->get('select_hotel');
		return $query->row();
	}

	public function get_makanan()
	{
		$query = $this->db->get('select_makanan');
		return $query->result();
	}

	public function get_makanan_hasil($id = null)
	{
		$query = $this->db->where('id_makanan',$id)
						  ->get('select_makanan');
		return $query->row();
	}



	public function get_kategori_hotel_tambahan()
	{
		$names = array('Turki', 'Dubai');
		$this->db->where('type','Hotel')
				 ->where_in('nama_kategori', $names);
		$query = $this->db->get('tb_kategori');
		return $query->result();
	}
	public function get_hotel_tambahan($id = null)
	{
		$this->db->where('id_kategori',$id);
		$query = $this->db->get('tb_hotel');
		return $query->result();
	}
	public function get_perlengkapan()
	{
		$query = $this->db->get('tb_perlengkapan');
		return $query->result();
	}
	public function get_perlengkapan_hasil($id = null)
	{
		$query = $this->db->where('id_perlengkapan',$id)->get('tb_perlengkapan');
		return $query->row();
	}
	public function get_hotel_tambahan_id($id = null)
	{
		$this->db->where('id_hotel',$id);
		$query = $this->db->get('tb_hotel');
		return $query->row();
	}
	public function get_hotel_mekah()
	{
		$this->db->where('id_kategori','1');
		$query = $this->db->get('tb_hotel');
		return $query->result();
	}
	public function get_hotel_jeddah()
	{
		$this->db->where('id_kategori','3');
		$query = $this->db->get('tb_hotel');
		return $query->result();
	}
	public function get_delete($id)
	{
		$query = $this->db->where('id_paket',$id)
						  ->get('tb_paket');
		
		$result = $query->row_array();
        
        unset($result['gambar']);
		$result['gambar'] = base_url('gudang/upload/paket/'.$query->row_array()['gambar']);

        return $result;
	}
	public function get_id($id)
	{
		$query = $this->db->where('id_paket',$id)
						  ->get('tb_paket');
		
		$result = $query->row_array();


        return $result;
	}
	public function get_id_input($data)
	{
		$query = $this->db->where('id_paket',$data['id_paket'])
						  ->get('tb_paket');
		
		return $query->row();
	}
	public function get_id_paket($id)
	{
		$query = $this->db->where('id_paket',$id)
						  ->get('tb_paket');
		
        return $query->result();
	}
	public function input($data)
	{
		$datainput = array('id_bus'				=> $data['id_bus'],
						   'id_perlengkapan'	=> $data['id_perlengkapan'],
						   'id_pesawat'			=> $data['id_pesawat'],
						   'id_hotel_makkah'	=> $data['id_hotel_makkah'],
						   'id_hotel_madinah'	=> $data['id_hotel_madinah'],
						   'id_hotel_jeddah'	=> @$data['id_hotel_jeddah'],
						   'id_hotel_tambahan'	=> @$data['id_hotel_tambahan'],
						   'id_makanan'			=> $data['id_makanan'],
						   'tahun' 				=> $data['tahun'],
						   'nama_paket' 		=> $data['nama_paket'],
						   'nama_type' 			=> $data['nama_type'],
						   'harga' 				=> trim( preg_replace( "/[^0-9a-z]+/i", "", $data['harga'])),
						   'biaya_tambahan' 	=> trim( preg_replace( "/[^0-9a-z]+/i", "", $data['biaya_tambahan'])),
						   'berangkat' 			=> $data['berangkat'],
						   'pulang' 			=> $data['pulang'],
						   'status_berangkat' 	=> $data['status_berangkat'],
						   'status_pulang' 		=> $data['status_pulang'],
						   'deskripsi' 			=> @$data['deskripsi'],
						   'itinerary' 			=> @$data['itinerary'],
						   'persyaratan'		=> @$data['persyaratan'],
						   'akomodasi' 			=> @$data['akomodasi'],
						   'fasilitas' 			=> @$data['fasilitas'],
						   'including' 			=> @$data['including'],
						   'info_tambahan' 		=> @$data['info'],
						   'double' 			=> 0,
						   'triple' 			=> trim( preg_replace( "/[^0-9a-z]+/i", "", $data['triple'])),
						   'quad' 				=> trim( preg_replace( "/[^0-9a-z]+/i", "", $data['quad'])),
							'tampilkan' 		=> $data['tampilkan']
						   );

		$query = $this->db->insert('tb_paket',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Paket Berhasil diinput.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Paket Tidak Berhasil diinput.');
		}
	}
	public function update($data,$gambar = NULL)
	{
		if (@$gambar != NULL) {
			$datagambar = array('gambar' => @$gambar );
		}
		else{
			
		}
		$datainput = array('id_bus'				=> $data['id_bus'],
						   'id_perlengkapan'			=> $data['id_perlengkapan'],
						   'id_pesawat'			=> $data['id_pesawat'],
						   'id_hotel_makkah'		=> $data['id_hotel_makkah'],
						   'id_hotel_madinah'	=> $data['id_hotel_madinah'],
						   'id_hotel_jeddah'	=> @$data['id_hotel_jeddah'],
						   'id_hotel_tambahan'	=> @$data['id_hotel_tambahan'],
						   'id_makanan'			=> $data['id_makanan'],
						   'tahun' 				=> $data['tahun'],
						   'nama_paket' 		=> $data['nama_paket'],
						   'nama_type' 			=> $data['nama_type'],
						   'harga' 				=> trim( preg_replace( "/[^0-9a-z]+/i", "", $data['harga'])),
						   // 'biaya_tambahan' 	=> trim( preg_replace( "/[^0-9a-z]+/i", "", $data['biaya_tambahan'])),
						   'berangkat' 			=> $data['berangkat'],
						   'pulang' 			=> $data['pulang'],
						   'status_berangkat' 	=> $data['status_berangkat'],
						   'status_pulang' 		=> $data['status_pulang'],
						   'deskripsi' 			=> @$data['deskripsi'],
						   'itinerary' 			=> @$data['itinerary'],
						   'persyaratan'		=> @$data['persyaratan'],
						   'akomodasi' 			=> @$data['akomodasi'],
						   'fasilitas' 			=> @$data['fasilitas'],
						   'including' 			=> @$data['including'],
						   'info_tambahan' 		=> @$data['info_tambahan'],
						   'double' 			=> 0,
						   'triple' 			=> trim( preg_replace( "/[^0-9a-z]+/i", "", $data['triple'])),
						   'quad' 				=> trim( preg_replace( "/[^0-9a-z]+/i", "", $data['quad'])),
							'tampilkan' 		=> $data['tampilkan']
						);

        if (@$gambar != NULL) {
        	$datamasuk = array_merge($datainput,$datagambar);
        }else{
        	$datamasuk = $datainput;
        }

		$this->db->where('id_paket',$data['id_paket']);
		$query = $this->db->update('tb_paket',$datamasuk);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Paket Berhasil diupdate.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Paket Tidak Berhasil diupdate.');
		}
	}
	public function delete($id)
	{
		$this->db->where('id_paket',$id);

		$query = $this->db->delete('tb_paket');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Paket Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Paket Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Paket_model.php */
/* Location: ./application/models/Paket_model.php */