<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level_model extends CI_Model {

   var $table = 'tb_level';
   var $column_search = array('nama_level'); //set column field
   var $column_order = array(null,'nama_level','status',null);
   
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_level','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	public function get_id($id)
	{
		$query = $this->db->where('id_level',$id)
						  ->get('tb_level');
		
		return $query->row_array();
	}
    private function get_submenu($id_submenu='')
    {
        $query = $this->db
                      ->where('id_sub',$id_submenu)
                      ->get('tb_menu_admin');
                      // ->where('id_level',$id)
        return $query->result_array();
    }

    // public function get_menu($id)
    // {
    //     $query = $this->db
    //                   ->where('id_sub','0')
    //                   ->where('id_level',$id)
    //                   ->get('menu_admin');

    //     foreach ($query->result() as $r) {
    //       $array = array( "nama_menu"         => $r->nama_menu,
    //                       "judul_menu"        => $r->judul_menu,
    //                       "link"              => $r->link,
    //                       "icon"              => $r->icon,
    //                       "sub"               => $this->get_submenu($r->id_menu_admin,$id));
    //       $looping_data[] = (array) $array;     
    //     }
    //     return $looping_data;
    // }

    public function get_menu()
    {
        $query = $this->db->where('id_sub','0')->get('tb_menu_admin');

        foreach ($query->result() as $r) {
          $array = array( "id_menu_admin"     => $r->id_menu_admin,
                          "nama_menu"         => $r->nama_menu,
                          "sub"               => $this->get_submenu($r->id_menu_admin));

          $looping_data[] = (array) $array;     
        }
       
        return $looping_data;
    }
    public function get_edit_menu($id)
    {
        $query = $this->db->where('id_level',$id)
                          ->get('edit_menu_admin');
        
        return $query->result();
    }
    public function cek_akses($id)
    {
        $query = $this->db->where('id_level',$id)
                           ->get('tb_akses');

        return $query->num_rows();
    }
    public function get_akses($id)
    {
        $query = $this->db->where('id_level',$id)
                           ->get('tb_akses');

        return $query->result();
    }
	public function input($data)
	{
		$datainput = array('nama_level' => $data['nama_level'],
    					   'status' => $data['status']);

		$query = $this->db->insert('tb_level',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Level Berhasil diinput.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Level Tidak Berhasil diinput.');
		}
	}
    public function input_akses($data)
    {
        foreach ($data['akses'] as $key => $value) {

            $datainput = array('akses'          => $value,
                               'id_level'       => $data['id_level'],
                               'id_menu_admin'  => $key);
             $query = $this->db->insert('tb_akses',$datainput);
        }

        if ($query) {
            return array('status'   =>  '1',
                         'messages' =>  'Akses Berhasil diinput.');
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Akses Tidak Berhasil diinput.');
        }
    }
    public function update_akses($data)
    {
        $this->db->where('id_level',$data['id_level'])
                  ->delete('tb_akses');
                  
        foreach ($data['akses'] as $key => $value) {
            $datainput = array('akses'          => $value,
                               'id_level'       => $data['id_level'],
                               'id_menu_admin'  => $key);

             $query = $this->db->insert('tb_akses',$datainput);
        }

        if ($query) {
            return array('status'   =>  '1',
                         'messages' =>  'Akses Berhasil diupdate.');
        }else{
            return array('status'   =>  '0',
                         'messages' =>  'Akses Tidak Berhasil diupdate.');
        }
    }
	public function update($data)
	{
		$datainput = array('nama_level' => $data['nama_level'],
    					   'status' => $data['status']);

		$this->db->where('id_level',$data['id_level']);
		
		$query = $this->db->update('tb_level',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Level Berhasil diupdate.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Level Tidak Berhasil diupdate.');
		}
	}
	public function delete($id)
	{
		$this->db->where('id_level',$id);

		$query = $this->db->delete('tb_level');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Level Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Level Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Level_model.php */
/* Location: ./application/models/Level_model.php */