<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_admin_model extends CI_Model {

   var $table = 'menu_admin_setting';
   var $column_search = array('nama_menu'); //set column field
   var $column_order = array(null,null,'nama_menu','link','seo_link','status','type',null);

    private function _get_datatables_query($id_page= NULL)
    {
        $this->db->where('id_sub','0')->order_by('urutan','ASC')->from($this->table);
 
        $i = 0;
            foreach ($this->column_search as $item) // loop column
        {
            if(@$_GET['cari']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, @$_GET['cari']);
                }
                else
                {
                    $this->db->or_like($item, @$_GET['cari']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->order_by('id_menu_admin','desc')->get();
        return $query->result();
    }
   function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->where('id_sub','0')->from($this->table);
        return $this->db->count_all_results();
    }

	public function get_sub()
	{
		$query = $this->db->where('id_sub','0')

						  ->get('tb_menu_admin');
		return $query->result();
	}
	public function get_sub_by_id($id)
	{
		$query = $this->db->where('id_sub',$id)
						  ->order_by('urutan','asc')
						  ->get('tb_menu_admin');
		return $query->result();
	}
	public function get_id($id)
	{
		$query = $this->db->where('id_menu_admin',$id)
						  ->get('tb_menu_admin');
		
		return $query->row();
	}
	public function input($data)
	{
		$datainput = array('id_sub' 	=> @$data['id_sub'],
						   'nama_menu' 	=> @$data['nama_menu'],
						   'judul_menu' => @$data['judul_menu'],
						   'link' 		=> @$data['link'],
						   'icon' 		=> @$data['icon'],
						   'urutan'		=> @$data['urutan'],
						   'status' 	=> @$data['status']
						   );

		$query = $this->db->insert('tb_menu_admin',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Menu Berhasil diinput.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Menu Tidak Berhasil diinput.');
		}
	}
	public function update($data)
	{
		$datainput = array('id_sub' 	=> @$data['id_sub'],
						   'nama_menu' 	=> @$data['nama_menu'],
						   'judul_menu' => @$data['judul_menu'],
						   'link' 		=> @$data['link'],
						   'icon' 		=> @$data['icon'],
						   'urutan'		=> @$data['urutan'],
						   'status' 	=> @$data['status']
						   );

		$this->db->where('id_menu_admin',$data['id_menu_admin']);
		
		$query = $this->db->update('tb_menu_admin',$datainput);

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Menu Berhasil diupdate.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Menu Tidak Berhasil diupdate.');
		}
	}
	public function delete($id)
	{
		$this->db->where('id_menu_admin',$id);

		$query = $this->db->delete('tb_menu_admin');

		if ($query) {
			return array('status'	=>	'1',
						 'messages'	=>	'Menu Berhasil dihapus.');
		}else{
			return array('status'	=>	'0',
						 'messages'	=>	'Menu Tidak Berhasil dihapus.');
		}
	}
}

/* End of file Menu_model.php */
/* Location: ./application/models/Menu_model.php */