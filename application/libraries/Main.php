<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Main {

  
  function parser_cicilan($r,$d,$s,$dis = null)
  {
    if (@$r->hargakhusus) {
    $hargatambahkhusus = @$r->hargakhusus;
    }else{
    $hargatambahkhusus = @$r->harga;
    }
    $harga = ($hargatambahkhusus * $d->nominal) - @$dis->harga;
    $array = array(
    "id_jamaah"       => @$r->id_jamaah,
    "id_duta"         => @$r->id_duta,
    "id_order_detail" => @$r->id_order_detail,
    "id_cicilan"      => @$r->id_cicilan,
    "nama_lengkap"    => @$r->nama_lengkap,
    "nama_paket"      => @$r->nama_paket,
    "no_telp"      => @$r->no_telp,
    "email"      => @$r->email,
    "harga"           => rupiah(@$harga),
    "hargapaket"      => @$harga,
    "sisa"            => @$s);
    $looping_data = (object) $array;        

    return @$looping_data;
  }
  function parser_angsuran($r)
  {
    $array = array(
    "nama_lengkap"         => @$r->nama_lengkap,
    "pekerjaan"            => @$r->pekerjaan,
    "alamat"               => @$r->alamat,
    "no_telp"              => @$r->no_telp,
    "email"                => @$r->email,
    "kamar"                => @$r->kamar,
    "jumlah_jamaah"        => @$r->jumlah_jamaah,
    "bank_pembiayaan"      => @$r->bank_pembiayaan,
    "uang_muka"            => rupiah(@$r->uang_muka),
    "jumlah_pembiayaan"    => rupiah(@$r->jumlah_pembiayaan),
    "jangka_waktu"         => @$r->jangka_waktu,
    "angsuran_perbulan"    => rupiah(@$r->angsuran_perbulan),
    "biaya_administrasi"   => rupiah(@$r->biaya_administrasi),
    "setoran_awal"         => rupiah(@$r->setoran_awal),
    "tanggal_daftar"       => tgl_indo(@$r->tanggal_daftar),
    "nama_paket"           => @$r->nama_paket,
    "harga"                => rupiah(@$r->harga),
    );
    $looping_data = (object) $array;        

    return @$looping_data;
  }
  function parser_edit_order($r)
  {
    $array = array(
    "id_jamaah"             => @$r->id_jamaah,
    "id_duta"             => @$r->id_duta,
    "id_order"             => @$r->id_order,
    "nama_paket"            => @$r->nama_paket,
    "jumlah_orang"          => @$r->jumlah_orang,
    "tanggal"               => tgl_indo(@$r->tanggal),
    "tanggal_pulang"        => tgl_indo(@$r->tanggal_pulang),
    "kode_unik"             => @$r->kode_unik,
    "tanggal_booking"       => tgl_indo(@$r->tanggal_booking),
    "total_pembayaran"      => rupiah(@$r->total_pembayaran),
    "nama_lengkap"          => @$r->nama_lengkap,
    "email"                 => @$r->email,
    "no_telp"               => @$r->no_telp,
    "alamat"                => @$r->alamat,
    "kode"                  => @$r->kode,
    "hargadiskon"           => rupiah(@$r->hargadiskon),
    "biaya_tambahandiskon"  => rupiah(@$r->biaya_tambahandiskon),
    "id_diskon"             => @$r->id_diskon);
    $looping_data = (object) $array;        

    return @$looping_data;
  }
  function parser_edit_jamaah($r,$d = null)
  {
    $array = array(
     "id_order"               => @$r->id_order,
     "nama_lengkap"           => @$r->nama_lengkap,
     "jenis_kelamin"          => @$r->jenis_kelamin,
     "nama_ayah"              => @$r->nama_ayah,
     "tanggal_lahir"          => @$r->tanggal_lahir,
     "jenis_kamar"            => @$r->jenis_kamar,
     "id_order_detail"        => @$r->id_order_detail,
     "id_jadwal"              => @$r->id_jadwal,
     "id_paket"               => @$r->id_paket,
     "harga"                  => @$r->harga,
     "id_duta"                => @$r->id_duta,
     "hargadiskon"            => @$d->harga
    );
    $looping_data = (object) $array;        

    return @$looping_data;
  }
  function parser_jamaah($arrays)
  {
    foreach($arrays as $r){
    $array = array(
    "id_jamaah"                   =>$r->id_jamaah,
    "id_jadwal"                   =>$r->id_jadwal,
    "id_order_detail"             =>$r->id_order_detail,
    "nama_lengkap"                =>$r->nama_lengkap,
    "nama_passport"               =>$r->nama_passport,
    "nama_panggilan"              =>$r->nama_panggilan,
    "nama_ayah"                   =>$r->nama_ayah,
    "jenis_kelamin"               =>$r->jenis_kelamin,
    "no_passport"                 =>$r->no_passport,
    "masa_berlaku"                =>$r->masa_berlaku,
    "tempat_lahir"                =>$r->tempat_lahir,
    "tanggal_lahir"   => tgl_indo(@$r->tanggal_lahir),
    "email"                       =>$r->email,
    "no_telp"                     =>$r->no_telp,
    "status"                      =>$r->status,
    "jenis_kartu_identitas"       =>$r->jenis_kartu_identitas,
    "alamat_rumah"                =>$r->alamat_rumah,
    "alamat_kantor"               =>$r->alamat_kantor,
    "nomor_kartu_identitas"       =>$r->nomor_kartu_identitas,
    "status_perkawinan"           =>$r->status_perkawinan,
    "kewarganegaraan"             =>$r->kewarganegaraan,
    "pendidikan_terakhir"         =>$r->pendidikan_terakhir,
    "jumlah_tanggungan"           =>$r->jumlah_tanggungan,
    "jenis_pekerjaan"             =>$r->jenis_pekerjaan,
    "status_pekerjaan"            =>$r->status_pekerjaan,
    "size_baju"                   =>$r->size_baju,
    "nama_kerabat"                =>$r->nama_kerabat,
    "alamat_kerabat"              =>$r->alamat_kerabat,
    "no_hp_kerabat"               =>$r->no_hp_kerabat,
    "email_kerabat"               =>$r->email_kerabat,
    "nama_pasangan"               =>$r->nama_pasangan,
    "tempat_lahir_pasangan"       =>$r->tempat_lahir_pasangan,
    "tanggal_lahir_pasangan"      =>$r->tanggal_lahir_pasangan,
    "kewarganegaraan_pasangan"    =>$r->kewarganegaraan_pasangan,
    "jenis_kartu_pasangan"        =>$r->jenis_kartu_pasangan,
    "nomor_kartu_pasangan"        =>$r->nomor_kartu_pasangan,
    "alamat_pasangan"             =>$r->alamat_pasangan,

     "tanggal"         => tgl_indo(@$r->tanggal)
     
     
    );
    $looping_data[] = (object) $array;        
  }   

    return @$looping_data;
  }

  function parser_alumni($arrays)
  {
    foreach($arrays as $r){
      $pecah = explode(" ", tgl_indo(@$r->tanggal_lahir));
      $tanggal_ultah =  $pecah[0]." ".$pecah[1];

      $now = date('Y-m-d');
      $datetime1 = date_create(@$r->tanggal_lahir);
      $datetime2 = date_create($now);

      $interval = date_diff($datetime1, $datetime2);


      $array = array(
      "id_jamaah"              => @$r->id_jamaah,
      "nama_lengkap"           => @$r->nama_lengkap,
      "masa_berlaku"           => tgl_indo(@$r->masa_berlaku),
      "jenis_kelamin"          => @$r->jenis_kelamin,
      "nama_ayah"              => @$r->nama_ayah,
      "tanggal_lahir"          => tgl_indo(@$r->tanggal_lahir),
      "umur"                    =>  $interval->y+1,
      "tanggal_ultah"          => $tanggal_ultah,
      "jenis_kamar"            => @$r->jenis_kamar,
      "id_order_detail"        => @$r->id_order_detail,
      "id_jadwal"              => @$r->id_jadwal,
      "id_paket"               => @$r->id_paket,
      "tanggal"                => tgl_indo(@$r->tanggal),
    );
    $looping_data[] = (object) $array;        
  }
    return @$looping_data;


    $array = array(
     "id_order"               => @$r->id_order,
     "nama_lengkap"           => @$r->nama_lengkap,
     "jenis_kelamin"          => @$r->jenis_kelamin,
     "nama_ayah"              => @$r->nama_ayah,
     "tanggal_lahir"          => @$r->tanggal_lahir,
     "jenis_kamar"            => @$r->jenis_kamar,
     "id_order_detail"        => @$r->id_order_detail,
     "id_jadwal"              => @$r->id_jadwal,
     "id_paket"               => @$r->id_paket,
     "harga"                  => @$r->harga,
     "id_duta"                => @$r->id_duta,
     "hargadiskon"            => @$d->harga
    );
    $looping_data = (object) $array;        

    return @$looping_data;
  }
  // function parser_konfirmasi_order($arrays)
  // {
  //   foreach($arrays as $r){
  //   $array = array(
  //   "id_konfirmasi"          => @$r->id_konfirmasi,
  //   "id_order"               => @$r->id_order,
  //   "id_order_detail"        => @$r->id_order_detail,
  //   "nama"                   => @$r->nama,
  //   "nama_bank"              => @$r->nama_bank,
  //   "no_rekening"            => @$r->no_rekening,
  //   "nominal"                => @$r->nominal,
  //   "tanggal_konfirmasi"     => tgl_indo(@$r->tanggal_konfirmasi));
  //   $looping_data[] = (object) $array;        
  // }
  //   return @$looping_data;
  // }
  function parser_history_cicilan($arrays,$d)
  {
     foreach($arrays as $r){
    $harga = ($r->harga * $d->nominal);
    $array = array(
    "id_jamaah"      => @$r->id_jamaah,
    "id_cicilan"     => @$r->id_cicilan,
    "nama_lengkap"   => @$r->nama_lengkap,
    "nama_paket"     => @$r->nama_paket,
    "tanggal"        => tgl_indo(@$r->tanggal),
    "nominal"        => rupiah(@$r->nominal),
    "harga"          => rupiah(@$harga));
    $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

  function parser_jamaahduta($arrays)
  {
     foreach($arrays as $r){
    $array = array(
    "id_cicilan"      => @$r->id_cicilan,
    "id_duta"      => @$r->id_duta,
    "id_jamaah"      => @$r->id_jamaah,
    "nama_lengkap"     => @$r->nama_lengkap,
    "nominal"        => rupiah(@$r->jumlah_fee));
    $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

  function parser_log($arrays)
  {
     foreach($arrays as $r){
    $array = array(
    "id_admin"      => @$r->id_admin,
    "nama"          => @$r->nama,
    "keterangan"    => @$r->keterangan,
    "status"        => @$r->status,
    "tanggal"       => indonesian_date(@$r->tanggal));
    $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }


  function parser_edit_jadwal($r)
  {
    $array = array(
    "id_jamaah"               => @$r->id_jamaah,
    "id_jadwal"               => @$r->id_jadwal,
    "id_jadwal_sebelumnya"    => @$r->id_jadwal,
    "nama_paket"              => @$r->nama_paket,
    "tanggal"                 => tgl_indo(@$r->tanggal));

    $looping_data = (object) $array;        
    return @$looping_data;
  }
  function edit_jadwal_halaltour_parser($arrays)
  {
        foreach($arrays as $r){
        $pecah_tgl      = explode("-", $r->tanggal);   
        $pecah_tgl_plg  = explode("-", $r->tanggal_pulang);

        $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];
        $tanggal_pulang = $pecah_tgl_plg[2]."/".$pecah_tgl_plg[1]."/".$pecah_tgl_plg[0];


            // $r->gambar = base_url('gudang/upload/slider/'.$query->row_array()['gambar']);

        $tes = file_exists(FCPATH."gudang/upload/paket/".$r->gambar);
          if ($tes == true) {
            $gambar = base_url('gudang/upload/paket/'.$r->gambar.'');
          }else{
            $gambar = base_url('gudang/upload/no_image.jpg');
          }

        $array = array(
        // "id_jadwal"         => "qwe",
        "id_jadwal_halaltour"         => $r->id_jadwal_halaltour,
        "id_paket_halaltour"          => $r->id_paket_halaltour,
        "gambar"            => $gambar,
        "gambar_asli"            => $tes,
        "tanggal"           => $tanggal,
        "tanggal_pulang"    => $tanggal_pulang,
        "kuota"             => $r->kuota,
        "tampilkan"             => $r->tampilkan,
        "status"             => $r->status
        );
        $looping_data = (object) $array;        
      }
        return @$looping_data;
  }

  function parser_edit_agenda($r)
  {
    $pecah_tgl            = explode("-", $r->tanggal);   
    $pecah_tgl_reminder   = explode("-", $r->tanggal_reminder);

    $tanggal              = $pecah_tgl[2]."-".$pecah_tgl[1]."-".$pecah_tgl[0];
    $tanggal_reminder     = $pecah_tgl_reminder[2]."-".$pecah_tgl_reminder[1]."-".$pecah_tgl_reminder[0];

    $array = array(
    "id_agenda"           => @$r->id_agenda,
    "id_jamaah1"          => @$r->id_jamaah1,
    "id_jamaah2"          => @$r->id_jamaah2,
    "judul"               => @$r->judul,
    "type"                => @$r->type,
    "privasi"             => @$r->privasi,
    "tanggal"             => @$tanggal,
    "tanggal_reminder"    => @$tanggal_reminder,
    "keterangan"          => @$r->keterangan,
    "nama1"               => @$r->nama1,
    "nama2"               => @$r->nama2);

    $looping_data = (object) $array;        
    return @$looping_data;
  }

  function parser_jadwal($arrays)
  {
    foreach($arrays as $r){
    $array = array(
    "id_jadwal"               => @$r->id_jadwal,
    "nama_paket"              => @$r->nama_paket,
    "tanggal"                 => tgl_indo(@$r->tanggal),
    "tanggalval"              => @$r->tanggal
    );
    $looping_data[] = (object) $array;        
    };     
    return @$looping_data;
  }

  function parser_jadwal_by_paket($arrays)
  {
    foreach($arrays as $r){

          if ($r->status == "Aktif") {
            $color = "green";
          }else{
            $color = "red";
          }
    $array = array(
    "id_paket"            => @$r->id_paket,
    "id_jadwal"           => @$r->id_jadwal,
    "nama_paket"          => @$r->nama_paket,
    "tanggal"             => tgl_indo(@$r->tanggal),
    "tanggal_pulang"      => tgl_indo(@$r->tanggal_pulang),
    "status"              => @$r->status,
    "color"              => @$color,
    "logo"                => @$r->logo
    );
    $looping_data[] = (object) $array;        
    };     
    return @$looping_data;
  }



  function parser_history_jadwal($arrays)
  {
    
    foreach($arrays as $r){

                        $array = array(
                        "id_jamaah"                  => $r->id_jamaah,
                        "id_history_edit_jadwal"     => $r->id_history_edit_jadwal,
                        "id_jadwal"                  => $r->id_jadwal,
                        "id_jadwal_sebelumnya"       => $r->id_jadwal_sebelumnya,
                        "tanggal_sekarang"           => tgl_indo($r->tanggal_sekarang),
                        "nama_paket_sekarang"        => $r->nama_paket_sekarang,
                        "nama_paket_sebelumnya"      => $r->nama_paket_sebelumnya,
                        "tanggal_sebelumnya"         => tgl_indo($r->tanggal_sebelumnya),
                        "tanggal"                    => tgl_indo($r->tanggal),
                        );
                        $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }


  function parser_absen_rekap($arrays)
  {
    
    foreach($arrays as $r){
            $total_fee  = 0;
                        $total_fee += $r->fee_in + $r->fee_out;


                        $array = array(
                        "id_absen"            => $r->id_absen,
                        "id_admin"            => $r->id_admin,
                        "status_in"           => $r->status_in,
                        "status_out"          => $r->status_out,
                        "tanggal_in"          => $r->tanggal_in,
                        "tanggal_out"         => $r->tanggal_out,
                        "nama"                => $r->nama,
                        "fee_nmr"             => $r->total_fee_in + $r->total_fee_out,
                        "fee"                 => rupiah($r->total_fee_in + $r->total_fee_out),
                        "feetotal"            => ($r->total_fee_in + $r->total_fee_out)
                        );
                        $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

  function parser_absen($arrays)
  {
    
    foreach($arrays as $r){

                        $array = array(
                        "id_absen"            => $r->id_absen,
                        "id_admin"            => $r->id_admin,
                        "status_in"           => $r->status_in,
                        "status_out"          => $r->status_out,
                        "tanggal_in"          => $r->tanggal_in,
                        "tanggal_out"         => $r->tanggal_out,
                        "nama"                => $r->nama,
                        "fee_nmr"             => $r->fee_in + $r->fee_out,
                        "fee"                 => rupiah($r->fee_in + $r->fee_out),
                        "feetotal"                 => ($r->fee_in + $r->fee_out)
                        );
                        $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

  function parser_rekap($arrays)
  {
    
    foreach($arrays as $r){

                        $array = array(
                        "id_absen"            => $r->id_absen,
                        "id_admin"            => $r->id_admin,
                        "status_in"           => $r->status_in,
                        "status_out"          => $r->status_out,
                        "tanggal_in"          => $r->tanggal_in,
                        "tanggal_out"         => $r->tanggal_out,
                        "tanggal"             => bulan($r->bulan)." ".$r->tahun,
                        "nama"                => $r->nama,
                        "fee_nmr"             => $r->fee_in + $r->fee_out,
                        "fee"                 => rupiah($r->fee_in + $r->fee_out),
                        "feetotal"            => rupiah($r->total_fee_in + $r->total_fee_out)
                        );
                        $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

  function parser_analitik($arrays)
  {
    
    foreach($arrays as $r){

                        $array = array(
                        "id_analitik"        => $r->id_analitik,
                        "nama_analitik"      => $r->nama_analitik,
                        "hit"                => intval($r->hit),
                        "tanggal"            => tgl_indo($r->tanggal)
                        );
                        $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

  function parser_analitik_order($arrays)
  {
    
    foreach($arrays as $r){

                        $array = array(
                        "id_order_detail"    => $r->id_order_detail,
                        "id_order"           => $r->id_order,
                        "tanggal"            => $r->tanggal,
                        "status"             => $r->status,
                        "jumlah_orang"       => count($arrays),
                        );
                        $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

  function hargakhusus_parser($r)
  {
    $pecah_tgl      = explode("-", $r->tanggal);   

    $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];


    $array = array(
    // "id_jadwal"         => "qwe",
    "id_hargakhusus"    => $r->id_hargakhusus,
    "id_paket"          => $r->id_paket,
    "id_bus"            => $r->id_bus,
    "id_perlengkapan"   => $r->id_perlengkapan,
    "id_pesawat"        => $r->id_pesawat,
    "id_hotel_makkah"   => $r->id_hotel_makkah,
    "id_hotel_madinah"  => $r->id_hotel_madinah,
    "id_hotel_jeddah"   => $r->id_hotel_jeddah,
    "id_hotel_tambahan" => $r->id_hotel_tambahan,
    "id_makanan"        => $r->id_makanan,
    "tanggal"           => $tanggal,
    "harga"             => $r->harga,
    "triple"            => $r->triple,
    "quad"              => $r->quad,
    "biaya_tambahan"    => $r->biaya_tambahan,
    "deskripsi"         => $r->deskripsi,
    "itinerary"         => $r->itinerary,
    "persyaratan"       => $r->persyaratan,
    "akomodasi"         => $r->akomodasi,
    "fasilitas"         => $r->fasilitas,
    "including"         => $r->including,
    "info_tambahan"     => $r->info_tambahan

    );
    $looping_data = (object) $array;        
    return @$looping_data;
  }


  function jadwal_parser($arrays)
  {
    foreach($arrays as $r){
    $pecah_tgl      = explode("-", $r->tanggal);   
    $pecah_tgl_plg  = explode("-", $r->tanggal_pulang);

    $tanggal        = $pecah_tgl[2]."-".$pecah_tgl[1]."-".$pecah_tgl[0];
    $tanggal_pulang = $pecah_tgl_plg[2]."-".$pecah_tgl_plg[1]."-".$pecah_tgl_plg[0];


    $array = array(
    // "id_jadwal"         => "qwe",
    "id_jadwal"         => $r->id_jadwal,
    "id_paket"          => $r->id_paket,
    "tanggal"           => $tanggal,
    "tanggal_pulang"    => $tanggal_pulang
    );
    $looping_data = (object) $array;        
  }
    return @$looping_data;
  }

  function edit_diskon_parser($r)
  {
    $pecah_tgl      = explode("-", $r->tanggal_mulai);   
    $pecah_tgl_plg  = explode("-", $r->tanggal_akhir);

    $tanggal_mulai        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];
    $tanggal_akhir = $pecah_tgl_plg[2]."/".$pecah_tgl_plg[1]."/".$pecah_tgl_plg[0];


    $array = array(
    "id_paket"          => $r->id_paket ,
    "id_diskon"         => $r->id_diskon,
    "kode"              => $r->kode,
    "harga"             => $r->harga,
    "triple"            => $r->triple,
    "quad"              => $r->quad,
    "biaya_tambahan"    => $r->biaya_tambahan,
    "tanggal_mulai"     => $tanggal_mulai,
    "tanggal_akhir"     => $tanggal_akhir
    );
    $looping_data = (object) $array;        
    return @$looping_data;
  }


  function edit_jadwal_parser($arrays)
  {
    foreach($arrays as $r){
    $pecah_tgl      = explode("-", $r->tanggal);   
    $pecah_tgl_plg  = explode("-", $r->tanggal_pulang);

    $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];
    $tanggal_pulang = $pecah_tgl_plg[2]."/".$pecah_tgl_plg[1]."/".$pecah_tgl_plg[0];


        // $r->gambar = base_url('gudang/upload/slider/'.$query->row_array()['gambar']);

          $tes = file_exists(FCPATH."gudang/upload/paket/".$r->gambar);
            if ($tes == true) {
              $gambar = base_url('gudang/upload/paket/'.$r->gambar.'');
            }else{
              $gambar = base_url('gudang/upload/no_image.jpg');
            }

    $array = array(
    // "id_jadwal"         => "qwe",
    "id_jadwal"         => $r->id_jadwal,
    "id_paket"          => $r->id_paket,
    "gambar"            => $gambar,
    "gambar_asli"            => $tes,
    "tanggal"           => $tanggal,
    "tanggal_pulang"    => $tanggal_pulang,
    "kuota"             => $r->kuota,
    "tampilkan"             => $r->tampilkan
    );
    $looping_data = (object) $array;        
  }
    return @$looping_data;
  }

  function edit_jadwal_haji_parser($arrays)
  {
    foreach($arrays as $r){
    $pecah_tgl      = explode("-", $r->tanggal);   
    $pecah_tgl_plg  = explode("-", $r->tanggal_pulang);

    $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];
    $tanggal_pulang = $pecah_tgl_plg[2]."/".$pecah_tgl_plg[1]."/".$pecah_tgl_plg[0];


        // $r->gambar = base_url('gudang/upload/slider/'.$query->row_array()['gambar']);

          $tes = file_exists(FCPATH."gudang/upload/paket/".$r->gambar);
            if ($tes == true) {
              $gambar = base_url('gudang/upload/paket/'.$r->gambar.'');
            }else{
              $gambar = base_url('gudang/upload/no_image.jpg');
            }

    $array = array(
    // "id_jadwal"         => "qwe",
    "id_jadwal_haji"         => $r->id_jadwal_haji,
    "id_paket_haji"          => $r->id_paket_haji,
    "gambar"            => $gambar,
    "gambar_asli"            => $tes,
    "tanggal"           => $tanggal,
    "tanggal_pulang"    => $tanggal_pulang,
    "kuota"             => $r->kuota
    );
    $looping_data = (object) $array;        
  }
    return @$looping_data;
  }


  function album_parser($arrays)
  {
    foreach($arrays as $r){
    $pecah_tgl      = explode("-", $r->tanggal);   
    $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];

    $array = array(
    "id_album"         => $r->id_album,
    "nama_album"       => $r->nama_album,
    "tanggal"          => $tanggal
    );
    $looping_data = (object) $array;        
    }
    return @$looping_data;
  }
  function album_video_parser($arrays)
  {
    foreach($arrays as $r){
    $pecah_tgl      = explode("-", $r->tanggal);   
    $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];

    $array = array(
    "id_album_embed"   => $r->id_album_embed,
    "nama_album"       => $r->nama_album,
    "tanggal"          => $tanggal
    );
    $looping_data = (object) $array;        
    }
    return @$looping_data;
  }

  function bus_parser($arrays)
  {
    foreach($arrays as $r){
    $array = array(
    "id"         => $r->id_bus,
    "nama"       => $r->nama_bus,
    "foto"       => $r->gambar_bus,
    "type"       => "bus",
    "folder"     => "bus"
    );
    $looping_data[] = (object) $array;        
    }
    return @$looping_data;
  }

  function tanya_ustad_parser($arrays)
  {
    foreach($arrays as $r){

    $array = array(
    'id_pertanyaan'       => $r->id_pertanyaan,
    'id_sub_pertanyaan'   => $r->id_sub_pertanyaan,
    'nama_lengkap'        => $r->nama_lengkap,
    'email'               => $r->email,
    'komen'               => $r->komen,
    'ip'                  => $r->ip,
    'tanggal'             => $r->tanggal,
    'status'              => $r->status
    );
    $looping_data = (object) $array;        
    }
    return @$looping_data;
  }

  function perlengkapan_parser($arrays)
  {
    foreach($arrays as $r){
    $array = array(
    "id"         => $r->id_perlengkapan,
    "nama"       => $r->nama_perlengkapan,
    "foto"       => null,
    "type"       => "perlengkapan",
    "folder"     => "perlengkapan"
    );
    $looping_data[] = (object) $array;        
    }
    return @$looping_data;
  }

  function pesawat_parser($arrays)
  {
    foreach($arrays as $r){
    $array = array(
    "id"         => $r->id_pesawat,
    "nama"       => $r->nama_pesawat,
    "foto"       => $r->gambar_pesawat,
    "type"       => "pesawat",
    "folder"     => "pesawat"
    );
    $looping_data[] = (object) $array;        
    }
    return @$looping_data;
  }

  function makanan_parser($arrays)
  {
    foreach($arrays as $r){
    $array = array(
    "id"         => $r->id_makanan,
    "nama"       => $r->makanan,
    "foto"       => $r->gambar_makanan,
    "type"       => "makanan",
    "folder"     => "makanan"
    );
    $looping_data[] = (object) $array;        
    }
    return @$looping_data;
  }

  function hotel_parser($arrays,$daerah_hotel)
  {
    foreach($arrays as $r){
    $array = array(
    "id"         => $r->id_hotel,
    "nama"       => $r->nama_hotel,
    "foto"       => $r->gambar_hotel,
    "type"       => "hotel_".$daerah_hotel,
    "folder"     => "hotel"
    );
    $looping_data[] = (object) $array;        
    }
    return @$looping_data;
  }

  function allhotel_parser($arrays)
  {
    foreach($arrays as $r){
    $array = array(
    "id"         => $r->id_hotel,
    "nama"       => $r->nama_hotel,
    "foto"       => $r->gambar_hotel,
    "type"       => "hotel",
    "folder"     => "hotel"
    );
    $looping_data[] = (object) $array;        
    }
    return @$looping_data;
  }


  
    function order_parser($arrays)
  {
    foreach($arrays as $r){
    $pecah_tgl      = explode("-", $r->tanggal);   
    $pecah_tgl_plg  = explode("-", $r->tanggal_pulang);

    $tanggal        = $pecah_tgl[2]."-".$pecah_tgl[1]."-".$pecah_tgl[0];
    $tanggal_pulang = $pecah_tgl_plg[2]."-".$pecah_tgl_plg[1]."-".$pecah_tgl_plg[0];

    if ($r->status_berangkat == "1") {
      $status_berangkat = "Transit";
    }else{
      $status_berangkat = "Langsung";
    }

    if ($r->status_pulang == "1") {
      $status_pulang = "Transit";
    }else{
      $status_pulang = "Langsung";
    }



    $array = array(
    "id_jadwal"         => $r->id_jadwal,
    "id_paket"          => $r->id_paket,
    "id_order"          => $r->id_order,
    "nama_paket"        => $r->nama_paket,
    "harga"             => rupiah($r->harga),
    "nama_pesawat"      => $r->nama_pesawat,
    "double"            => $r->double,
    "triple"            => rupiah($r->harga - $r->triple),
    "quad"              => rupiah($r->harga - $r->quad),
    "total_double"      => rupiah($r->order_double * $r->harga),
    "total_triple"      => rupiah($r->order_triple * ($r->harga - $r->triple)),
    "total_quad"        => rupiah($r->order_quad * ($r->harga - $r->quad)),
    "total"             => rupiah(($r->order_double * $r->harga)+($r->order_triple * ($r->harga - $r->triple))+($r->order_quad * ($r->harga - $r->quad))),
    "pendaftaran"       => rupiah($r->biaya_tambahan),
    "order_double"      => $r->order_double,
    "order_triple"      => $r->order_triple,
    "order_quad"        => $r->order_quad,
    "nama_pesawat"      => $r->nama_pesawat,
    "nama_pesawat"      => $r->nama_pesawat,
    "nama_pesawat"      => $r->nama_pesawat,
    "berangkat"         => $r->berangkat,
    "pulang"            => $r->pulang,
    "status_berangkat"  => $status_berangkat,
    "status_pulang"     => $status_pulang,
    "tanggal"           => tgl_indo($r->tanggal),
    "tanggal_pulang"    => tgl_indo($r->tanggal_pulang),
    "nama_lengkap"      => $r->nama_lengkap,
    "email"             => $r->email,
    "no_telp"           => $r->no_telp,
    "alamat"            => $r->alamat,
    "tanggal_booking"   => tgl_indo($r->tanggal_booking)
    );
    $looping_data = (object) $array;        
  }
    return @$looping_data;
  }

    function mata_uang_parser($arrays)
  {
    foreach($arrays as $r){

    $array = array(
    "id_history_mata_uang"      => $r->id_history_mata_uang,
    "nominal"                   => $r->nominal,
    "tanggal"                   => tgl_indo($r->tanggal)
    );

    $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

    function order_dash_parser($arrays)
  {
    foreach($arrays as $r){

    $array = array(
    "id_order"              => $r->id_order,
    "nama_lengkap"          => $r->nama_lengkap,
    "tanggal_booking"       => tgl_indo($r->tanggal_booking)
    );

    $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }
    function jadwal_dash_parser($arrays)
  {
    foreach($arrays as $r){

    $array = array(
    "id_jadwal"      => $r->id_jadwal,
    "nama_paket"     => $r->nama_paket,
    "tanggal"        => tgl_indo($r->tanggal),
    "tanggal_pulang" => tgl_indo($r->tanggal_pulang)
    );

    $looping_data[] = (object) $array;        
  }
    return @$looping_data;
  }

  function edit_paket_parser($arrays,$id)
  {
    foreach($arrays as $r){

    $array = array(
        "id_kategori_hotel"         => $id,
    );

    $arraaaay = array_merge($array,$arrays);
    $looping_data = (object) $arraaaay;        
  }
    return @$looping_data;
  }
          
  }

// class Main {


//   function parser_cicilan($r,$d)
//   {
//     $harga = ($r->harga * $d->nominal);
//     $array = array(
//     "id_jamaah"      => @$r->id_jamaah,
//     "id_duta"        => @$r->id_duta,
//     "id_cicilan"     => @$r->id_cicilan,
//     "nama_lengkap"   => @$r->nama_lengkap,
//     "nama_paket"     => @$r->nama_paket,
//     "harga"          => rupiah(@$harga),
//     "hargapaket"     => @$harga);
//     $looping_data = (object) $array;        

//     return @$looping_data;
//   }

//   function parser_edit_order($r)
//   {
//     $array = array(
//     "id_jamaah"      => @$r->id_jamaah,
//     "nama_paket"     => @$r->nama_paket,
//     "jumlah_orang"     => @$r->jumlah_orang,
//     "tanggal"     => tgl_indo(@$r->tanggal),
//     "tanggal_pulang"     => tgl_indo(@$r->tanggal_pulang),
//     "kode_unik"     => @$r->kode_unik,
//     "tanggal_booking"     => tgl_indo(@$r->tanggal_booking),
//     "total_pembayaran"     => rupiah(@$r->total_pembayaran),
//     "nama_lengkap"     => @$r->nama_lengkap,
//     "email"     => @$r->email,
//     "no_telp"     => @$r->no_telp,
//     "alamat"     => @$r->alamat);
//     $looping_data = (object) $array;        

//     return @$looping_data;
//   }

//   function parser_history_cicilan($arrays,$d)
//   {
//      foreach($arrays as $r){
//     $harga = ($r->harga * $d->nominal);
//     $array = array(
//     "id_jamaah"      => @$r->id_jamaah,
//     "id_cicilan"     => @$r->id_cicilan,
//     "nama_lengkap"   => @$r->nama_lengkap,
//     "nama_paket"     => @$r->nama_paket,
//     "tanggal"        => tgl_indo(@$r->tanggal),
//     "nominal"        => rupiah(@$r->nominal),
//     "harga"          => rupiah(@$harga));
//     $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }
//   function parser_edit_jadwal($r)
//   {
//     $array = array(
//     "id_jamaah"               => @$r->id_jamaah,
//     "id_jadwal"               => @$r->id_jadwal,
//     "id_jadwal_sebelumnya"    => @$r->id_jadwal,
//     "nama_paket"              => @$r->nama_paket,
//     "tanggal"                 => tgl_indo(@$r->tanggal));

//     $looping_data = (object) $array;        
//     return @$looping_data;
//   }

//   function parser_history_jadwal($arrays)
//   {
    
//     foreach($arrays as $r){

//                         $array = array(
//                         "id_jamaah"                  => $r->id_jamaah,
//                         "id_history_edit_jadwal"     => $r->id_history_edit_jadwal,
//                         "id_jadwal"                  => $r->id_jadwal,
//                         "id_jadwal_sebelumnya"       => $r->id_jadwal_sebelumnya,
//                         "tanggal_sekarang"           => tgl_indo($r->tanggal_sekarang),
//                         "nama_paket_sekarang"        => $r->nama_paket_sekarang,
//                         "nama_paket_sebelumnya"      => $r->nama_paket_sebelumnya,
//                         "tanggal_sebelumnya"         => tgl_indo($r->tanggal_sebelumnya),
//                         "tanggal"                    => tgl_indo($r->tanggal),
//                         );
//                         $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }
//   function parser_absen($arrays)
//   {
    
//     foreach($arrays as $r){

//                         $array = array(
//                         "id_absen"            => $r->id_absen,
//                         "id_admin"            => $r->id_admin,
//                         "status_in"           => $r->status_in,
//                         "status_out"          => $r->status_out,
//                         "tanggal_in"          => $r->tanggal_in,
//                         "tanggal_out"         => $r->tanggal_out,
//                         "nama"         => $r->nama,
//                         "fee_nmr"             => $r->fee_in + $r->fee_out,
//                         "fee"                 => rupiah($r->fee_in + $r->fee_out),
//                         "feetotal"                 => ($r->fee_in + $r->fee_out)
//                         );
//                         $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }
//   function parser_rekap($arrays)
//   {
    
//     foreach($arrays as $r){

//                         $array = array(
//                         "id_absen"            => $r->id_absen,
//                         "id_admin"            => $r->id_admin,
//                         "status_in"           => $r->status_in,
//                         "status_out"          => $r->status_out,
//                         "tanggal_in"          => $r->tanggal_in,
//                         "tanggal_out"         => $r->tanggal_out,
//                         "tanggal"         => bulan($r->bulan)." ".$r->tahun,
//                         "nama"         => $r->nama,
//                         "fee_nmr"             => $r->fee_in + $r->fee_out,
//                         "fee"                 => rupiah($r->fee_in + $r->fee_out),
//                         "feetotal"                 => rupiah($r->total_fee_in + $r->total_fee_out)
//                         );
//                         $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }

//   function parser_analitik($arrays)
//   {
    
//     foreach($arrays as $r){

//                         $array = array(
//                         "id_analitik"        => $r->id_analitik,
//                         "nama_analitik"      => $r->nama_analitik,
//                         "hit"                => intval($r->hit),
//                         "tanggal"            => tgl_indo($r->tanggal)
//                         );
//                         $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }
//   function parser_analitik_order($arrays)
//   {
    
//     foreach($arrays as $r){

//                         $array = array(
//                         "id_order_detail"    => $r->id_order_detail,
//                         "id_order"           => $r->id_order,
//                         "tanggal"            => $r->tanggal,
//                         "status"             => $r->status,
//                         "jumlah_orang"       => count($arrays),
//                         );
//                         $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }
//   function jadwal_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $pecah_tgl      = explode("-", $r->tanggal);   
//     $pecah_tgl_plg  = explode("-", $r->tanggal_pulang);

//     $tanggal        = $pecah_tgl[2]."-".$pecah_tgl[1]."-".$pecah_tgl[0];
//     $tanggal_pulang = $pecah_tgl_plg[2]."-".$pecah_tgl_plg[1]."-".$pecah_tgl_plg[0];


//     $array = array(
//     // "id_jadwal"         => "qwe",
//     "id_jadwal"         => $r->id_jadwal,
//     "id_paket"          => $r->id_paket,
//     "tanggal"           => $tanggal,
//     "tanggal_pulang"    => $tanggal_pulang
//     );
//     $looping_data = (object) $array;        
//   }
//     return @$looping_data;
//   }

//   function edit_jadwal_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $pecah_tgl      = explode("-", $r->tanggal);   
//     $pecah_tgl_plg  = explode("-", $r->tanggal_pulang);

//     $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];
//     $tanggal_pulang = $pecah_tgl_plg[2]."/".$pecah_tgl_plg[1]."/".$pecah_tgl_plg[0];


//         // $r->gambar = base_url('gudang/upload/slider/'.$query->row_array()['gambar']);

//           $tes = file_exists(FCPATH."gudang/upload/paket/".$r->gambar);
//             if ($tes == true) {
//               $gambar = base_url('gudang/upload/paket/'.$r->gambar.'');
//             }else{
//               $gambar = base_url('gudang/upload/no_image.jpg');
//             }

//     $array = array(
//     // "id_jadwal"         => "qwe",
//     "id_jadwal"         => $r->id_jadwal,
//     "id_paket"          => $r->id_paket,
//     "gambar"            => $gambar,
//     "gambar_asli"            => $tes,
//     "tanggal"           => $tanggal,
//     "tanggal_pulang"    => $tanggal_pulang,
//     "kuota"             => $r->kuota
//     );
//     $looping_data = (object) $array;        
//   }
//     return @$looping_data;
//   }


//   function album_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $pecah_tgl      = explode("-", $r->tanggal);   
//     $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];

//     $array = array(
//     "id_album"         => $r->id_album,
//     "nama_album"       => $r->nama_album,
//     "tanggal"          => $tanggal
//     );
//     $looping_data = (object) $array;        
//     }
//     return @$looping_data;
//   }
//   function album_video_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $pecah_tgl      = explode("-", $r->tanggal);   
//     $tanggal        = $pecah_tgl[2]."/".$pecah_tgl[1]."/".$pecah_tgl[0];

//     $array = array(
//     "id_album_embed"   => $r->id_album_embed,
//     "nama_album"       => $r->nama_album,
//     "tanggal"          => $tanggal
//     );
//     $looping_data = (object) $array;        
//     }
//     return @$looping_data;
//   }

//   function bus_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $array = array(
//     "id"         => $r->id_bus,
//     "nama"       => $r->nama_bus,
//     "foto"       => $r->gambar_bus,
//     "type"       => "bus",
//     "folder"     => "bus"
//     );
//     $looping_data[] = (object) $array;        
//     }
//     return @$looping_data;
//   }

//   function tanya_ustad_parser($arrays)
//   {
//     foreach($arrays as $r){

//     $array = array(
//     'id_pertanyaan'       => $r->id_pertanyaan,
//     'id_sub_pertanyaan'   => $r->id_sub_pertanyaan,
//     'nama_lengkap'        => $r->nama_lengkap,
//     'email'               => $r->email,
//     'komen'               => $r->komen,
//     'ip'                  => $r->ip,
//     'tanggal'             => $r->tanggal,
//     'status'              => $r->status
//     );
//     $looping_data = (object) $array;        
//     }
//     return @$looping_data;
//   }

//   function perlengkapan_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $array = array(
//     "id"         => $r->id_perlengkapan,
//     "nama"       => $r->nama_perlengkapan,
//     "foto"       => null,
//     "type"       => "perlengkapan",
//     "folder"     => "perlengkapan"
//     );
//     $looping_data[] = (object) $array;        
//     }
//     return @$looping_data;
//   }

//   function pesawat_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $array = array(
//     "id"         => $r->id_pesawat,
//     "nama"       => $r->nama_pesawat,
//     "foto"       => $r->gambar_pesawat,
//     "type"       => "pesawat",
//     "folder"     => "pesawat"
//     );
//     $looping_data[] = (object) $array;        
//     }
//     return @$looping_data;
//   }

//   function makanan_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $array = array(
//     "id"         => $r->id_makanan,
//     "nama"       => $r->makanan,
//     "foto"       => $r->gambar_makanan,
//     "type"       => "makanan",
//     "folder"     => "makanan"
//     );
//     $looping_data[] = (object) $array;        
//     }
//     return @$looping_data;
//   }

//   function hotel_parser($arrays,$daerah_hotel)
//   {
//     foreach($arrays as $r){
//     $array = array(
//     "id"         => $r->id_hotel,
//     "nama"       => $r->nama_hotel,
//     "foto"       => $r->gambar_hotel,
//     "type"       => "hotel_".$daerah_hotel,
//     "folder"     => "hotel"
//     );
//     $looping_data[] = (object) $array;        
//     }
//     return @$looping_data;
//   }
  
//     function order_parser($arrays)
//   {
//     foreach($arrays as $r){
//     $pecah_tgl      = explode("-", $r->tanggal);   
//     $pecah_tgl_plg  = explode("-", $r->tanggal_pulang);

//     $tanggal        = $pecah_tgl[2]."-".$pecah_tgl[1]."-".$pecah_tgl[0];
//     $tanggal_pulang = $pecah_tgl_plg[2]."-".$pecah_tgl_plg[1]."-".$pecah_tgl_plg[0];

//     if ($r->status_berangkat == "1") {
//       $status_berangkat = "Transit";
//     }else{
//       $status_berangkat = "Langsung";
//     }

//     if ($r->status_pulang == "1") {
//       $status_pulang = "Transit";
//     }else{
//       $status_pulang = "Langsung";
//     }



//     $array = array(
//     "id_jadwal"         => $r->id_jadwal,
//     "id_paket"          => $r->id_paket,
//     "id_order"          => $r->id_order,
//     "nama_paket"        => $r->nama_paket,
//     "harga"             => rupiah($r->harga),
//     "nama_pesawat"      => $r->nama_pesawat,
//     "double"            => $r->double,
//     "triple"            => rupiah($r->harga - $r->triple),
//     "quad"              => rupiah($r->harga - $r->quad),
//     "total_double"      => rupiah($r->order_double * $r->harga),
//     "total_triple"      => rupiah($r->order_triple * ($r->harga - $r->triple)),
//     "total_quad"        => rupiah($r->order_quad * ($r->harga - $r->quad)),
//     "total"             => rupiah(($r->order_double * $r->harga)+($r->order_triple * ($r->harga - $r->triple))+($r->order_quad * ($r->harga - $r->quad))),
//     "pendaftaran"       => rupiah($r->biaya_tambahan),
//     "order_double"      => $r->order_double,
//     "order_triple"      => $r->order_triple,
//     "order_quad"        => $r->order_quad,
//     "nama_pesawat"      => $r->nama_pesawat,
//     "nama_pesawat"      => $r->nama_pesawat,
//     "nama_pesawat"      => $r->nama_pesawat,
//     "berangkat"         => $r->berangkat,
//     "pulang"            => $r->pulang,
//     "status_berangkat"  => $status_berangkat,
//     "status_pulang"     => $status_pulang,
//     "tanggal"           => tgl_indo($r->tanggal),
//     "tanggal_pulang"    => tgl_indo($r->tanggal_pulang),
//     "nama_lengkap"      => $r->nama_lengkap,
//     "email"             => $r->email,
//     "no_telp"           => $r->no_telp,
//     "alamat"            => $r->alamat,
//     "tanggal_booking"   => tgl_indo($r->tanggal_booking)
//     );
//     $looping_data = (object) $array;        
//   }
//     return @$looping_data;
//   }

//     function mata_uang_parser($arrays)
//   {
//     foreach($arrays as $r){

//     $array = array(
//     "id_history_mata_uang"      => $r->id_history_mata_uang,
//     "nominal"                   => $r->nominal,
//     "tanggal"                   => tgl_indo($r->tanggal)
//     );

//     $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }

//     function order_dash_parser($arrays)
//   {
//     foreach($arrays as $r){

//     $array = array(
//     "id_order"              => $r->id_order,
//     "nama_lengkap"          => $r->nama_lengkap,
//     "tanggal_booking"       => tgl_indo($r->tanggal_booking)
//     );

//     $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }
//     function jadwal_dash_parser($arrays)
//   {
//     foreach($arrays as $r){

//     $array = array(
//     "id_jadwal"      => $r->id_jadwal,
//     "nama_paket"     => $r->nama_paket,
//     "tanggal"        => tgl_indo($r->tanggal),
//     "tanggal_pulang" => tgl_indo($r->tanggal_pulang)
//     );

//     $looping_data[] = (object) $array;        
//   }
//     return @$looping_data;
//   }

//   function edit_paket_parser($arrays,$id)
//   {
//     foreach($arrays as $r){

//     $array = array(
//         "id_kategori_hotel"         => $id,
//     );

//     $arraaaay = array_merge($array,$arrays);
//     $looping_data = (object) $arraaaay;        
//   }
//     return @$looping_data;
//   }
          
//   }
