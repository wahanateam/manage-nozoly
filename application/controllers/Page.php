<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model');
	}
	public function _remap($method)
	{
		$id = $this->session->userdata('id_level');
		// die($id);
		$data['get_menu'] = $this->dashboard_model->get_menu($id);
		
		if ($this->session->has_userdata('logged_in') == FALSE) {
			redirect('auth/');
		}

		$i = $this->uri->segment(4);
	    if ($method != "" )
	    {
	     $data['konten'] = 'angular_page';
		 $this->load->view('main',$data);
	    }
	    else
	    {
	       show_404();
	    }
	}

}

/* End of file Main.php */
/* Location: ./application/controllers/manage/Main.php */