<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $value = $this->input->post();
        $cek   = $this->auth_model->login($value);
        if($cek['num'] > 0){
            foreach ($cek['data'] as $key ) {
                $id_admin       = $key->id_admin;
                $username       = $key->username;
                $gambar         = $key->gambar;
                $nama           = $key->nama;
                $id_level       = $key->id_level;
                $main           = $key->main;
            }
            
            $newdata = array(
            'id_admin'          => $id_admin,
            'nama'              => $nama,
            'username'          => $username,
            'gambar'            => $gambar,
            'id_level'          => $id_level,
            'main'              => $main,
            'logged_in'         => TRUE);
            
            $this->session->set_userdata($newdata);
            
            redirect('page/home/dashboard');
        }else{

            $this->session->set_flashdata('message', '<div class="warning">Username / Password yang anda masukan salah atau tidak terdaftar.</div>');
            redirect('auth/');
        }
    }
}
