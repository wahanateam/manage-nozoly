<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('menu_admin_model');
		$this->load->model('admin_model');
		$this->load->model('level_model');
		$this->load->model('log_model');
		$this->load->model('data_travel_model');
		$this->load->model('data_jamaah_model');
		$this->urisegment	=	$this->uri->segment(4);
		$this->id_admin     = $this->session->userdata('id_admin');
	}
	public function log_admin()
	{
		$post 	= $this->input->post();
		$list   = $this->log_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = $person->nama;
            $row[] = $person->keterangan;
            $row[] = indonesian_date($person->tanggal);
 
            //add html for action
            $row[] = '';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->log_model->count_all(),
				   'recordsFiltered'=> $this->log_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->log_model->get_id($id);
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}	


	public function admin()
	{
		$post 	= $this->input->post();
		$list   = $this->admin_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = '<a href="'.base_url('page/master/admin/input/'.$person->id_admin.'').'">'.$person->nama.'</a>';
            $row[] = $person->username;
            $row[] = $person->nama_level;
            $row[] = $person->status;
 
            //add html for action
            $row[] = '
			<a id="DomDelete" id-delete="'.$person->id_admin.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>

                  ';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->admin_model->count_all(),
				   'recordsFiltered'=> $this->admin_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->admin_model->get_id($id);
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function admin_post()
	{
		$input = $this->input->post();

		$config['upload_path'] = './gudang/upload/admin/';
		$config['allowed_types'] = '*';
		$config['file_name'] = date('Ymdhis');
		$config['max_size'] = '8000';
		$config['max_width'] = '8000';
		$config['max_height'] = '8000';
			
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

        $this->form_validation->set_data($input);

		if (! @$input['id_admin']) {

        	if ($this->form_validation->run('admin_insert_post') == FALSE)
        	{
        		    $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
        		                   'error_part' => $this->form_validation->error_array());
					$this->output
				   	 ->set_content_type('application/json')
     			     ->set_status_header(404)
				   	 ->set_output(json_encode($array));
		
        	}elseif ($input['password'] != $input['re_password']) {

        		    $array = array('messages'    => 'Password dan Repassword harus sama.',
        		                   'error_part' => $this->form_validation->error_array());
					$this->output
				   	 ->set_content_type('application/json')
     			     ->set_status_header(404)
				   	 ->set_output(json_encode($array));

        	}else{
				if ( ! $this->upload->do_upload('gambar'))
            	    {
		            $array = array('messages' => 'Belum ada gambar yang di pilih .');
					$this->output
					   	 ->set_content_type('application/json')
			     	     ->set_status_header(404)
					   	 ->set_output(json_encode($array));
            	    }
            	else
            	    {
							$resize_img['source_image'] 	=  $this->upload->data('full_path');
							$resize_img['new_image'] 		= './gudang/upload/admin/thumb/';
							$resize_img['width']         	= 360;
							$notif_resize = resize_img($resize_img);

            	            $data_gambar = array('upload_data' => $this->upload->data());
            	            $new_id 	 = $this->admin_model->input($input,@$data_gambar['upload_data']['file_name'],$notif_resize);
           					$keterangan  = "Input Admin";
           					$log         = $this->log_model->input($this->id_admin,$keterangan,null);
				    	  	$this->output
						       ->set_content_type('application/json')
							   ->set_output(json_encode($new_id));
            		}
				

			}
		}else{
				if($this->form_validation->run('admin_post') == FALSE){
        		    $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
        		                   'error_part' => $this->form_validation->error_array());
					$this->output
				   	 ->set_content_type('application/json')
     			     ->set_status_header(404)
				   	 ->set_output(json_encode($array));
				   	 
        		}elseif (@$input['password'] != @$input['re_password']) {

        		    $array = array('messages'    => 'Password dan Repassword harus sama.',
        		                   'error_part' => $this->form_validation->error_array());
					$this->output
				   	 ->set_content_type('application/json')
     			     ->set_status_header(404)
				   	 ->set_output(json_encode($array));

        		}else{

					$query = $this->admin_model->get_id_input($input);
		
					if ( ! @$_FILES['gambar']['name'])
           		     {	
							$new_id = $this->admin_model->update($input);

							if ($this->session->userdata('id_admin') == $input['id_admin']) {
								$this->session->set_userdata($new_id['session']);
							}
							$keterangan  = "Update Admin";
           					$log         = $this->log_model->input($this->id_admin,$keterangan,null);
							$this->output
		   					 ->set_content_type('application/json')
		   					 ->set_output(json_encode($new_id));
           		     }
		
           		 elseif( @$_FILES['gambar']['name'])
           		     {
           		     	$this->upload->do_upload('gambar');
						$resize_img['source_image'] 	=  $this->upload->data('full_path');
						$resize_img['new_image'] 		= './gudang/upload/admin/thumb/';
						$resize_img['width']         	= 360;
						$notif_resize = resize_img($resize_img);

           		     	$data_gambar = array('upload_data' => $this->upload->data());

							$new_id = $this->admin_model->update($input,@$data_gambar['upload_data']['file_name'],$notif_resize);
							$keterangan  = "Update Admin";
           					$log         = $this->log_model->input($this->id_admin,$keterangan,null);
							if ($this->session->userdata('id_admin') == $input['id_admin']) {
								$this->session->set_userdata($new_id['session']);
							}

							$this->output
		   					 ->set_content_type('application/json')
		   					 ->set_output(json_encode($new_id));
							
		   					@unlink('gudang/upload/admin/'.$query->gambar);
		   					@unlink('gudang/upload/admin/thumb/'.$query->gambar);
           		 	}     			
        		}

			}
	}
	public function admin_tambahan()
	{
		$result = array('level' 	=> $this->admin_model->get_level());

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	

	public function data_travel()
	{
		$post 	= $this->input->post();
		$list   = $this->data_travel_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = '<a href="'.base_url('page/master/data_travel/input/'.$person->id_travel.'').'">'.$person->nama_travel.'</a>';
            $row[] = $person->alamat;
            $row[] = $person->status;
 
            //add html for action
            $row[] = '
			<a id="DomDelete" id-delete="'.$person->id_travel.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>

                  ';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->data_travel_model->count_all(),
				   'recordsFiltered'=> $this->data_travel_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->data_travel_model->get_id($id);
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function data_travel_post()
	{
		$input = $this->input->post();

        $this->form_validation->set_data($input);

        if ($this->form_validation->run('data_travel_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part' => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }else{
			if (! @$input['id_travel']) {
				$result = $this->data_travel_model->input($input);
				$keterangan  = "Input Level";
				$log         = $this->log_model->input($this->id_admin,$keterangan,null);
				$this->output
				   	 ->set_content_type('application/json')
				   	 ->set_output(json_encode($result));
			}else{
				$result = $this->data_travel_model->update($input);
				$keterangan  = "Update Level";
				$log         = $this->log_model->input($this->id_admin,$keterangan,null);
				$this->output
				   	 ->set_content_type('application/json')
				   	 ->set_output(json_encode($result));
			}
		}
	}
	public function data_travel_delete($id)
	{
		$result = $this->data_travel_model->delete($id);
		$keterangan  = "Delete Data Travel";
		$log         = $this->log_model->input($this->id_admin,$keterangan,null);
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}


	public function level()
	{
		$post 	= $this->input->post();
		$list   = $this->level_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = '<a href="'.base_url('page/master/level/input/'.$person->id_level.'').'">'.$person->nama_level.'</a>';
            $row[] = $person->status;
 
            //add html for action
            $row[] = '
            <a href="'.base_url('page/master/level/input_akses/'.$person->id_level.'').'" class="btn btn-info"><span class="icon pen"></span>Hak Akses</a>
			<a id="DomDelete" id-delete="'.$person->id_level.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>
                  ';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->level_model->count_all(),
				   'recordsFiltered'=> $this->level_model->count_filtered(),
				   'data' => $data];

		}else{
			$result1	=	$this->level_model->get_id($id);
			$get_akses	=	$this->level_model->get_akses($id);
			$result2['akses'] = array();
			foreach ($get_akses as $r) {
				$result2['akses'][$r->id_menu_admin] = $r->akses;
			}
			$result = array_merge($result1,$result2);
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}		
	public function level_tambahan()
	{
		$id = $this->input->get('id');

		$menu = $this->level_model->get_menu();		

		$result = array('menu' 	=> $menu);

        
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function level_post()
	{
		$input = $this->input->post();


        if ($this->form_validation->run('level_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part' => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }else{
		if (! @$input['id_level']) {
			$result = $this->level_model->input($input);
			$keterangan  = "Input Level";
			$log         = $this->log_model->input($this->id_admin,$keterangan,null);
			$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
		}else{
			$result = $this->level_duta_model->update($input);
			$keterangan  = "Update Level";
			$log         = $this->log_model->input($this->id_admin,$keterangan,null);
			$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
		}
		}
	}
	public function levelakses_post()
	{
		$input 	= $this->input->post();
		$count 	= $this->level_model->cek_akses($input['id_level']);
		if ($count == 0) {
			$result = $this->level_model->input_akses($input);		
			$keterangan  = "Input Akses";
			$log         = $this->log_model->input($this->id_admin,$keterangan,null);
		}else{
			$result = $this->level_model->update_akses($input);		
			$keterangan  = "Update Akses";
			$log         = $this->log_model->input($this->id_admin,$keterangan,null);
		}
/*		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));*/

	}
	public function level_delete($id)
	{
		$result = $this->level_model->delete($id);
		$keterangan  = "Delete Level";
		$log         = $this->log_model->input($this->id_admin,$keterangan,null);
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}


	public function data_jamaah()
	{
		$post 	= $this->input->post();
		$list   = $this->data_jamaah_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = $person->nama_lengkap;
            $row[] = $person->nama_paket;
            $row[] = tgl_indo($person->tanggal);            
            $row[] = '<a href="'.base_url('page/jamaah_v2/data_jamaah/dokumen/'.$person->id_jamaah).'" modal class="btn-flat white-text '.$colordokumen.'"> Dokumen</a>
                    <a href="'.base_url('page/jamaah_v2/data_jamaah/pembayaran/'.$person->id_keberangkatan).'" modal class="btn-flat white-text '.$colorcicilan.'"> History Pembayaran</a>
                    <a href="'.base_url('page/jamaah_v2/data_jamaah/edit_jadwal/'.$person->id_jamaah).'" modal class="btn-flat white-text '.$colorhistory.'"> Edit Jadwal</a>
                    <a href="'.base_url('page/jamaah_v2/data_jamaah/edit_profile/'.$person->id_jamaah).'" modal class="btn-flat white-text green"> Edit Profile</a>
                    <a id="DomDelete" id-delete="'.$person->id_jamaah.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->data_jamaah_model->count_all(),
				   'recordsFiltered'=> $this->data_jamaah_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->data_jamaah_model->get_id($id);
			$cekprov = $this->data_jamaah_model->cek_prov(@$result->id_kab);

	        @$result->id_provinsi			 = $cekprov->id_prov;
	        @$result->masa_berlaku 			 = formatParserTgl(@$result->masa_berlaku);
	        @$result->tanggal_lahir 		 = formatParserTgl(@$result->tanggal_lahir);
	        @$result->tanggal_lahir_pasangan = formatParserTgl(@$result->tanggal_lahir_pasangan);
		}


		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function data_jamaah_post()
	{
		$input 	= $this->input->post();
		if (@$input['id_jadwal_sebelumnya'] == NULL || @$input['id_jadwal_sebelumnya'] == "") {
			if (@$input['id_jamaah'] == NULL || @$input['id_jamaah'] == "") {
			$result = $this->data_jamaah_model->input($input);
			$keterangan  = "Input Jamaah : ". @$input['nama_lengkap'];
			}else{
			$result = $this->data_jamaah_model->update($input);
			$keterangan  = "Edit Jamaah : ". @$input['nama_lengkap'];
			}
		}else{
			$result = $this->data_jamaah_model->update_jadwal($input);
			$keterangan  = "Edit Jadwal : ". @$input['nama_lengkap'];
		}

            $log         = $this->log_model->input($this->id_admin,$keterangan,'1');

		$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
	}
	public function data_jamaah_tambahan()
	{
		$id 					= $this->input->get('id');

		$edit					= $this->data_jamaah_model->get_edit_jadwal($id);
		$history_edit_jadwal 	= $this->data_jamaah_model->history_edit_jadwal($id);
		$dokumen 				= $this->data_jamaah_model->get_dokumen($id);
		$history_pembayaran 	= $this->data_jamaah_model->get_history_pembayaran($id);

		$result = array('edit_jadwal' 			=> $this->main->parser_edit_jadwal($edit),
						'paket' 				=> $this->data_jamaah_model->get_paket(),
						'provinsi' 				=> $this->data_jamaah_model->get_provinsi(),
						'dokumen' 				=> $dokumen,
						'history_pembayaran' 	=> $history_pembayaran,
						'history_edit_jadwal'	=> $this->main->parser_history_jadwal($history_edit_jadwal)
					);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function data_jamaah_delete($id)
	{

		$getid 	 	 = $this->data_jamaah_model->get_id($id);
		$keterangan  = "Delete Jamaah : ". @$getid->nama_lengkap;
		$result 	 = $this->data_jamaah_model->delete($id);
		$log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function menu_admin_delete($id)
	{

		$result = $this->menu_admin_model->delete($id);
		$keterangan  = "Delete Menu Admin";
		$log         = $this->log_model->input($this->id_admin,$keterangan,null);
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}


	public function menu_admin()
	{
		$post 	= $this->input->post();
		$list   = $this->menu_admin_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            if ($person->status == "1") {
            	$status = "Aktif";
            }else{
            	$status = "Tidak Aktif";
            }


            $row[] = $no++;
            $row[] = $person->nama_sub;
            $row[] = '<a href="'.base_url('page/master/menu_admin/input/'.$person->id_menu_admin.'').'">'.$person->nama_menu.'</a>';
            $row[] = $person->link;
            $row[] = $person->urutan;
 
            //add html for action
            $row[] = '
            <a id="DomListSub" id-menu-admin="'.$person->id_menu_admin.'" modal class="btn-flat white-text blue"><span class="icon "></span> Lihat Sub</a>
			<a id="DomDelete" id-delete="'.$person->id_menu_admin.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>';

            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->menu_admin_model->count_all(),
				   'recordsFiltered'=> $this->menu_admin_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->menu_admin_model->get_id($id);
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}		
	public function menu_admin_post()
	{
		$input = $this->input->post();
        if ($this->form_validation->run('menu_admin_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part' => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }else{
		if (! @$input['id_menu_admin']) {
			$result = $this->menu_admin_model->input($input);
			$keterangan  = "Input Menu Admin";
			$log         = $this->log_model->input($this->id_admin,$keterangan,null);
			$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
		}else{
			$result = $this->menu_admin_model->update($input);
			$keterangan  = "Update Menu Admin";
			$log         = $this->log_model->input($this->id_admin,$keterangan,null);
			$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
		}
	}

	}
	public function menu_admin_tambahan()
	{
		$result = array('sub' => $this->menu_admin_model->get_sub());
			$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
	}
	public function admin_delete($id)
	{
		$query 	= $this->admin_model->get_delete($id);
		$result = $this->admin_model->delete($id);
		$keterangan  = "Delete Admin";
		$log         = $this->log_model->input($this->id_admin,$keterangan,null);
		@unlink('gudang/upload/admin/'.$query->gambar);
		@unlink('gudang/upload/admin/thumb/'.$query->gambar);
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}


	public function sub_menu_admin()
	{
		$id = $this->input->get('id');
		$result = $this->menu_admin_model->get_sub_by_id($id);

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}
}

/* End of file Master.php */
/* Location: ./application/controllers/manage/api/Master.php */