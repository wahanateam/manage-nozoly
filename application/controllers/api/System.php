<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		//Do your magic here
		$this->load->model('pesawat_model');
		$this->load->model('bus_model');
		$this->load->model('makanan_model');
		$this->load->model('hotel_model');
		$this->load->model('perlengkapan_model');
		$this->load->model('gambar_model');
		$this->load->model('log_model');
		$this->load->model('data_jamaah_model');
		$this->load->model('package_model');
		$this->urisegment	= $this->uri->segment(4);
		$this->id_admin     = $this->session->userdata('id_admin');
	}

	public function get_jadwal()
	{
		$id 	= $this->input->get('id');
		$result	= $this->data_jamaah_model->get_jadwal($id);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function tanggal_jamaah()
	{
		$res   			= $this->data_jamaah_model->get_jadwal_jamaah();
		$res_jadwal   	= $this->main->parser_jadwal($res);
		$result   		= array('jadwal' 	=> $res_jadwal);

		$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
	}
	public function get_kabupaten()
	{
		$id 	= $this->input->get('id');
		$result	= $this->data_jamaah_model->get_kabupaten($id);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	
	public function data_jamaah()
	{
		$post 	= $this->input->post();
		$list   = $this->data_jamaah_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
        	$cekdokumen = $this->data_jamaah_model->cekdokumen($person->id_jamaah);
        	$cekcicilan = $this->data_jamaah_model->cekcicilan($person->id_jamaah);
        	$cekhistory = $this->data_jamaah_model->cekhistory($person->id_jamaah);
        	if ($cekdokumen > 0) {
        		$colordokumen = "green";
        	}
        	else{
        		$colordokumen = "light-blue";
        	}
        	if ($cekcicilan > 0) {
        		$colorcicilan = "green";
        	}
        	else{
        		$colorcicilan = "light-blue";
        	}
        	if ($cekhistory > 0) {
        		$colorhistory = "green";
        	}
        	else{
        		$colorhistory = "light-blue";
        	}
            $row = array();
            $row[] = $no++;
            $row[] = $person->nama_lengkap;
            $row[] = $person->nama_paket;
            $row[] = tgl_indo($person->tanggal);            
            $row[] = '<a href="'.base_url('page/system/data_jamaah/dokumen/'.$person->id_jamaah).'" modal class="btn-flat white-text '.$colordokumen.'"> Dokumen</a>
                    <a href="'.base_url('page/system/data_jamaah/pembayaran/'.$person->id_keberangkatan).'" modal class="btn-flat white-text '.$colorcicilan.'"> History Pembayaran</a>
                    <a href="'.base_url('page/system/data_jamaah/edit_jadwal/'.$person->id_jamaah).'" modal class="btn-flat white-text '.$colorhistory.'"> Edit Jadwal</a>
                    <a href="'.base_url('page/system/data_jamaah/edit_profile/'.$person->id_jamaah).'" modal class="btn-flat white-text green"> Edit Profile</a>
                    <a id="DomDelete" id-delete="'.$person->id_jamaah.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>';

            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->data_jamaah_model->count_all(),
				   'recordsFiltered'=> $this->data_jamaah_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->data_jamaah_model->get_id($id);
			$cekprov = $this->data_jamaah_model->cek_prov(@$result->id_kab);

	        @$result->id_provinsi			 = $cekprov->id_prov;
	        @$result->masa_berlaku 			 = formatParserTgl(@$result->masa_berlaku);
	        @$result->tanggal_lahir 		 = formatParserTgl(@$result->tanggal_lahir);
	        @$result->tanggal_lahir_pasangan = formatParserTgl(@$result->tanggal_lahir_pasangan);
		}


		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function data_jamaah_post()
	{
		$input 	= $this->input->post();
		if (@$input['id_jadwal_sebelumnya'] == NULL || @$input['id_jadwal_sebelumnya'] == "") {
			if (@$input['id_jamaah'] == NULL || @$input['id_jamaah'] == "") {
			$result = $this->data_jamaah_model->input($input);
			$keterangan  = "Input Jamaah : ". @$input['nama_lengkap'];
			}else{
			$result = $this->data_jamaah_model->update($input);
			$keterangan  = "Edit Jamaah : ". @$input['nama_lengkap'];
			}
		}else{
			$result = $this->data_jamaah_model->update_jadwal($input);
			$keterangan  = "Edit Jadwal : ". @$input['nama_lengkap'];
		}

            $log         = $this->log_model->input($this->id_admin,$keterangan,'1');

		$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
	}
	public function data_jamaah_tambahan()
	{
		$id 					= $this->input->get('id');

		$edit					= $this->data_jamaah_model->get_edit_jadwal($id);
		$history_edit_jadwal 	= $this->data_jamaah_model->history_edit_jadwal($id);
		$dokumen 				= $this->data_jamaah_model->get_dokumen($id);
		$history_pembayaran 	= $this->data_jamaah_model->get_history_pembayaran($id);

		$result = array('edit_jadwal' 			=> $this->main->parser_edit_jadwal($edit),
						'paket' 				=> $this->data_jamaah_model->get_paket(),
						'provinsi' 				=> $this->data_jamaah_model->get_provinsi(),
						'travel' 				=> $this->data_jamaah_model->get_travel(),
						'dokumen' 				=> $dokumen,
						'history_pembayaran' 	=> $history_pembayaran,
						'history_edit_jadwal'	=> $this->main->parser_history_jadwal($history_edit_jadwal)
					);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}

	public function data_jamaah_delete($id)
	{

		$getid 	 	 = $this->data_jamaah_model->get_id($id);
		$keterangan  = "Delete Jamaah : ". @$getid->nama_lengkap;
		$result 	 = $this->data_jamaah_model->delete($id);
		$log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}


	public function package()
	{
		$post 	= $this->input->post();
		$list   = $this->package_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;
		
		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = '<a href="'.base_url('page/system/package/input/'.$person->id_paket.'').'">'.$person->nama_paket.'</a>';
            $row[] = strtoupper($person->jenis_produk);
            $row[] = $person->tampilkan == 1 ? 'Aktif' : 'Tidak Aktif';
            // $row[] = '<img src="'.base_url('gudang/upload/profile/'.$person->logo.'').'" width="50">';  
            //add html for action
            $row[] = '
			<a id="DomDelete" id-delete="'.$person->id_paket.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>
                  ';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->package_model->count_all(),
				   'recordsFiltered'=> $this->package_model->count_filtered(),
				   'data' => $data];

		}else{
			$datax	=	$this->package_model->get_id($id);
			$datag	=	$this->package_model->get_id_paket($id);
			foreach ($datag as $r) {
				$id_hotel_tambahan = $r->id_hotel_tambahan;
			}
			$query  =   $this->package_model->get_hotel_tambahan_id($id_hotel_tambahan);
			$result	=	$this->main->edit_paket_parser($datax,@$query->id_kategori);
		
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}		
	public function package_post()
	{
		$input = $this->input->post();

        $this->form_validation->set_data($input);

        if ($this->form_validation->run('paket_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part'  => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }
        else{
			if (! @$input['id_paket'])
			{
			    $new_id = $this->package_model->input($input);

				$keterangan  = "Input Paket : ".@$input['nama_paket'];
            	$log         = $this->log_model->input($this->id_admin,$keterangan,'1');

				$this->output
				     ->set_content_type('application/json')
					 ->set_output(json_encode($new_id));
			}
			else
			{
				$query = $this->package_model->get_id_input($input);

				$new_id = $this->package_model->update($input);

				$keterangan  = "Update Paket : ".@$input['nama_paket'];
            	$log         = $this->log_model->input($this->id_admin,$keterangan,'1');

				$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($new_id));

			}
		}
	}
	public function package_tambahan()
	{
		$id = $this->input->get('id');
		$data	=	$this->package_model->get_id_paket($id);
		if(count($data) > 0 ){

		
		foreach ($data as $key){
		$result = array('pesawat' 		=> $this->post_maskapai_hasil($key->id_pesawat),
						'bus' 			=> $this->post_bus_hasil($key->id_bus),
						'hotel_makkah' 	=> $this->post_hotel_makkah_hasil($key->id_hotel_makkah),
						'hotel_madinah' => $this->post_hotel_madinah_hasil($key->id_hotel_madinah),
						'hotel_jeddah' 	=> $this->post_hotel_jeddah_hasil($key->id_hotel_jeddah),
						'makanan' 		=> $this->post_makanan_hasil($key->id_makanan),
						'perlengkapan'	=> $this->post_perlengkapan_hasil($key->id_perlengkapan));
		}}else{
			$result = null;
		}
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}
	public function package_delete($id)
	{
		$result = $this->package_model->delete($id);
		$keterangan  = "Delete Paket Umrah";
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}


	public function post_maskapai()
	{
    	$data   = $this->paket_model->get_pesawat();
		$result = $this->main->pesawat_parser($data);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}
	public function post_maskapai_hasil($id = null)
	{
		if($id == null){
			$id = $this->input->get('id');
		$result   = $this->paket_model->get_maskapai_hasil($id);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
		}else{
			$id = $id;
			$result   = $this->paket_model->get_maskapai_hasil($id);
			return $result;
		}
	}

	public function post_bus()
	{
		$data   = $this->paket_model->get_bus();
		$result = $this->main->bus_parser($data);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}
	public function post_bus_hasil($id = null)
	{
		if($id == null){
			$id = $this->input->get('id');
		$result   = $this->paket_model->get_bus_hasil($id);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
		}else{
			$id = $id;
		$result   = $this->paket_model->get_bus_hasil($id);
		return $result;
		}
	}

	public function post_makanan()
	{
		$data   = $this->paket_model->get_makanan();
		$result = $this->main->makanan_parser($data);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}
	public function post_makanan_hasil($id = null)
	{
		if($id == null){
			$id = $this->input->get('id');
		$result   = $this->paket_model->get_makanan_hasil($id);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
		}else{
		$id = $id;
		$result   = $this->paket_model->get_makanan_hasil($id);
		return $result;
		}
	}


	public function post_hotel_madinah()
	{
		$data   = $this->paket_model->get_hotel('2');
		$result = $this->main->hotel_parser($data,'madinah');
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}

	public function post_hotel()
	{
		$data   = $this->paket_model->get_all_hotel();
		$result = $this->main->allhotel_parser($data);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}

	public function post_hotel_hasil($id = null)
	{
		if($id == null){
			$id 		= $this->input->get('id');
			$result 	= $this->paket_model->get_all_hotel_hasil($id);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
		}else{
			$id = $id;
			$result   	= $this->paket_model->get_all_hotel_hasil($id);
			return $result;
		}
	}



	public function post_hotel_madinah_hasil($id = null)
	{
		if($id == null){
			$id = $this->input->get('id');
		$result   = $this->paket_model->get_hotel_hasil('2',$id,'madinah');
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
		}else{
			$id = $id;
			$result   = $this->paket_model->get_hotel_hasil('2',$id,'madinah');
			return $result;
		}
	}

	public function post_hotel_makkah()
	{
		$data   = $this->paket_model->get_hotel('1');
		$result = $this->main->hotel_parser($data,'makkah');
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}


	public function post_hotel_makkah_hasil($id = null)
	{
		if($id == null){
			$id = $this->input->get('id');
		$result   = $this->paket_model->get_hotel_hasil('1',$id,'makkah');
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
		}else{
			$id = $id;
			$result   = $this->paket_model->get_hotel_hasil('1',$id,'makkah');
			return $result;
		}
	}

	public function post_hotel_jeddah()
	{
		$data   = $this->paket_model->get_hotel('3');
		$result = $this->main->hotel_parser($data,'jeddah');
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}
	public function post_hotel_jeddah_hasil($id = null)
	{
		if($id == null){
			$id = $this->input->get('id');
		$result   = $this->paket_model->get_hotel_hasil('3',$id,'jeddah');
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
		}else{
			$id = $id;
			$result   = $this->paket_model->get_hotel_hasil('3',$id,'jeddah');
			return $result;
		}
	}

	public function post_perlengkapan()
	{
		$data   = $this->paket_model->get_perlengkapan();
		$result = $this->main->perlengkapan_parser($data);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}
	public function post_perlengkapan_hasil($id = null)
	{
		if($id == null){
			$id = $this->input->get('id');
		$result   = $this->paket_model->get_perlengkapan_hasil($id);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
		}else{
			$id = $id;
			$result   = $this->paket_model->get_perlengkapan_hasil($id);
			return $result;
		}
	}

	public function pesawat()
	{
		$post 	= $this->input->post();
		$list   = $this->pesawat_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
        	$d = $this->gambar_model->get_gambar_solo($person->logo);
            
            $row = array();
            $row[] = $no++;
            $row[] = '<a href="'.base_url('page/system/pesawat/input/'.$person->id_pesawat.'').'" >'.$person->nama_pesawat.'</a>';
        	$tes = file_exists(FCPATH."gudang/upload/pesawat/".$person->logo);
            if ($tes == true) {
            	$row[] = '<img src="'.base_url('gudang/upload/pesawat/'.$person->logo.'').'" width="50">';
            }else{
            	$row[] = '<img src="'.base_url('gudang/upload/no_image.jpg').'" width="50">';
            }
            $row[] = strip_tags(word_limiter($person->keterangan,20));
 
            //add html for action
            $row[] = '
			<a id="DomDelete" id-delete="'.$person->id_pesawat.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>
                  ';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->pesawat_model->count_all(),
				   'recordsFiltered'=> $this->pesawat_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->pesawat_model->get_id($id);
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}		
	public function pesawat_tambahan()
	{
		$id 				= $this->input->get('id');
		$query      		= $this->pesawat_model->get_tambahan($id);
		$id_gambar_group 	= $query['id_gambar_group'];


		$result = array('gambar' => $this->gambar_model->get_gambar_by_id($id_gambar_group));

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function pesawat_post()
	{
    	$this->load->library('upload');
		$input = $this->input->post();

		if ($this->form_validation->run('pesawat_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part' => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }else{
        	if(! @$input['id_pesawat']){
				$data_gambar 		= "";
			    $number_of_files 	= sizeof(@$_FILES['gambar']['tmp_name']);
			    $files 				= @$_FILES['gambar'];
        		if($number_of_files == 0){
		            $array = array('messages' => 'Belum ada gambar yang di pilih .');
					$this->output
					   	 ->set_content_type('application/json')
			     	     ->set_status_header(404)
					   	 ->set_output(json_encode($array));
        		}else{
				    $config['upload_path'] 		= './gudang/upload/pesawat/';
				    $config['allowed_types'] 	= 'jpg|png|jpeg';
					$config['max_size']     	= '5000';
					$config['max_width'] 		= '5000';
					$config['remove_spaces']    = TRUE;
					//create id_gambar_group gunakan mysql insert id
					$id_gambar_group = $this->gambar_model->input_gambar_group('Pesawat');

				    for ($i = 0; $i < $number_of_files; $i++)
				    {
				      $_FILES['gambar']['name'] 		= $files['name'][$i];
				      $_FILES['gambar']['type'] 		= $files['type'][$i];
				      $_FILES['gambar']['tmp_name'] 	= $files['tmp_name'][$i];
				      $_FILES['gambar']['error'] 		= $files['error'][$i];
				      $_FILES['gambar']['size'] 		= $files['size'][$i];
				      $this->upload->initialize($config);	
				      if ($this->upload->do_upload('gambar'))
				      {
							$resize_img['source_image'] =  $this->upload->data('full_path');
							$resize_img['new_image'] 	= './gudang/upload/pesawat/thumb/';
							$resize_img['width']        = 360;
							$notif_resize 	 			= resize_img($resize_img);
					   // masukan gambar by id group
							$data_gambar  = $this->upload->data('file_name');
							$input_gambar = $this->gambar_model->input_gambar($data_gambar,$id_gambar_group);
							
				      }else{
				      	$notif = ",tapi beberapa gambar tidak terupload , silahkan pastikan ukuran sudah sesuai.";
				      }
        			}
				 	$configlogo['upload_path'] 		= './gudang/upload/pesawat/';
				 	$configlogo['allowed_types'] 	= '*';
				 	$configlogo['max_size']     	= '150';
				 	$configlogo['max_width'] 		= '150';
				 	$configlogo['remove_spaces']    = TRUE;
			      $this->upload->initialize($configlogo);	
			      if ($this->upload->do_upload('logo'))
			      {
			      		$data_logo  = $this->upload->data('file_name');
			      		$return 	= $this->pesawat_model->input($input,$id_gambar_group,$data_logo,@$notif);

						$keterangan  = "Input Pesawat : ".@$input['nama_pesawat'];
            			$log         = $this->log_model->input($this->id_admin,$keterangan,'1');


			      		   $this->output
							   	->set_content_type('application/json')
							   	->set_output(json_encode($return));
			      }else{
			      	// select gambar by group looping lalu unlink.
			      	$datagambar =  $this->gambar_model->get_by_id_group($id_gambar_group);
			      	foreach ($datagambar as $r) {
			      		@unlink("./gudang/upload/pesawat/".$r->gambar);
			      		@unlink("./gudang/upload/pesawat/thumb/".$r->gambar);
			      		$gambar = $r->gambar;
			      	}
		            $array = array('messages' => 'Logo Pesawat Belum dipilih / ukuran gambar tidak cocok .');
					$this->output
					   	 ->set_content_type('application/json')
			     	     ->set_status_header(404)
					   	 ->set_output(json_encode($array));
			      }
				}
        	}else{
				$data_gambar 		= "";
			    $number_of_files 	= sizeof(@$_FILES['gambar']['tmp_name']);
			    $files 				= @$_FILES['gambar'];
        		
				    $config['upload_path'] 		= './gudang/upload/pesawat/';
				    $config['allowed_types'] 	= 'jpg|png|jpeg';
					$config['max_size']     	= '5000';
					$config['max_width'] 		= '5000';
					$config['remove_spaces']    = TRUE;

				    for ($i = 0; $i < $number_of_files; $i++)
				    {
				      $_FILES['gambar']['name'] 		= $files['name'][$i];
				      $_FILES['gambar']['type'] 		= $files['type'][$i];
				      $_FILES['gambar']['tmp_name'] 	= $files['tmp_name'][$i];
				      $_FILES['gambar']['error'] 		= $files['error'][$i];
				      $_FILES['gambar']['size'] 		= $files['size'][$i];
				      $this->upload->initialize($config);	
				      if ($this->upload->do_upload('gambar'))
				      {
							$resize_img['source_image'] =  $this->upload->data('full_path');
							$resize_img['new_image'] 	= './gudang/upload/pesawat/thumb/';
							$resize_img['width']        = 360;
							$notif_resize 	 			= resize_img($resize_img);
					   // masukan gambar by id group
							$data_gambar  = $this->upload->data('file_name');
							$input_gambar = $this->gambar_model->input_gambar($data_gambar,$input['id_gambar_group']);
							
				      }else{
				      }
        			}
				 	$configlogo['upload_path'] 		= './gudang/upload/pesawat/';
				 	$configlogo['allowed_types'] 	= '*';
				 	$configlogo['max_size']     	= '200';
				 	$configlogo['max_width'] 		= '200';
				 	$configlogo['remove_spaces']    = TRUE;
			      $this->upload->initialize($configlogo);	
			      if ($this->upload->do_upload('logo'))
			      {
			      		$query 		= $this->pesawat_model->get_id_input($input);
			      		@unlink("./gudang/upload/pesawat/".$query->logo);

			      		$data_logo  = $this->upload->data('file_name');
			      		$return 	= $this->pesawat_model->update($input,$data_logo,@$notif);
			      		$keterangan  = "Update Pesawat : ".@$input['nama_pesawat'];
            			$log         = $this->log_model->input($this->id_admin,$keterangan,'1');
			      		   $this->output
							   	->set_content_type('application/json')
							   	->set_output(json_encode($return));
			      }else{
			      	// select gambar by group looping lalu unlink.
			      	$error = $this->upload->display_errors('','');
			      	$return 	= $this->pesawat_model->update($input,'',' tapi '.$error);
					$this->output
					   	 ->set_content_type('application/json')
					   	 ->set_output(json_encode($return));
			      }
        	}
		}	    
	}
	public function hotel()
	{
		$post 	= $this->input->post();
		$list   = $this->hotel_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
        	$d = $this->gambar_model->get_gambar_solo($person->id_gambar_group);

            $row = array();
            $row[] = $no++;
            $row[] = $person->lokasi;
 			$row[] = '<a href="'.base_url('page/system/hotel/input/'.$person->id_hotel.'').'" >'.$person->nama_hotel.'</a>';
            $row[] = strip_tags(word_limiter($person->keterangan,20));
			$row[] = '
				<a id="DomDelete" id-delete="'.$person->id_hotel.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>
                  ';
            

            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->hotel_model->count_all(),
				   'recordsFiltered'=> $this->hotel_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->hotel_model->get_id($id);
		}
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function hotel_post()
	{
    	$this->load->library('upload');
		$input = $this->input->post();

		if ($this->form_validation->run('hotel_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part' => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }else{
        	if(! @$input['id_hotel']){
				$data_gambar 		= "";
			    $number_of_files 	= sizeof(@$_FILES['gambar']['tmp_name']);
			    $files 				= @$_FILES['gambar'];
        		if($number_of_files == 0){
		            $array = array('messages' => 'Belum ada gambar yang di pilih .');
					$this->output
					   	 ->set_content_type('application/json')
			     	     ->set_status_header(404)
					   	 ->set_output(json_encode($array));
        		}else{
				    $config['upload_path'] 		= './gudang/upload/hotel/';
				    $config['allowed_types'] 	= 'jpg|png|jpeg';
					$config['max_size']     	= '5000';
					$config['max_width'] 		= '5000';
					$config['remove_spaces']    = TRUE;
					//create id_gambar_group gunakan mysql insert id
					$id_gambar_group = $this->gambar_model->input_gambar_group('Hotel');

				    for ($i = 0; $i < $number_of_files; $i++)
				    {
				      $_FILES['gambar']['name'] 		= $files['name'][$i];
				      $_FILES['gambar']['type'] 		= $files['type'][$i];
				      $_FILES['gambar']['tmp_name'] 	= $files['tmp_name'][$i];
				      $_FILES['gambar']['error'] 		= $files['error'][$i];
				      $_FILES['gambar']['size'] 		= $files['size'][$i];
				      $this->upload->initialize($config);	
				      if ($this->upload->do_upload('gambar'))
				      {
							$resize_img['source_image'] =  $this->upload->data('full_path');
							$resize_img['new_image'] 	= './gudang/upload/hotel/thumb/';
							$resize_img['width']        = 360;
							$notif_resize 	 			= resize_img($resize_img);
					   // masukan gambar by id group
							$data_gambar  = $this->upload->data('file_name');
							$input_gambar = $this->gambar_model->input_gambar($data_gambar,$id_gambar_group);
							
				      }else{
				      	$notif = ",tapi beberapa gambar tidak terupload , silahkan pastikan ukuran sudah sesuai.";
				      }
        			}
			      		$return 	= $this->hotel_model->input($input,$id_gambar_group,@$notif);
			      		$keterangan  = "Input Hotel : ".@$input['nama_hotel'];
            			$log         = $this->log_model->input($this->id_admin,$keterangan,'1');


			      		   $this->output
							   	->set_content_type('application/json')
							   	->set_output(json_encode($return));

					}
        	}else{
				$data_gambar 		= "";
			    $number_of_files 	= sizeof(@$_FILES['gambar']['tmp_name']);
			    $files 				= @$_FILES['gambar'];
        		
				    $config['upload_path'] 		= './gudang/upload/hotel/';
				    $config['allowed_types'] 	= 'jpg|png|jpeg';
					$config['max_size']     	= '5000';
					$config['max_width'] 		= '5000';
					$config['remove_spaces']    = TRUE;

				    for ($i = 0; $i < $number_of_files; $i++)
				    {
				      $_FILES['gambar']['name'] 		= $files['name'][$i];
				      $_FILES['gambar']['type'] 		= $files['type'][$i];
				      $_FILES['gambar']['tmp_name'] 	= $files['tmp_name'][$i];
				      $_FILES['gambar']['error'] 		= $files['error'][$i];
				      $_FILES['gambar']['size'] 		= $files['size'][$i];
				      $this->upload->initialize($config);	
				      if ($this->upload->do_upload('gambar'))
				      {
							$resize_img['source_image'] =  $this->upload->data('full_path');
							$resize_img['new_image'] 	= './gudang/upload/hotel/thumb/';
							$resize_img['width']        = 360;
							$notif_resize 	 			= resize_img($resize_img);
					   // masukan gambar by id group
							$data_gambar  = $this->upload->data('file_name');
							$input_gambar = $this->gambar_model->input_gambar($data_gambar,$input['id_gambar_group']);
							
				      }else{
				      }
        			}
			      		$return 	= $this->hotel_model->update($input,@$notif);

			      		$keterangan  = "Update Hotel : ".@$input['nama_hotel'];
            			$log         = $this->log_model->input($this->id_admin,$keterangan,'1');

			      		   $this->output
							   	->set_content_type('application/json')
							   	->set_output(json_encode($return));

        	}
		}	    
	}
	public function hotel_tambahan()
	{
		$id 				= $this->input->get('id');
		$query      		= $this->hotel_model->get_id($id);
		$id_gambar_group 	= $query['id_gambar_group'];

		$result = array('kategori' => $this->hotel_model->get_kategori(),
					    'gambar' => $this->gambar_model->get_gambar_by_id($id_gambar_group));

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}


	public function bus()
	{
		$post 	= $this->input->post();
		$list   = $this->bus_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = '<a href="'.base_url('page/system/bus/input/'.$person->id_bus.'').'">'.$person->nama_bus.'</a>';
            $row[] = strip_tags(word_limiter($person->keterangan,20));
            $row[] = '
			<a id="DomDelete" id-delete="'.$person->id_bus.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>

                  ';

            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->bus_model->count_all(),
				   'recordsFiltered'=> $this->bus_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->bus_model->get_id($id);
		}
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}		

	public function bus_post()
	{
		$this->load->library('upload');
		$input = $this->input->post();

		if ($this->form_validation->run('bus_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part' => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }else{
        	if(! @$input['id_bus']){
				$data_gambar 		= "";
			    $number_of_files 	= sizeof(@$_FILES['gambar']['tmp_name']);
			    $files 				= @$_FILES['gambar'];
        		if($number_of_files == 0){
		            $array = array('messages' => 'Belum ada gambar yang di pilih .');
					$this->output
					   	 ->set_content_type('application/json')
			     	     ->set_status_header(404)
					   	 ->set_output(json_encode($array));
        		}else{
				    $config['upload_path'] 		= './gudang/upload/bus/';
				    $config['allowed_types'] 	= 'jpg|png|jpeg';
					$config['max_size']     	= '5000';
					$config['max_width'] 		= '5000';
					$config['remove_spaces']    = TRUE;
					//create id_gambar_group gunakan mysql insert id
					$id_gambar_group = $this->gambar_model->input_gambar_group('Bus');

				    for ($i = 0; $i < $number_of_files; $i++)
				    {
				      $_FILES['gambar']['name'] 		= $files['name'][$i];
				      $_FILES['gambar']['type'] 		= $files['type'][$i];
				      $_FILES['gambar']['tmp_name'] 	= $files['tmp_name'][$i];
				      $_FILES['gambar']['error'] 		= $files['error'][$i];
				      $_FILES['gambar']['size'] 		= $files['size'][$i];
				      $this->upload->initialize($config);	
				      if ($this->upload->do_upload('gambar'))
				      {
							$resize_img['source_image'] =  $this->upload->data('full_path');
							$resize_img['new_image'] 	= './gudang/upload/bus/thumb/';
							$resize_img['width']        = 360;
							$notif_resize 	 			= resize_img($resize_img);
					   // masukan gambar by id group
							$data_gambar  = $this->upload->data('file_name');
							$input_gambar = $this->gambar_model->input_gambar($data_gambar,$id_gambar_group);
				      }else{
				      	$notif = ",tapi beberapa gambar tidak terupload , silahkan pastikan ukuran sudah sesuai.";
				      }
        			}
			      		   $return = $this->bus_model->input($input,$id_gambar_group,@$notif);

			      		   $keterangan  = "Input Bus : ".@$input['nama_bus'];
            			$log         = $this->log_model->input($this->id_admin,$keterangan,'1');


			      		   $this->output
							   	->set_content_type('application/json')
							   	->set_output(json_encode($return));
					}
        	}else{
				$data_gambar 		= "";
			    $number_of_files 	= sizeof(@$_FILES['gambar']['tmp_name']);
			    $files 				= @$_FILES['gambar'];
        		
				    $config['upload_path'] 		= './gudang/upload/bus/';
				    $config['allowed_types'] 	= 'jpg|png|jpeg';
					$config['max_size']     	= '5000';
					$config['max_width'] 		= '5000';
					$config['remove_spaces']    = TRUE;

				    for ($i = 0; $i < $number_of_files; $i++)
				    {
				      $_FILES['gambar']['name'] 		= $files['name'][$i];
				      $_FILES['gambar']['type'] 		= $files['type'][$i];
				      $_FILES['gambar']['tmp_name'] 	= $files['tmp_name'][$i];
				      $_FILES['gambar']['error'] 		= $files['error'][$i];
				      $_FILES['gambar']['size'] 		= $files['size'][$i];
				      $this->upload->initialize($config);	
				      if ($this->upload->do_upload('gambar'))
				      {
							$resize_img['source_image'] =  $this->upload->data('full_path');
							$resize_img['new_image'] 	= './gudang/upload/bus/thumb/';
							$resize_img['width']        = 360;
							$notif_resize 	 			= resize_img($resize_img);
					   // masukan gambar by id group
							$data_gambar  = $this->upload->data('file_name');
							$input_gambar = $this->gambar_model->input_gambar($data_gambar,$input['id_gambar_group']);
							
				      }else{
				      }
        			}
			      		$return 	= $this->bus_model->update($input,@$notif);

			      		$keterangan  = "Update Bus : ".@$input['nama_bus'];
            			$log         = $this->log_model->input($this->id_admin,$keterangan,'1');

			      		   $this->output
							   	->set_content_type('application/json')
							   	->set_output(json_encode($return));

        	}
		}

	}
	public function bus_tambahan()
	{
		$id 				= $this->input->get('id');
		$query      		= $this->bus_model->get_id($id);
		$id_gambar_group 	= $query['id_gambar_group'];

		$result = array('gambar' => $this->gambar_model->get_gambar_by_id($id_gambar_group));

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}

	public function makanan()
	{
		$post 	= $this->input->post();
		$list   = $this->makanan_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = '<a href="'.base_url('page/system/makanan/input/'.$person->id_makanan.'').'" >'.$person->makanan.'</a>';
 
            //add html for action
            $row[] = '
			<a id="DomDelete" id-delete="'.$person->id_makanan.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>

                  ';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->makanan_model->count_all(),
				   'recordsFiltered'=> $this->makanan_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->makanan_model->get_id($id);
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}

	public function makanan_post()
	{
   		$this->load->library('upload');
		$input = $this->input->post();

		if ($this->form_validation->run('makanan_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part' => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }else{
        	if(! @$input['id_makanan']){
				$data_gambar 		= "";
			    $number_of_files 	= sizeof(@$_FILES['gambar']['tmp_name']);
			    $files 				= @$_FILES['gambar'];
        		if($number_of_files == 0){
        			$id_gambar_group = $this->gambar_model->input_gambar_group('Makanan');
        			$input_gambar 	 = $this->gambar_model->input_gambar("default.jpg",$id_gambar_group);
        			$array 	= $this->makanan_model->input($input,$id_gambar_group);
					$this->output
					   	 ->set_content_type('application/json')
					   	 ->set_output(json_encode($array));
        		}else{
				    $config['upload_path'] 		= './gudang/upload/makanan/';
				    $config['allowed_types'] 	= 'jpg|png|jpeg';
					$config['max_size']     	= '5000';
					$config['max_width'] 		= '5000';
					$config['remove_spaces']    = TRUE;
					//create id_gambar_group gunakan mysql insert id
					$id_gambar_group = $this->gambar_model->input_gambar_group('Makanan');

				    for ($i = 0; $i < $number_of_files; $i++)
				    {
				      $_FILES['gambar']['name'] 		= $files['name'][$i];
				      $_FILES['gambar']['type'] 		= $files['type'][$i];
				      $_FILES['gambar']['tmp_name'] 	= $files['tmp_name'][$i];
				      $_FILES['gambar']['error'] 		= $files['error'][$i];
				      $_FILES['gambar']['size'] 		= $files['size'][$i];
				      $this->upload->initialize($config);	
				      if ($this->upload->do_upload('gambar'))
				      {
							$resize_img['source_image'] =  $this->upload->data('full_path');
							$resize_img['new_image'] 	= './gudang/upload/makanan/thumb/';
							$resize_img['width']        = 360;
							$notif_resize 	 			= resize_img($resize_img);
					   // masukan gambar by id group
							$data_gambar  = $this->upload->data('file_name');
							$input_gambar = $this->gambar_model->input_gambar($data_gambar,$id_gambar_group);
							
				      }else{
				      	$notif = ",tapi beberapa gambar tidak terupload , silahkan pastikan ukuran sudah sesuai.";
				      }
        			}
			      		$return 	= $this->makanan_model->input($input,$id_gambar_group,@$notif);
			      		$keterangan  = "Input Makanan : ".@$input['makanan'];
            			$log         = $this->log_model->input($this->id_admin,$keterangan,'1');
			      		   $this->output
							   	->set_content_type('application/json')
							   	->set_output(json_encode($return));

					}
        	}else{
				$data_gambar 		= "";
			    $number_of_files 	= sizeof(@$_FILES['gambar']['tmp_name']);
			    $files 				= @$_FILES['gambar'];
        		
				    $config['upload_path'] 		= './gudang/upload/makanan/';
				    $config['allowed_types'] 	= 'jpg|png|jpeg';
					$config['max_size']     	= '5000';
					$config['max_width'] 		= '5000';
					$config['remove_spaces']    = TRUE;

				    for ($i = 0; $i < $number_of_files; $i++)
				    {
				      $_FILES['gambar']['name'] 		= $files['name'][$i];
				      $_FILES['gambar']['type'] 		= $files['type'][$i];
				      $_FILES['gambar']['tmp_name'] 	= $files['tmp_name'][$i];
				      $_FILES['gambar']['error'] 		= $files['error'][$i];
				      $_FILES['gambar']['size'] 		= $files['size'][$i];
				      $this->upload->initialize($config);	
				      if ($this->upload->do_upload('gambar'))
				      {
							$resize_img['source_image'] =  $this->upload->data('full_path');
							$resize_img['new_image'] 	= './gudang/upload/makanan/thumb/';
							$resize_img['width']        = 360;
							$notif_resize 	 			= resize_img($resize_img);
					   // masukan gambar by id group
							$data_gambar  = $this->upload->data('file_name');
							$input_gambar = $this->gambar_model->input_gambar($data_gambar,$input['id_gambar_group']);
							
				      }else{
				      }
        			}
			      		$return 	= $this->makanan_model->update($input,@$notif);
			      		$keterangan  = "Update Makanan : ".@$input['nama_makanan'];
            			$log         = $this->log_model->input($this->id_admin,$keterangan,'1');
			      		   $this->output
							   	->set_content_type('application/json')
							   	->set_output(json_encode($return));

        	}
		}

	}
	public function makanan_tambahan()
	{
		$id 				= $this->input->get('id');
		$query      		= $this->makanan_model->get_id($id);
		$id_gambar_group 	= $query['id_gambar_group'];

		$result = array('gambar' => $this->gambar_model->get_gambar_by_id($id_gambar_group));

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function perlengkapan()
	{
		$post 	= $this->input->post();
		$list   = $this->perlengkapan_model->get_datatables($post);
        $data 	= array();
        $no 	= @$post['start']+1;
		$id 	= $this->urisegment;

		if(!$id){
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = '<a href="'.base_url('page/system/perlengkapan/input/'.$person->id_perlengkapan.'').'" >'.$person->nama_perlengkapan.'</a>';
            $row[] = '
			<a id="DomDelete" id-delete="'.$person->id_perlengkapan.'" modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>
                  ';
            $data[] = $row;
        }

		$result = ['draw' => $post['draw'],
				   'recordsTotal' => $this->perlengkapan_model->count_all(),
				   'recordsFiltered'=> $this->perlengkapan_model->count_filtered(),
				   'data' => $data];

		}else{
			$result	=	$this->perlengkapan_model->get_id($id);
		}

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}		

	public function perlengkapan_post()
	{
		$input = $this->input->post();

        if ($this->form_validation->run('perlengkapan_post') == FALSE)
        {
            $array = array('messages'    => 'Form yang anda input tidak valid , silahkan coba lagi!',
                           'error_part'  => $this->form_validation->error_array());
			$this->output
		   	 ->set_content_type('application/json')
     	     ->set_status_header(404)
		   	 ->set_output(json_encode($array));
        }else{
		if (! @$input['id_perlengkapan']) {
			$result = $this->perlengkapan_model->input($input);
			$keterangan  = "Input Perlengkapan : ".@$input['nama_perlengkapan'];
        	$log         = $this->log_model->input($this->id_admin,$keterangan,'1');


			$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
		}else{
			$result = $this->perlengkapan_model->update($input);
			$keterangan  = "Update Perlengkapan : ".@$input['nama_perlengkapan'];
        	$log         = $this->log_model->input($this->id_admin,$keterangan,'1');
			$this->output
			   	 ->set_content_type('application/json')
			   	 ->set_output(json_encode($result));
		}
		}

	}
	public function bus_gambar_delete($id)
	{

		$query 	= $this->gambar_model->get_delete($id);
		$keterangan  = "Delete Gambar Bus : ".$query->nama_bus;
		$result = $this->gambar_model->delete($id);
		
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');

		@unlink('gudang/upload/bus/'.$query->gambar);
		@unlink('gudang/upload/bus/thumb/'.$query->gambar);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function bus_delete($id)
	{
		$query 	= $this->bus_model->get_delete($id);
		$result = $this->bus_model->delete($id);
		$keterangan  = "Delete Bus : ".$query->nama_bus;
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function perlengkapan_delete($id)
	{
		$id = $this->uri->segment(4);

		$result = $this->perlengkapan_model->delete($id);
		$keterangan  = "Delete Perlengkapan ";
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function makanan_delete($id)
	{
		$id = $this->uri->segment(4);

		$result = $this->makanan_model->delete($id);
		$keterangan  = "Delete Makanan";
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function makanan_gambar_delete($id)
	{

		$query 	= $this->gambar_model->get_delete($id);
		$result = $this->gambar_model->delete($id);
		$keterangan  = "Delete Gambar Makanan : ".$query->nama_makanan;
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		@unlink('gudang/upload/makanan/'.$query->gambar);
		@unlink('gudang/upload/makanan/thumb/'.$query->gambar);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function pesawat_gambar_delete($id)
	{

		$query 	= $this->gambar_model->get_delete($id);
		$result = $this->gambar_model->delete($id);
		$keterangan  = "Delete Gambar Pesawat : ".$query->nama_pesawat;
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		@unlink('gudang/upload/pesawat/'.$query->gambar);
		@unlink('gudang/upload/pesawat/thumb/'.$query->gambar);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function pesawat_delete($id)
	{
		$query 			= $this->pesawat_model->get_delete($id);
		$querygambar 	= $this->gambar_model->get_by_id_group($query->id_gambar_group);
		$keterangan  = "Delete Pesawat : ".$query->nama_pesawat;
		foreach ($querygambar as $r) {
			@unlink('gudang/upload/pesawat/'.$r->gambar);
			@unlink('gudang/upload/pesawat/thumb/'.$r->gambar);
		}
		$resultgambar 	= $this->gambar_model->delete_by_group($query->id_gambar_group);
		$result 		= $this->pesawat_model->delete($id);

        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');

			@unlink('gudang/upload/pesawat/'.$query->logo);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function hotel_gambar_delete($id)
	{

		$query 	= $this->gambar_model->get_delete($id);
		$keterangan  = "Delete Gambar Hotel : ".$query->nama_hotel;
		$result = $this->gambar_model->delete($id);
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		@unlink('gudang/upload/hotel/'.$query->gambar);
		@unlink('gudang/upload/hotel/thumb/'.$query->gambar);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
	public function hotel_delete($id)
	{
		$query 			= $this->hotel_model->get_delete($id);
		$keterangan  = "Delete Hotel : ".$query->nama_hotel;
		$querygambar 	= $this->gambar_model->get_by_id_group($query->id_gambar_group);
        $log         = $this->log_model->input($this->id_admin,$keterangan,'1');
		foreach ($querygambar as $r) {
			@unlink('gudang/upload/hotel/'.$r->gambar);
			@unlink('gudang/upload/hotel/thumb/'.$r->gambar);
		}
		$resultgambar 	= $this->gambar_model->delete_by_group($query->id_gambar_group);
		$result 		= $this->hotel_model->delete($id);

		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($result));
	}
}

/* End of file Master.php */
/* Location: ./application/controllers/manage/api/Master.php */