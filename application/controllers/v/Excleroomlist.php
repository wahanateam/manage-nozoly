<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excleroomlist extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('V2_report_model');
        $this->load->library('excel');
        
    }

    public function index()
    {
            $jadwal             = $this->input->get('tanggal');
            $start              = 3; 
            $roomlist           = $this->V2_report_model->report_roomlist($jadwal);
            $kondisi_roomlist   = $this->V2_report_model->kondisi_roomlist($jadwal);
            // print_r($kondisi_roomlist);
            $objPHPExcel = new PHPExcel();
             $error = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FF0000')
                ));    
                
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'CCCCCC')
                    ));    
                
                $isinya = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ));

                $judulnya = array(
                    'font'  => array(
                        'bold'  => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ));
            $objPHPExcel->getProperties()->setCreator("PT. WAHANA HAJI UMRAH")
                                         ->setDescription("data roomlist.");
            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($judulnya);
            $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A1:F2');
            $objPHPExcel->getActiveSheet()->setCellValue('A1', "RoomList ".tgl_indo($jadwal));
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $start, "No")
                                          ->setCellValue('B'. $start, "SEX")
                                          ->setCellValue('C'. $start, "NAMA SESUAI PASPOR")
                                          ->setCellValue('D'. $start, "STATUS")
                                          ->setCellValue('E'. $start, "TIPE KAMAR")
                                          ->setCellValue('F'. $start, "NOMOR KAMAR");
            $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':F'.$start)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(16);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);           
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);        
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $i      = $start+1;
            $no     = 1;
            $no2    = 1;
            $aa = count($roomlist)+3;
            foreach ($kondisi_roomlist as $key) {
                if($no == 1){
                      $ii = $i;
                } else{
                    $ii += $end;
                }
            // echo "kondisi start: ". $ii."<br>";
            // echo "kondisi end : ".($ii+$key->count-1)."<br>";
            $end = $key->count;
            // echo "count : ". $end."<br>";
            // echo " ----------------------- <br>";
             $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('E'.$ii.':E'.($ii+$key->count-1));
             $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('F'.$ii.':F'.($ii+$key->count-1));
            $no++;
           
            }
 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$aa)->applyFromArray($isinya);
            foreach ($roomlist as $r){
                if($r->jenis_kelamin == "Perempuan"){
                    $jenis_kelamin = "Ms";
                }else if($r->jenis_kelamin == "Laki-laki"){
                    $jenis_kelamin = "Mr";
                }else{
                    $jenis_kelamin = "-";
                }          

                if($r->nama_passport == "null" || $r->nama_passport == ""){
                    $nama_passport = "-";
                }else if($r->nama_passport != "" || $r->nama_passport != "null"){
                    $nama_passport = $r->nama_passport;
                }else{
                    $nama_passport = "-";
                }


                if($r->status_keluarga == "null" || $r->status_keluarga == ""){
                    $status_keluarga = "-";
                }else if($r->status_keluarga != "" || $r->status_keluarga != "null"){
                    $status_keluarga = $r->status_keluarga;
                }else{
                    $status_keluarga = "-";
                }
               $objPHPExcel->getActiveSheet()->setCellValue('A' .  $i, $no2);
               $objPHPExcel->getActiveSheet()->setCellValue('B' .  $i, $jenis_kelamin);
               $objPHPExcel->getActiveSheet()->setCellValue('C' .  $i, $nama_passport);
               $objPHPExcel->getActiveSheet()->setCellValue('D' .  $i, $status_keluarga);
               $objPHPExcel->getActiveSheet()->setCellValue('E' .  $i, $r->jenis_kamar);
               $objPHPExcel->getActiveSheet()->setCellValue('F' .  $i, '');
                $i++;
                $no2++;
            }

            $objPHPExcel->setActiveSheetIndex(0);
            // $objPHPExcel->getActiveSheet()->setTitle('ROOM LIST '.$jadwal);
            // $objPHPExcel->getActiveSheet()
            //     ->getHeaderFooter()->setOddHeader('&C&24&K0000FF&B&U&A');
            // $objPHPExcel->getActiveSheet()
            //     ->getHeaderFooter()->setEvenHeader('&C&24&K0000FF&B&U&A');
            // $objPHPExcel->getActiveSheet()
            //     ->getHeaderFooter()->setOddFooter('&R&D &T&C&F&LPage &P / &N');
            // $objPHPExcel->getActiveSheet()
            //     ->getHeaderFooter()->setEvenFooter('&L&D &T&C&F&RPage &P / &N');
            $callStartTime = microtime(true);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save(FCPATH.'reprotexcle/Roomlist '.tgl_indo($jadwal).'.xlsx');
            $callEndTime = microtime(true);
            $callTime = $callEndTime - $callStartTime;
            $callStartTime = microtime(true);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save(FCPATH.'reprotexcle/Roomlist '.tgl_indo($jadwal).'.xls');
            $callEndTime = microtime(true);
            $callTime = $callEndTime - $callStartTime;

            echo 'Roomlist '.tgl_indo($jadwal).'.xlsx';
    }
}
