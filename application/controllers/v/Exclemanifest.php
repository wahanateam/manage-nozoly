<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exclemanifest extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('V2_report_model');
        $this->load->library('excel');
        
    }

    public function index()
    {
        $jadwal = $this->input->get('tanggal');
        $manifest = $this->V2_report_model->report_manifest($jadwal);
        
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();
            $error = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FF0000')
                ));    
                
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'CCCCCC')
                    ));    
                
                $isinya = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ));
            // Set document properties
            $objPHPExcel->getProperties()->setCreator("PT. WAHANA HAJI UMRAH")
                                         ->setDescription("data manifest.");
            // Create a first sheet
            $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A1:A2')
                        // ->mergeCells('AB1:AD1')
                        // ->mergeCells('AB1:AD1')
                        ->mergeCells('AB1:AB2')
                        ->mergeCells('AC1:AE1')
                        ->mergeCells('AF1:AG1')
                        ->mergeCells('AH1:AL1')
                        // ->mergeCells('AE1:AF1')
                        // ->mergeCells('AG1:AK1')
                        ->mergeCells('B1:B2')
                        ->mergeCells('C1:C2')
                        ->mergeCells('D1:D2')
                        ->mergeCells('E1:E2')
                        ->mergeCells('F1:F2')
                        ->mergeCells('G1:G2')
                        ->mergeCells('H1:H2')
                        ->mergeCells('I1:I2')
                        ->mergeCells('J1:J2')
                        ->mergeCells('K1:K2')
                        ->mergeCells('L1:L2')
                        ->mergeCells('M1:M2')
                        ->mergeCells('N1:N2')
                        ->mergeCells('O1:O2')
                        ->mergeCells('P1:P2')
                        ->mergeCells('Q1:Q2')
                        ->mergeCells('R1:R2')
                        ->mergeCells('S1:AA1');
            $objPHPExcel->getActiveSheet()->setCellValue('A1', "No")
                                          ->setCellValue('B1', "SEX")
                                          ->setCellValue('C1', "NAMA SESUAI PASPOR")
                                          ->setCellValue('D1', "NAMA LENGKAP")
                                          ->setCellValue('E1', "NAMA PANGGILAN")
                                          ->setCellValue('F1', "NAMA AYAH")
                                          ->setCellValue('G1', "STATUS")
                                          ->setCellValue('H1', "TEMPAT LAHIR")
                                          ->setCellValue('I1', "TANGGAL LAHIR")
                                          ->setCellValue('J1', "UMUR")
                                          ->setCellValue('K1', "KETERANGAN MAHRAM")
                                          ->setCellValue('L1', "PEKERJAAN")
                                          ->setCellValue('M1', "PENDIDIKAN")
                                          ->setCellValue('N1', "ALAMAT")
                                          ->setCellValue('O1', "NOMOR HP")
                                          ->setCellValue('P1', "NO PASSPORT")
                                          ->setCellValue('Q1', "TEMPAT DIKELUARKAN")
                                          ->setCellValue('R1', "MASA BERLAKU")
                                          ->setCellValue('S1', "KELENGKAPAN DOKUMEN")
                                          ->setCellValue('S2', "KTP")
                                          ->setCellValue('T2', "KTP ORTU")
                                          ->setCellValue('U2', "AKTE")
                                          ->setCellValue('V2', "IJAZAH")
                                          ->setCellValue('W2', "KK")
                                          ->setCellValue('X2', "BUKU NIKAH")
                                          ->setCellValue('Y2', "PASSPORT")
                                          ->setCellValue('Z2', "BUKU KUNING")
                                          ->setCellValue('AA2', "PHOTO")
                                          ->setCellValue('AB1', "PAKET")
                                          ->setCellValue('AC2', "DOUBLE")
                                          ->setCellValue('AD2', "TRIPLE")
                                          ->setCellValue('AE2', "QUAD")
                                          ->setCellValue('AF2', "MADINAH")
                                          ->setCellValue('AG2', "MAKKAH")
                                          ->setCellValue('AH2', "ABAYA")
                                          ->setCellValue('AI2', "KOKO")
                                          ->setCellValue('AJ2', "JAKET")
                                          ->setCellValue('AK2', "KAOS")
                                          ->setCellValue('AL2', "BERGO")
                                          ->setCellValue('AC1', "TIPE PAKET")
                                          ->setCellValue('AF1', "HOTEL")
                                          ->setCellValue('AH1', "UKURAN SERAGAM");
                                          
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);           
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17.71);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17.71);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);        
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->getAlignment()->setWrapText(true); 
            $objPHPExcel->getActiveSheet()->getStyle('A1:AL2')->applyFromArray($styleArray);
            
        //    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
            // Add data
            $i = 3;
            $no = 1;
            $ktp = "";
            $akte_lahir = "";
            $kk = "";
            $buku_nikah = "";
            $passport = "";
            $buku_kuning = "";
            $foto = "";
            $ktp_ortu = "";
            $ijazah = "";
            $Double = "";
            $Triple = "";
            $Quad = "";
            $aa = count($manifest)+2;
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':AL'.$aa)->applyFromArray($isinya);
            foreach ($manifest as $r){
                if($r->jenis_kelamin == "Perempuan"){
                    $bergo         = $r->size_baju;
                    $abaya         = $r->size_baju; 
                    $jaket         = $r->size_baju; 
                    $kaos          = $r->size_baju; 
                    $koko          = "-"; 
                    
                    $jenis_kelamin = "Ms";
                }else if($r->jenis_kelamin == "Laki-laki"){
                    $jenis_kelamin = "Mr";
                    $bergo         = "-";
                    $abaya         = "-";
                    $jaket         = $r->size_baju; 
                    $kaos          = $r->size_baju; 
                    $koko          = $r->size_baju; 
                }else{
                    $jenis_kelamin = $r->jenis_kelamin;
                    if($r->jenis_kelamin == "Ms"){
                        $bergo         = $r->size_baju;
                        $abaya         = $r->size_baju; 
                        $jaket         = $r->size_baju; 
                        $kaos          = $r->size_baju; 
                        $koko          = "-"; 
                    }else{
                        $bergo         = "-";
                        $abaya         = "-";
                        $jaket         = $r->size_baju; 
                        $kaos          = $r->size_baju; 
                        $koko          = $r->size_baju; 
                    }
                }

                $now = date('Y-m-d');
                $datetime1 = date_create(@$r->tanggal_lahir);
                $datetime2 = date_create($now);
                $interval = date_diff($datetime1, $datetime2);

                if($r->ktp != null){$ktp = "Ѵ";}else{$ktp = "-";}
                if($r->akte_lahir != null){$akte_lahir = "Ѵ";}else{$akte_lahir = "-";}
                if($r->kk != null){$kk = "Ѵ";}else{$kk = "-";}
                if($r->buku_nikah != null){$buku_nikah = "Ѵ";}else{$buku_nikah = "-";}
                if($r->passport != null){$passport = "Ѵ";}else{$passport = "-";}
                if($r->buku_kuning != null){$buku_kuning = "Ѵ";}else{$buku_kuning = "-";}
                if($r->foto != null){$foto = "Ѵ";}else{$foto = "-";}
                if($r->ktp_ortu != null){$ktp_ortu = "Ѵ";}else{$ktp_ortu = "-";}
                if($r->ijazah != null){$ijazah = "Ѵ";}else{$ijazah = "-";}
                if($r->tipe_paket == "Double"){$Double = "Ѵ";}else{$Double = "";}
                if($r->tipe_paket == "Triple"){$Triple = "Ѵ";}else{$Triple = "";}
                if($r->tipe_paket == "Quad"){$Quad = "Ѵ";}else{$Quad = "";}
                if($jenis_kelamin == "Mr"){ 
                    $keterangan_mahram = " - ";
                 }else{
                     if($interval->y+1 > 45 && !$r->keterangan_mahram || $r->keterangan_mahram == "null"){
                        $keterangan_mahram = " - ";
                    }else{
                        $keterangan_mahram = $r->keterangan_mahram;
                    }
                }
                $objPHPExcel->getActiveSheet()->setCellValue('A' .  $i, $no);
                $objPHPExcel->getActiveSheet()->setCellValue('B' .  $i, $jenis_kelamin);
                if($r->nama_passport == "" || $r->nama_passport == "null"){
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($error);
                  }
                $objPHPExcel->getActiveSheet()->setCellValue('D' .  $i, $r->nama_lengkap == "null" ? '' : $r->nama_lengkap);
                $objPHPExcel->getActiveSheet()->setCellValue('C' .  $i, $r->nama_passport == "null" ? '' : $r->nama_passport);
                if($r->nama_panggilan == "" || $r->nama_panggilan == "null"){
                    $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($error);
                  }
                $objPHPExcel->getActiveSheet()->setCellValue('E' .  $i, $r->nama_panggilan == "null" ? '' : $r->nama_panggilan);
                $objPHPExcel->getActiveSheet()->setCellValue('F' .  $i, $r->nama_ayah);
                if($r->status_keluarga == "" || $r->status_keluarga == "null"){
                    $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray($error);
                  }
                $objPHPExcel->getActiveSheet()->setCellValue('G' .  $i, $r->status_keluarga == "null" ? '' : $r->status_keluarga);
                $objPHPExcel->getActiveSheet()->setCellValue('H' .  $i, $r->tempat_lahir);
                $objPHPExcel->getActiveSheet()->setCellValue('I' .  $i, tgl_indo($r->tanggal_lahir));
                $objPHPExcel->getActiveSheet()->setCellValue('J' .  $i, $interval->y+1);
                if($keterangan_mahram == ""){
                  $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray($error);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('K' .  $i, $keterangan_mahram);
                $objPHPExcel->getActiveSheet()->setCellValue('L' .  $i, $r->jenis_pekerjaan);
                $objPHPExcel->getActiveSheet()->setCellValue('M' .  $i, $r->pendidikan_terakhir);
                if($r->alamat_rumah == "" || $r->alamat_rumah == "null"){
                    $objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($error);
                  }
                $objPHPExcel->getActiveSheet()->setCellValue('N' .  $i,  $r->alamat_rumah == "null" ? '' : $r->alamat_rumah);
                if($r->no_telp == "" || $r->no_telp == "null"){
                    $objPHPExcel->getActiveSheet()->getStyle('O'.$i)->applyFromArray($error);
                  }
                $objPHPExcel->getActiveSheet()->setCellValue('O' .  $i, $r->no_telp == "null" ? '' : $r->no_telp);
                if($r->no_passport == "" || $r->no_passport == "null"){
                    $objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($error);
                  }
                $objPHPExcel->getActiveSheet()->setCellValue('P' .  $i, $r->no_passport == "null" ? '' : $r->no_passport);
                if($r->tempat_dikeluarkan == "" || $r->tempat_dikeluarkan == "null"){
                    $objPHPExcel->getActiveSheet()->getStyle('Q'.$i)->applyFromArray($error);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('Q' .  $i, $r->tempat_dikeluarkan == "null" ? '' : $r->tempat_dikeluarkan);
                if($r->masa_berlaku == "" || $r->masa_berlaku == "0000-00-00"){
                    $objPHPExcel->getActiveSheet()->getStyle('R'.$i)->applyFromArray($error);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('R' .  $i, $r->masa_berlaku == "0000-00-00" ? '' : tgl_indo($r->masa_berlaku));
                $objPHPExcel->getActiveSheet()->setCellValue('S' .  $i, $ktp);
                $objPHPExcel->getActiveSheet()->setCellValue('T' .  $i, $ktp_ortu );
                $objPHPExcel->getActiveSheet()->setCellValue('U' .  $i, $akte_lahir);
                $objPHPExcel->getActiveSheet()->setCellValue('V' .  $i, $ijazah );
                $objPHPExcel->getActiveSheet()->setCellValue('W' .  $i, $kk);
                $objPHPExcel->getActiveSheet()->setCellValue('x' .  $i, $buku_nikah);
                $objPHPExcel->getActiveSheet()->setCellValue('Y' .  $i, $passport);
                $objPHPExcel->getActiveSheet()->setCellValue('Z' .  $i, $buku_kuning );
                $objPHPExcel->getActiveSheet()->setCellValue('AA' .  $i, $foto);
                $objPHPExcel->getActiveSheet()->setCellValue('AB' . $i, $r->nama_paket);
                $objPHPExcel->getActiveSheet()->setCellValue('AC' . $i, $Double);
                $objPHPExcel->getActiveSheet()->setCellValue('AD' . $i, $Triple);
                $objPHPExcel->getActiveSheet()->setCellValue('AE' . $i, $Quad);
                $objPHPExcel->getActiveSheet()->setCellValue('AF' . $i, $r->nama_hotel_makkah);
                $objPHPExcel->getActiveSheet()->setCellValue('AG' . $i, $r->nama_hotel_madinah);
                $objPHPExcel->getActiveSheet()->setCellValue('AH' . $i, $abaya);
                $objPHPExcel->getActiveSheet()->setCellValue('AI' . $i, $koko);
                $objPHPExcel->getActiveSheet()->setCellValue('AJ' . $i, $jaket);
                $objPHPExcel->getActiveSheet()->setCellValue('AK' . $i, $kaos);
                $objPHPExcel->getActiveSheet()->setCellValue('AL' . $i, $bergo);
                $i++;
                $no++;
            }

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('Manifest '.$jadwal);
            $objPHPExcel->getActiveSheet()
                ->getHeaderFooter()->setOddHeader('&C&24&K0000FF&B&U&A');
            $objPHPExcel->getActiveSheet()
                ->getHeaderFooter()->setEvenHeader('&C&24&K0000FF&B&U&A');
            $objPHPExcel->getActiveSheet()
                ->getHeaderFooter()->setOddFooter('&R&D &T&C&F&LPage &P / &N');
            $objPHPExcel->getActiveSheet()
                ->getHeaderFooter()->setEvenFooter('&L&D &T&C&F&RPage &P / &N');
            $callStartTime = microtime(true);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save(FCPATH.'reprotexcle/manifest '.tgl_indo($jadwal).'.xlsx');
            $callEndTime = microtime(true);
            $callTime = $callEndTime - $callStartTime;
            $callStartTime = microtime(true);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save(FCPATH.'reprotexcle/manifest '.tgl_indo($jadwal).'.xls');
            $callEndTime = microtime(true);
            $callTime = $callEndTime - $callStartTime;

            echo 'manifest '.tgl_indo($jadwal).'.xlsx';
    }
}
