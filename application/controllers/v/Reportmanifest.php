<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportmanifest extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('report_model');
    }

    public function index()
    {
        $jadwal = $this->input->get('tanggal');
        $data['datamanifest']   = $this->report_model->report_manifest($jadwal);
        $data['jadwal']         =  $jadwal;
        $this->load->view('v/reportmanifest',$data);
    }
}
