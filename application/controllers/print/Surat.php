<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('surat_cuti_jamaah_model');
	}
	public function index()
	{
		$id = $this->input->get();
		$data['core'] 		= $this->surat_cuti_jamaah_model->get_id($id['id']);

		if($data['core']->nama_penanggungjawab == "Rahmaji Asmuri"){
			$data['penanggung_jawab'] = ['nama' => 'Rahmaji Asmuri', 'jabatan' => 'Deputy'];
		}elseif($data['core']->nama_penanggungjawab == "Muhammad Faza Fathurrahman"){
			$data['penanggung_jawab'] = ['nama' => 'Muhammad Faza Fathurrahman', 'jabatan' => 'Direktur'];
		}

		if($data['core']->jenis == "Surat Cuti Jamaah"){
			$this->load->view('print/surat_cuti_jamaah', $data);
		}elseif($data['core']->jenis == "Surat Perpanjangan Passport"){
			$this->load->view('print/surat_perpanjangan_passport', $data);
		}elseif($data['core']->jenis == "Surat Passport Sukukata"){
			$this->load->view('print/surat_pembuatan_paspor', $data);
		}
	}

}

/* End of file Surat.php */
/* Location: ./application/controllers/print/Surat.php */