<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jamaah extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('data_jamaah_model');
	}
	public function index()
	{
			$id = $this->input->get('id');
			$data['result']	=	$this->data_jamaah_model->get_id($id);
			// echo count($array);
			// for ($i=0; $i < count($array); $i++) { 
			// 	echo $array[$i][1];
			// }
			$this->load->view('print/jamaah',$data);
	}

}

/* End of file jamaah.php */
/* Location: ./application/controllers/print/jamaah.php */