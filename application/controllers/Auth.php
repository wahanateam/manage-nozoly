<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->FolderAuth = 'halaman/auth/';
		
	}

	public function index()
	{
		if ($this->session->has_userdata('logged_in') == TRUE) {
			redirect('page/home/dashboard');
		}
		$data['konten'] = $this->FolderAuth.'index';
		$this->load->view('halaman/auth/main',$data);	
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('auth');
	}
}

/* End of file index.php */
/* Location: ./application/controllers/manage/index.php */