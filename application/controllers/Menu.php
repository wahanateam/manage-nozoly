<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model');
	}
	public function get_menu()
	{
		$id = $this->session->userdata('id_level');
		$res = $this->dashboard_model->get_menu($id);
		
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($res));
	}

	public function cari_menu()
	{
		$id = $this->session->userdata('id_level');
		$cari = $this->input->get('cari');
		$res = $this->dashboard_model->cari_menu($id,$cari);
		
		$this->output
		   	 ->set_content_type('application/json')
		   	 ->set_output(json_encode($res));
	}

}
