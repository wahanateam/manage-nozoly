<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Router extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model');
	}
	private function router_setup($namefile,$uri= "",$uri2 = "")
	{
		$path 	= uri_string();
		
		$rep 	= str_replace("router","page",$path);
		
		$cek    = explode("/", $rep);

		$rep 	= $cek[0]."/".$cek[1]."/".$cek[2];
		
		$id     = $this->session->userdata('id_level');
		$kondisi = $this->dashboard_model->check_akses_menu($rep,$id);
		
		if ($path == "router/absen/manual") {
			$default_file =  $namefile.'/'.$uri.'/index';
			$folder = "halaman/".$default_file;
			$this->load->view($folder);
		}elseif($path == "router/home/notif"){
			$default_file =  $namefile.'/'.$uri.'/index';
			$folder = "halaman/".$default_file;
			$this->load->view($folder);
		}
		elseif ($kondisi > 0 || $path == "router/master/admin/input") {
		if($uri2 != ""){
			$default_file = $namefile.'/'.$uri.'/'.$uri2;
		}else if($uri != ""){
			$default_file = $namefile.'/'.$uri.'/index';
		}else{
			$default_file = $namefile.'/index';
		}
		$folder = "halaman/".$default_file;
		$this->load->view($folder);
		}else{
			// echo "Maaf halaman tidak ditemukan / sudah didelete.";
			$this->load->view('halaman/404.php');
		}
	}
	public function _remap($method)
	{
		$uri 	= $this->uri->segment(3);
		$uri2 	= $this->uri->segment(4);
		//die($uri);
        $this->router_setup($method,$uri,$uri2);
	}

}
