<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
        'admin_insert_post' => array(
             array('field' => 'nama'             , 'label' => 'Nama'                 ,'rules' => 'trim|required'),
             array('field' => 'username'         , 'label' => 'Username'             ,'rules' => 'trim|required'),
             array('field' => 'password'         , 'label' => 'Password'             ,'rules' => 'trim|required')
             ),
         'admin_post' => array(
             array('field' => 'nama'             , 'label' => 'Nama'                 ,'rules' => 'trim|required'),
             array('field' => 'username'         , 'label' => 'Username'             ,'rules' => 'trim|required')
             ),
         'data_travel_post' => array(
             array('field' => 'nama_travel'      , 'label' => 'Nama Travel'          ,'rules' => 'trim|required'),
             array('field' => 'alamat'           , 'label' => 'Alamat'               ,'rules' => 'trim|required'),
             array('field' => 'status'           , 'label' => 'Status'               ,'rules' => 'trim|required')
             ),
         'order_post' => array(
             array('field' => 'status_pembayaran'             , 'label' => 'Status Pembayaran'                 ,'rules' => 'trim|required'),
             ),
         'opini_post' => array(
             array('field' => 'opini'             , 'label' => 'Opini'                 ,'rules' => 'trim|required'),
             ),
         'bus_post' => array(
             array('field' => 'nama_bus'         , 'label' => 'Nama Bus'             ,'rules' => 'trim|required'),
             array('field' => 'keterangan'         , 'label' => 'Keterangan'             ,'rules' => 'trim|required')
             ),
         'hotel_post' => array(
             array('field' => 'nama_hotel'         , 'label' => 'Nama Hotel'             ,'rules' => 'trim|required'),
             array('field' => 'keterangan'         , 'label' => 'Keterangan'             ,'rules' => 'trim|required')
             ),
         'kategori_post' => array(
             array('field' => 'nama_kategori'             , 'label' => 'Nama Kategori'         ,'rules' => 'trim|required'),
             array('field' => 'type'             , 'label' => 'Type'         ,'rules' => 'trim|required')
             ),
         'makanan_post' => array(
             array('field' => 'makanan'             , 'label' => 'Nama Makanan'         ,'rules' => 'trim|required')
             ),
         'level_post' => array(
             array('field' => 'nama_level'          , 'label' => 'Nama Level'    ,'rules' => 'trim|required'),
             array('field' => 'status'             , 'label' => 'Status'         ,'rules' => 'trim|required')
             ),
         'level_duta_post' => array(
             array('field' => 'nama_level'          , 'label' => 'Nama Level'    ,'rules' => 'trim|required'),
             array('field' => 'fee'             , 'label' => 'fee'         ,'rules' => 'trim|required')
             ),
         'mata_uang_post' => array(
             array('field' => 'nominal'             , 'label' => 'Kode Mata Uang'         ,'rules' => 'trim|required')
             ),
         'pesawat_post' => array(
             array('field' => 'nama_pesawat'     , 'label' => 'Nama Pesawat'         ,'rules' => 'trim|required'),
             array('field' => 'keterangan'         , 'label' => 'Keterangan'             ,'rules' => 'trim|required')
             ),
         'profile_post' => array(
             array('field' => 'nama_perusahaan'     , 'label' => 'Nama Perusahaan'         ,'rules' => 'trim|required'),
             array('field' => 'judul_web'         , 'label' => 'Judul Website'         ,'rules' => 'trim|required'),
             array('field' => 'des_web'             , 'label' => 'Deskripsi Website'     ,'rules' => 'trim|required'),
             array('field' => 'keyword_web'         , 'label' => 'Keyword Website'         ,'rules' => 'trim|required')
             ),
         'menu_post' => array(
             array('field' => 'nama_menu'         , 'label' => 'Nama Perusahaan'         ,'rules' => 'trim|required'),
             ),
         'menu_admin_post' => array(
             array('field' => 'nama_menu'         , 'label' => 'Nama Perusahaan'         ,'rules' => 'trim|required')
             ),
         'paket_post' => array(
             array('field' => 'nama_paket'         , 'label' => 'Nama Paket'             ,'rules' => 'trim|required'),
             array('field' => 'harga'             , 'label' => 'Harga'                 ,'rules' => 'trim|required'),
             array('field' => 'berangkat'         , 'label' => 'Field Berangkat'        ,'rules' => 'trim|required'),
             array('field' => 'pulang'             , 'label' => 'Field Pulang'            ,'rules' => 'trim|required')
             ),
         'paket_halaltour_post' => array(
             array('field' => 'nama_paket'         , 'label' => 'Nama Paket'             ,'rules' => 'trim|required'),
             array('field' => 'harga'             , 'label' => 'Harga'                 ,'rules' => 'trim|required'),
             ),
         'jadwal_post' => array(
             array('field' => 'id_paket'            , 'label' => 'Paket'                 ,'rules' => 'trim|required'),
             array('field' => 'tanggal'             , 'label' => 'Tanggal'                 ,'rules' => 'trim|required'),
             array('field' => 'tanggal_pulang'      , 'label' => 'Tanggal Kepulangan'      ,'rules' => 'trim|required')
             ),
         'jadwal_haji_post' => array(
             array('field' => 'tanggal'             , 'label' => 'Tanggal'                 ,'rules' => 'trim|required'),
             array('field' => 'tanggal_pulang'      , 'label' => 'Tanggal Kepulangan'      ,'rules' => 'trim|required')
             ),
         'slider_post' => array(
             array('field' => 'caption'         , 'label' => 'Caption'             ,'rules' => 'trim|required'),
             array('field' => 'status'         , 'label' => 'Status'             ,'rules' => 'trim|required')
            ),
         'sliderapp_post' => array(
             array('field' => 'gambar'         , 'label' => 'Gambar'             ,'rules' => 'trim|required'),
         ),
         'berita_post' => array(
             array('field' => 'judul'             , 'label' => 'Judul'                 ,'rules' => 'trim|required'),
             array('field' => 'konten'             , 'label' => 'Konten'                 ,'rules' => 'trim|required'),
             array('field' => 'id_kategori_artikel' , 'label' => 'Kategori'                 ,'rules' => 'trim|required')
             ),
         'perlengkapan_post' => array(
             array('field' => 'nama_perlengkapan'             , 'label' => 'Nama Perlengkapan'                 ,'rules' => 'trim|required')
             ),
         'album_post' => array(
             array('field' => 'nama_album'         , 'label' => 'Nama Album'             ,'rules' => 'trim|required'),
             array('field' => 'tanggal'         , 'label' => 'Tanggal'             ,'rules' => 'trim|required')
             ),
         'blockquote_post' => array(
             array('field' => 'judul'             , 'label' => 'Judul'                 ,'rules' => 'trim|required'),
             array('field' => 'blockquote'         , 'label' => 'Blockquote'             ,'rules' => 'trim|required')
             ),
         'jawab_post' => array(
             array('field' => 'komentar'             , 'label' => 'Komentar'                 ,'rules' => 'trim|required')
			),
         'komen_post' => array(
             array('field' => 'komen'             , 'label' => 'Komentar'                 ,'rules' => 'trim|required')
            ),
         'embed_post' => array(
             array('field' => 'nama_album'         , 'label' => 'Nama Album'             ,'rules' => 'trim|required'),
             array('field' => 'tanggal'         , 'label' => 'Tanggal'             ,'rules' => 'trim|required')
            ),
         'pengumuman_post' => array(
             array('field' => 'judul'         , 'label' => 'Judul'             ,'rules' => 'trim|required'),
             array('field' => 'message'       , 'label' => 'Message'           ,'rules' => 'trim|required')
            ),
        'data_duta_insert_post' => array(
             array('field' => 'nama_lengkap'     , 'label' => 'Nama'          ,'rules' => 'trim|required'),
             array('field' => 'alamat'           , 'label' => 'Alamat'        ,'rules' => 'trim|required'),
             array('field' => 'email'            , 'label' => 'Email'         ,'rules' => 'trim|required')
             ),
         'data_duta_post' => array(
             array('field' => 'nama_lengkap'     , 'label' => 'Nama'          ,'rules' => 'trim|required'),
             array('field' => 'alamat'           , 'label' => 'Alamat'        ,'rules' => 'trim|required'),
             array('field' => 'email'            , 'label' => 'Email'         ,'rules' => 'trim|required')
             ),
         'agenda_post' => array(
             array('field' => 'tanggal'             , 'label' => 'Tanggal'          ,'rules' => 'trim|required'),
             array('field' => 'tanggal_reminder'    , 'label' => 'Tanggal Reminder'          ,'rules' => 'trim|required'),
             array('field' => 'type'                , 'label' => 'Type'      ,'rules' => 'trim|required')
             ),
         'sliderpromo_post' => array(
             array('field' => 'judul'               , 'label' => 'Judul'          ,'rules' => 'trim|required'),
             array('field' => 'status'               , 'label' => 'Status'          ,'rules' => 'trim|required'),
             ),
         'hargakhusus_post' => array(
             array('field' => 'tanggal'               , 'label' => 'Tanggal'          ,'rules' => 'trim|required'),
             ),
         'diskon_post' => array(
             array('field' => 'kode'               , 'label' => 'Kode'          ,'rules' => 'trim|required'),
             ),
        'testimoni_post' => array(
             array('field' => 'nama'             , 'label' => 'Nama'                 ,'rules' => 'trim|required'),
             ),
         'rekap_fee_post' => array(
             array('field' => 'nominal'             , 'label' => 'Nominal'                 ,'rules' => 'trim|required')
             ),
	);