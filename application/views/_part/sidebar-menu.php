<div id="sidebar-menu" class="main_menu_side hidden-print main_menu" ng-controller="sidebarmenu">
  <div class="menu_section">
    <h3>&nbsp;</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a name="/page/home/dashboard" ng-click="changelink('/page/home/dashboard','1')">Dashboard</a>
          </li>
        </ul>
      </li>
      <li><a><i class="fa fa-database"></i> Master <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a name="/page/master/admin" ng-click="changelink('/page/master/admin','2')">Administrator</a></li>
          <li><a name="/page/master/profile_website" ng-click="changelink('/page/master/profile_website','3')">Profile Website</a></li>
          <li><a name="/page/master/menu" ng-click="changelink('/page/master/menu','4')">Menu</a></li>
          <li><a name="/page/master/kategori" ng-click="changelink('/page/master/kategori','5')">Kategori</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-desktop"></i> Halaman Depan <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a name="/page/halaman_depan/blockquote" ng-click="changelink('/page/halaman_depan/blockquote','12')">Blockquote</a></li>
          <li><a name="/page/halaman_depan/berita" ng-click="changelink('/page/halaman_depan/berita','13')">Berita</a></li>
          <li><a name="/page/halaman_depan/slider" ng-click="changelink('/page/halaman_depan/slider','14')">Slider</a></li>
          <li><a name="/page/halaman_depan/album" ng-click="changelink('/page/halaman_depan/album','15')">Album</a></li>
          <li><a name="/page/halaman_depan/embed_video" ng-click="changelink('/page/halaman_depan/embed_video','16')">Embed Video</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
