<!-- Framework -->
<script src="<?php echo base_url('gudang/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script src="<?php echo base_url('gudang/js/materialize.min.js');?>"></script>
<!-- Datatable -->
<script src="<?php echo base_url('gudang/js/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/datatables/dataTables.material.min.js');?>"></script>
<!-- UI -->
<script src="<?php echo base_url('gudang/js/jquery.mCustomScrollbar.concat.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/select2.full.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/valida.2.1.6.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/input_mask/jquery.inputmask.js');?>"></script>
<script src="<?php echo base_url('gudang/js/jquery.maskMoney.js');?>"></script>

<script src="<?php echo base_url('gudang/js/base64.js');?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>
<script src="<?php echo base_url('gudang/js/html2canvas.js');?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta1/html2canvas.svg.js"></script>
<script src="<?php echo base_url('gudang/js/canvas2image.js');?>"></script>
<script src="http://canvg.github.io/canvg/rgbcolor.js"></script>
<script src="http://canvg.github.io/canvg/StackBlur.js"></script>
<script src="http://canvg.github.io/canvg/canvg.js"></script>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<!-- AngularJs -->
<script src="<?php echo base_url('gudang/js/angularjs/angular.min.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-locale_id-id.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-resource.min.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-sanitize.min.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-preload-image.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-route.min.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/ngNotificationsBar.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-toasty.min.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/sortable.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/loading-bar.min.js');?>"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>




<script src="<?php echo base_url('gudang/js/select.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/sc-select.min.js');?>"></script>
<!-- Angular Upload -->
<script src="<?php echo base_url('gudang/js/ngupload/ng-file-upload-shim.min.js'); ?>">"></script> 
<script src="<?php echo base_url('gudang/js/ngupload/ng-file-upload.min.js'); ?>">"></script>
<!-- UI, other -->
<script src="<?php echo base_url('gudang/js/tinymce.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/tinymce/tinymce.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/ui-bootstrap-tpls-1.3.3.min.js'); ?>"></script>
<!-- Custom -->
<script src="<?php echo base_url('gudang/js/apps/router-apps.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/apps/main-apps.js'); ?>"></script>