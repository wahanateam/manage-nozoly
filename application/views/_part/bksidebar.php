          <nav class="bg-wahana darker z-depth-0">
            <div class="nav-wrapper">
              <ul>
                <li><img src="<?php echo base_url('gudang/upload/admin/'. $this->session->userdata('gambar')) ?>" class="circle sidebar-brand hide-on-med-and-down"></li>
                <li><a href="#" class="tooltipped truncate" style="width: 150px;" data-position="bottom" data-delay="50" data-tooltip="<?php echo $this->session->userdata('nama') ?>"><?php echo $this->session->userdata('nama'); ?></a></li>
              </ul>
              <ul class="mini-title" ng-controller="CmsController">
                <li><a class="tooltipped" ng-click="refresh()" data-position="bottom" data-delay="50" data-tooltip="Segarkan Halaman"><i class="icon refresh"></i></a>
                <!-- <li><a href="#" class="dropdown-button" dropdownangular data-activates='setting'><i class="icon more rotate-270"></i></a> -->
                </li>
                <li><a class="tooltipped" href="<?php echo base_url('page/master/admin/input/'.$this->session->userdata('id_admin'))?>" data-position="bottom" data-delay="50" data-tooltip="Pengaturan Akun"><span class="icon config"></span></a></li>
                <li><a class="tooltipped" href="" onclick="javascript:window.location.href='<?php echo site_url('auth/logout');?>'; return false;" data-position="bottom" data-delay="50" data-tooltip="Log out"><i class="icon power"></i> </a></li>
              </ul>
            </div>

            <div class="logo-search">
              <div class="logo">
                <img src="<?php echo base_url('gudang/upload/wahana-dulu.png') ?>">
              </div>
              <div class="searchinput">
                <input id="text" type="text" ng-keyup="caridatatables(caridata)" ng-model="caridata" placeholder="Cari..">
              </div>
            </div>
          </nav>
          <!-- Menu -->
          <!-- Pake scrollbar custom -->
          <!-- <div class="sidebar mCustomScrollbar" data-mcs-theme="minimal-dark" data-mcs-autoHideScrollbar="true" > -->
            <div class="search-form">
            <input id="text" type="text" placeholder="Pencarian.." ng-keyup="caridatatables(caridata)" ng-model="caridata">
            </div>
          <div class="sidebar">
            <ul class="collapsible collapsible-accordion no-margin">
              <li> <a href="/page/home/notif" > <span class="icon info"></span><span class="new badge"><?php echo $notifikasi; ?></span> Notifikasi</a> </li>
              
              <?php 
              foreach ($get_menu as $r): ?>
              <li><a class="collapsible-header"><span class="<?php echo $r['icon']; ?>"></span> <?php echo $r['nama_menu']; ?><i class="icon angle-down right"></i></a>
                <div class="collapsible-body">
                  <ul>
                   <?php  
                   foreach ($r['sub'] as $key) {?>
                      <li><a ng-click="gantijudul('<?php echo $key['judul_menu']; ?>')" ng-class="getClass('<?php echo base_url($key['link']); ?>')" href="<?php echo base_url($key['link']); ?>" > <?php echo $key['nama_menu']; ?> </a></li>
                   <?php } ?>
                  </ul>
                </div>
              </li>
              <?php endforeach ?>
            </ul>
          </div>