<!-- Framework -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<!-- Datatable -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.material.min.js"></script>
<!-- UI -->
<script src="<?php echo base_url('gudang/js/jquery.mCustomScrollbar.concat.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/select2.full.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/valida.2.1.6.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/input_mask/jquery.inputmask.js');?>"></script>
<script src="<?php echo base_url('gudang/js/jquery.maskMoney.js');?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<!-- AngularJs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.5/angular.min.js"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-locale_id-id.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.5/angular-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.5/angular-sanitize.min.js"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-preload-image.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-route.min.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/ngNotificationsBar.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/angular-toasty.min.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/select.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/sc-select.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/angularjs/sortable.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.9.0/loading-bar.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<!-- Angular Upload -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/12.0.4/ng-file-upload-shim.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/12.0.4/ng-file-upload.min.js"></script>
<!-- UI, other -->
<script src="<?php echo base_url('gudang/js/tinymce.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/tinymce/tinymce.min.js');?>"></script>
<script src="<?php echo base_url('gudang/js/ui-bootstrap-tpls-1.3.3.min.js'); ?>"></script>
<!-- Custom -->
<script src="<?php echo base_url('gudang/js/apps/router-apps.js'); ?>"></script>
<script src="<?php echo base_url('gudang/js/apps/main-apps.js'); ?>"></script>
