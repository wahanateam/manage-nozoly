<ul class="side-nav collapsible collapsible-accordion no-margin" id="mobile-demo">
  <div class="sidebar-header">
    <div class="img-container">
      <img src="<?php echo base_url('gudang/upload/admin/'. $this->session->userdata('gambar')) ?>" class="circle sidebar-brand hide-on-med-and-down">
    </div>
    <a href="#" class="tooltipped truncate" data-position="bottom" data-delay="50" data-tooltip="<?php echo $this->session->userdata('nama') ?>"><?php echo $this->session->userdata('nama'); ?></a>
  </div>
  <a class="bg-cream" href="" onclick="javascript:window.location.href='<?php echo site_url('auth/logout');?>'; return false;"><i class="icon back"></i> Logout</a>
  <?php 
  foreach ($get_menu as $r): ?>              
  <li><a class="collapsible-header"><span class="<?php echo $r['icon']; ?>"></span> <?php echo $r['nama_menu']; ?><i class="icon angle-down right"></i></a>
    <div class="collapsible-body">
      <ul>
       <?php  
       foreach ($r['sub'] as $key) {?>
          <li><a ng-click="gantijudul('<?php echo $key['judul_menu']; ?>')" ng-class="getClass('<?php echo base_url($key['link']); ?>')" href="<?php echo base_url($key['link']); ?>" > <?php echo $key['nama_menu']; ?> </a></li>
       <?php } ?>
      </ul>
    </div>
  </li>
  <?php endforeach ?>
</ul>