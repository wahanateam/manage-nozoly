<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons|PT+Sans:400,400i,700|Open+Sans|Bitter">
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.min.css">

<?php
$ip = $this->input->ip_address(); 
if($ip != "127.0.0.1"){ ?>

<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.9.0/loading-bar.min.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.material.min.css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/select2.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/sc-select.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/select.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css-dist/custom-build.min.css');?>">

<?php }else{ ?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/dataTables.material.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/loading-bar.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/angular-toasty.min.css');?>">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/line-chart/2.0.28/LineChart.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/loading-bar.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/icon.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/select2.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/sc-select.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/select.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/core.css');?>">

<?php } ?>