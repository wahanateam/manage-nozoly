<!DOCTYPE html>
<html ng-app="AppRoute">
<head>
  <title>Halaman Admin</title>    
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type=image/x-icon href="<?php echo img_url('favicon.png'); ?>">
  <?php 
    $this->load->view('_part/css-load');
  ?>
  
<script type="text/javascript">
  var ADMIN_URL = "<?php echo base_url(); ?>";
</script>

</head>
<body>

      <?php $this->load->view($konten); ?>


  <div id="modalKonfirmasiOrder" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <h4 class="modal-title">Konfirmasi Order </h4>
    <div class="modal-content" align="center">
      <span class="icon check" style="font-size:6em;margin-top:10px;"></span>
      <p>Apakah Anda yakin akan menkonfirmasi order ini ?</p>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="konfirmasiOrder()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>

  <div id="cari_alumniv2" class="modal modal-fixed-footer" style="width:30%;height: 290px">
    <h5 class="modal-title">Pencarian Lanjutan</h5>
    <div class="modal-content">
      <form name="form">
        <label>Jadwal Keberangkatan :</label>
        <div class="input-field style-2">
          <div class="select-box">
          <select class="browser-default" ng-model="cari.jadwal">
            <option value="">Pilih Jadwal</option>
            <option value="{{r.tanggal}}" ng-repeat="r in loop_jadwal">{{r.tanggal}}</option>
          </select>        
          </div>
        </div>

        <label>Nama Paket :</label>
        <div class="input-field style-2 no-margin">
          <div class="select-box">
          <select class="browser-default" ng-model="cari.paket">
            <option value="">Pilih Paket</option>
            <option value="{{r.nama_paket}}" ng-repeat="r in loop_paket">{{r.nama_paket}}</option>
          </select>        
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer" align="center">
      <a ng-click="cari_alumniv2(cari)" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Ok</a>
    </div>
  </div>

  <div id="cari_artikel" class="modal modal-fixed-footer" style="width:30%;height: 210px">
    <h5 class="modal-title">Pencarian Lanjutan</h5>
    <div class="modal-content">
      <label>Kategori Artikel :</label>
      <div class="no-margin input-field style-2">
        <div class="select-box">
        <select class="browser-default" ng-model="kategori">
          <option value="">Pilih Kategori</option>
          <option value="{{r.id_kategori}}" ng-repeat="r in loop_kategori">{{r.nama_kategori}} - {{r.type}}</option>
        </select>        
        </div>
      </div>
    </div>
    <div class="modal-footer" align="center">
      <a ng-click="cari_kategori(kategori)" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Ok</a>
    </div>
  </div>

  <div id="cari_jadwalpaket" class="modal modal-fixed-footer" style="width:30%;height: 210px">
    <h5 class="modal-title">Pencarian Lanjutan</h5>
    <div class="modal-content">
      <label>Nama Type :</label>
      <div class="no-margin input-field style-2">
        <div class="select-box">
        <select class="browser-default" ng-model="nama_type">
          <option value="">Pilih Type</option>
          <option value="{{r.nama_type}}" ng-repeat="r in loop_type">{{r.nama_type}}</option>
        </select>        
        </div>
      </div>
    </div>
    <div class="modal-footer" align="center">
      <a ng-click="cari_jadwalpaket(nama_type)" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none" ><span class="icon check"></span> Ok</a>
    </div>
  </div>


  <div id="modalGambarDelete" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <h4 class="modal-title">Konfirmasi Hapus </h4>
    <div class="modal-content" align="center">
      <span class="icon trash" style="font-size:6em;margin-top:10px;"></span>
      <p>Apakah Anda yakin akan hapus gambar ini ?</p>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="deletegambar()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>


  <div id="modalDelete" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <h4 class="modal-title">Konfirmasi Hapus </h4>
    <div class="modal-content" align="center">
      <span class="icon trash" style="font-size:6em;margin-top:10px;"></span>
      <p>Apakah Anda yakin akan hapus data ini ?</p>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="deletedatatables()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>


  <div id="modalOrderDelete" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <h4 class="modal-title">Konfirmasi Hapus </h4>
    <div class="modal-content" align="center">
      <span class="icon trash" style="font-size:6em;margin-top:10px;"></span>
      <p>Apakah Anda yakin akan hapus Order 2 hari sebelumnya ?</p>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="deleteorderotomatis_modal()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>


  <div id="modalRekapfee" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <h4 class="modal-title">Konfirmasi Hapus </h4>
    <div class="modal-content" align="center">
      <span class="icon trash" style="font-size:6em;margin-top:10px;"></span>
      <p>Apakah Anda yakin akan hapus data ini ?</p>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="rekapfeeDelete()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>


  <div id="ustadModal" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <h4 class="modal-title">Konfirmasi Hapus </h4>
    <div class="modal-content" align="center">
      <span class="icon trash" style="font-size:6em;margin-top:10px;"></span>
      <p>Apakah Anda yakin akan hapus data ini ?</p>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="ustaddelete()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>


  <div id="modalBatal" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <h4 class="modal-title">Konfirmasi Batal </h4>
    <div class="modal-content" align="center">
      <span class="icon trash" style="font-size:6em;margin-top:10px;"></span>
      <p>Apakah Anda yakin akan membatalkan keberangkatan jamaah ini ?</p>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="ubahstatusbatal()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>
  

  <div id="modalSub" class="modal modal-fixed-footer" ng-controller="CmsController">
    <div class="modal-content" align="center">
    <table class="transparent">
      <thead>
        <th width="0">#</th>
        <th>Nama Sub</th>
        <th>Urutan</th>
        <th>Action</th>
      </thead>
      <tbody>
        <tr ng-repeat="r in sub">
          <td>{{$index +1}}</td>
          <td>{{r.nama_menu}}</td>
          <td>{{r.urutan}}</td>
          <td>
          <a href="<?php echo base_url('page/master/menu_admin/input/{{r.id_menu_admin}}')?>" ng-click="closemodal()" class="btn-flat white-text light-blue"><span class="icon pen">
          </span>Edit</a>
          <a ng-click="DomDelete(r.id_menu_admin)"  modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>
          </td>
        </tr>
      </tbody>
    </table>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class="modal-action modal-close waves-effect btn-flat" style="float:none">Tutup</a>
    </div>
  </div>

  <!-- Modal Structure -->

  <div id="modalSubJadwal" class="modal modal-fixed-footer" ng-controller="CmsController">
    <div class="modal-content" align="center">
    <h4>{{nama_paket}}</h4>
           <div class="row no-print no-margin">
              <div class="input-field style-2 col l4 s12">
                  <input type="text" name="tanggal" ng-model="tanggal_cari" placeholder="Cari menggunakan Tanggal">
              </div>
            </div>
    <table class="transparent" >
      <thead>
        <th width="0">#</th>
        <th>Tanggal Berangkat</th>
        <th>Tanggal Pulang</th>
        <th>Status</th>
        <th>Action</th>
      </thead>
      <tbody>
        <tr ng-repeat="r in jadwal_by_paket | filter:tanggal_cari"">
          <td>{{$index +1}}</td>
          <td>{{r.tanggal}}</td>
          <td>{{r.tanggal_pulang}}</td>
          <td><a ng-click="DomStatus(r.id_jadwal,r.status)" id-status="{{r.id_jadwal}}" status="{{r.status}}'"  modal class="btn-flat white-text {{r.color}}">{{r.status}}</a></td>
          <td>
          <a href="<?php echo base_url('page/umrah_sistem/jadwal_paket/input/{{r.id_jadwal}}')?>" ng-click="closemodal()" class="btn btn-info"><span class="icon pen">
          </span>Edit</a>
          <a ng-click="DomDelete(r.id_jadwal)"  modal class="btn-flat white-text red"><span class="icon trash"></span> Delete</a>
          </td>
        </tr>
      </tbody>
    </table>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class="modal-action modal-close waves-effect btn-flat" style="float:none">Tutup</a>
    </div>

    <script>
    $(document).ready(function() {
      $(":input").inputmask();
    });
  </script>

  </div>

  <div id="modalpaketselect" class="modal bottom-sheet" ng-controller="CmsController">
    <div class="modal-content" align="center">
      <h4>{{judul_modal_transportasi}}</h4>
      <ul >
        <li ng-repeat="r in datapaketselect"><a ng-click="pilihfitur(r.id,r.type)">
          <label><b>{{r.nama}}</b></label>
         <img class="responsive-img" width="100" src="<?php echo base_url('gudang/upload/{{r.folder}}/thumb/{{r.foto}}'); ?>"> </a></li>
      </ul>
    </div>
  </div>

  
  <form ng-submit="tambahjamaahdaftar(jamaah)" ng-controller="CmsController" >
  <div id="modaltambahJamaah" class="modal modal-fixed-footer" style="width:50%;height:80%">
    
    <div class="modal-content" align="center">
      <h4>Tambah Jamaah  </h4>
      <div class="input-field col s8">
        <div class="col s3" align="left">Nama Lengkap :</div>
        <div class="input-field style-2 col s8">
          <input type="text" ng-model="value.nama_lengkap">
        </div>

        <div class="col s3" align="left">Nama Ayah :</div>
        <div class="input-field style-2 col s8">
          <input type="text" ng-model="value.nama_ayah">
        </div>

        <div class="col s3" align="left">Jenis Kelamin :</div>
        <div class="input-field style-2 col s8">
          <input type="text" ng-model="value.jenis_kelamin">
        </div>

        <div class="col s3" align="left">Tanggal Lahir :</div>
        <div class="input-field style-2 col s8">
          <input type="text" ng-model="value.tanggal_lahir" ngmask data-inputmask="'mask': '99/99/9999'">
        </div>

        <div class="col s3" align="left">Email :</div>
        <div class="input-field style-2 col s8">
          <input type="text" ng-model="value.email" >
        </div>

        <div class="col s3" align="left">Jenis Kamar :</div>
        <div class="input-field style-2 col s8">
          <input type="text" ng-model="value.jenis_kamar">
        </div>
      </div>
      <!-- <textarea ng-model="pertanyaan.komen"></textarea> -->
      
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <button type="submit" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Simpan</button>
    </div>

  </div>
  </form>



  <form ng-submit="komen(pertanyaan)" ng-controller="CmsController" >
  <div id="komenModal" class="modal modal-fixed-footer" style="width:50%;height:80%">
    <div class="modal-content" align="center">
      <h4>Edit  </h4>
      <div class="input-field col s8">
              <textarea ng-model="pertanyaan.komen" ui-tinymce="tinymceOptions" class="form-control col-md-7 col-xs-12"></textarea>
      </div>
      <!-- <textarea ng-model="pertanyaan.komen"></textarea> -->
      
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <button type="submit" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Simpan</button>
    </div>
  </div>
  </form>

  
  <div id="modalAlumni" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <div class="modal-content" align="center">
      <h4>Konfirmasi Tambah </h4>
      <p>Apakah Anda yakin akan menambah data alumni ?</p>
      <span class="icon file" style="font-size:6em;margin-top:10px;"></span>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="tambahalumni()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>


  <!-- Modal Structure -->
  <div id="modalUbahAlumni" class="modal modal-fixed-footer" ng-controller="CmsController" style="width:30%;height:300px">
    <div class="modal-content" align="center">
      <h4>Konfirmasi Ubah </h4>
      <p>Apakah Anda yakin akan merubah data jamaah ke alumni ?</p>
      <span class="icon file" style="font-size:6em;margin-top:10px;"></span>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="rubahalumni()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>




    <!-- Modal Structure -->

  <form ng-submit="import(upload)" ng-controller="CmsController" >
  <div id="modalImport" class="modal modal-fixed-footer" style="width:50%;height:80%">
    <div class="modal-content" align="center">
      <h4>Import Excel Jamaah</h4>
    
    <div class="input-field style-2 col l8 s12">
      <div class="select-box">
      <select class="browser-default" ng-init="upload.jadwal=''" ng-model="upload.jadwal">
        <option value="">Pilih Jadwal</option>
        <option ng-repeat="r in jadwalimport" value="{{r.tanggalval}}"> {{r.tanggal}}</option>
      </select>
      </div>
    </div>

    <div class="input-field col s8">
          <input type="file"  ngf-select ng-model="upload.fileupload">
      </div>
      <!-- <textarea ng-model="pertanyaan.komen"></textarea> -->
      
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <button type="submit" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Simpan</button>
    </div>
  </div>
  </form>

<?php 
  $this->load->view('_part/javascript-load');
?>

<toasty></toasty>

</body>
</html>