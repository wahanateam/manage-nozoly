  <div class="this-container">
    <div class="this-content">
      <div class="row">
        <div class="col l2 m4 s12 hide-on-small-only">
          <?php $this->load->view('_part/sidebar.php'); ?>
        </div>
        <div class="col l10 m8 s12">
            <div ng-view></div>
        </div>
      </div>
    </div>
  </div>       
  
  <a href="#" data-activates="mobile-demo" ngsidenav class=" hide-on-med-and-up button-collapse"><i class="material-icons">menu</i></a>
  <?php $this->load->view('_part/sidebar_m.php'); ?>