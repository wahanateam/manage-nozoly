<nav class="blue z-depth-0">
            <div class="nav-wrapper">
              <a href="#" class="brand-logo truncate" title="Menu">Menu</a>
              <ul class="right">
                <li><a href="<?php echo base_url('page/master/menu/input');?>"><span class="icon copy-file"></span> Tambah</a></li>
              </ul>
            </div>
          </nav>
          <div class="steps-container">
            <ul class="steps">
              <li><a href="#">Home</a></li>
              <li><a href="#">Master</a></li>
              <li class="active"><a href="#">Menu</a></li>
            </ul>
            <div class="pencarian">
              <div class="search-form">
                <input id="text" type="text" placeholder="Pencarian.." ng-keyup="caridatatables(caridata)" ng-model="caridata">
                <span class="icon search"></span>
              </div>
            </div>
          </div>

<div class="content">
  <table id="datatables" class="mdl-data-table">
      <thead>
          <tr class="blue lighten-1 white-text">
                                <th>No.</th>
                                <th>Sub</th>
                                <th>Nama Menu</th>
                                <th>Link</th>
                                <th>Action</th>
          </tr>
      </thead>
  </table>
</div>
