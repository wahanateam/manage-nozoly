  <form ng-submit="save(value)" class="row" ng-init="load_tambah()">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Menu">Input Menu</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/menu');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/menu');?>">Menu</a></li>
      <li class="active"><a href="#">Input Menu</a></li>
    </ul>
  </div>
<div class="content">
      <div class="col l3 s12 ta-l-s" align="right">Submenu :</div>
      <div class="input-field style-2 col l8 s12">
        <div class="select-box">
        <select class="browser-default" ng-init="value.id_sub=''" ng-model="value.id_sub">
          <option value="">Pilih Submenu</option>
          <option ng-repeat="r in ii.sub" value="{{r.id_menu}}"> {{r.nama_menu}}</option>
        </select>
        </div>
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Nama Menu :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.nama_menu">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Judul Menu :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.judul_menu">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Ikon :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.ikon">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Link :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.link">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Seo Link :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.seo_link">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Meta Key  :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.meta_key">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Meta Des  :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.meta_des">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Tipe :</div>
      <div class="input-field style-2 col l8 s12">
        <div class="select-box">
        <select class="browser-default" ng-init="value.type=''" ng-model="value.type">
          <option value="">-Pilih Type-</option>
          <option value="1">Statis</option>
          <option value="0">Dinamis</option>
        </select>
        </div>
      </div>

      <div class="col l3 s12 ta-l-s" align="right" ng-show="value.type == 1">Konten :</div>
      <div class="input-field col l8 s12" ng-show="value.type == 1">
        <textarea name="konten" class="form-control col-md-7 col-xs-12" ui-tinymce="tinymceOptions" ng-model="value.konten"></textarea>
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Urutan :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.urutan">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Status :</div>
      <div class="input-field style-2 col l8 s12">
        <div class="select-box">
        <select class="browser-default" ng-init="value.status=''" ng-model="value.status">
          <option value="">Pilih Status</option>
          <option value="1">Aktif</option>
          <option value="0">Tidak Aktif</option>
        </select>
        </div>
      </div>
      </div>
  </form>
