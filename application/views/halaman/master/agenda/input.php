  <form ng-submit="save(value)" ng-init="load_tambah()" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Agenda">Input Agenda</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/agenda');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/agenda');?>">Agenda</a></li>
      <li class="active"><a href="#">Input Agenda</a></li>
    </ul>
  </div>

<div class="content">
      <div class="col l3 s12 ta-l-s" align="right">Type :</div>
      <div class="input-field style-2 col l8 s12">
        <div class="select-box">
        <select class="browser-default" ng-init="value.type=''" ng-model="value.type">
          <option value="">Pilih Type</option>
          <option value="ultah">Ulang Tahun</option>
          <option value="ultahnikah">Ulang Tahun Pernikahan</option>
          <option value="event">Event</option>
        </select>
        </div>
      </div>


      <div ng-show="value.type == 'event'" >
      <div class="col l3 s12 ta-l-s" align="right">Judul :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.judul">
      </div>
      </div>

      <div ng-hide="value.type == 'event' || value.type == ''" >
      <div class="col l3 s12 ta-l-s" align="right">Nama Jamaah :</div>
      <div class="input-field style-2 col l8 s12">
        {{value.nama1}}
        <select select2 class="js-example-basic-single" ng-init="value.id_jamaah1=''" ng-model="value.id_jamaah1">
          <option value="">Pilih Jamaah</option>
          <option ng-value="r.id_jamaah" ng-repeat="r in ii.jamaah">{{r.nama_lengkap}}</option>
        </select>
      </div>
      </div>

      <div ng-show="value.type == 'ultahnikah'">
      <div class="col l3 s12 ta-l-s" align="right">Nama Jamaah 2 :</div>
      <div class="input-field style-2 col l8 s12">
        {{value.nama2}}
        <select select2 class="js-example-basic-single" ng-init="value.id_jamaah2=''" ng-model="value.id_jamaah2">
          <option value="">Pilih Jamaah 2</option>
          <option ng-value="r.id_jamaah" ng-repeat="r in ii.jamaah">{{r.nama_lengkap}}</option>
        </select>
      </div>
      </div>
      
      <div class="col l3 s12 ta-l-s" align="right">Tanggal :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.tanggal" data-inputmask="'mask': '99/99/9999'">
      </div>
      
      <div class="col l3 s12 ta-l-s" align="right">Tanggal Reminder :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.tanggal_reminder" data-inputmask="'mask': '99/99/9999'">
      </div>
      
      <div class="col l3 s12 ta-l-s" align="right">Keterangan :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.keterangan">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Privasi :</div>
      <div class="input-field style-2 col l8 s12">
        <div class="select-box">
        <select class="browser-default" ng-init="value.privasi=''" ng-model="value.privasi">
          <option value="">Pilih Privasi</option>
          <option value="1">Public</option>
          <option value="0">Privasi</option>
        </select>
        </div>
      </div>


</div>

  </form>

  <script>
    $(document).ready(function() {
      $(":input").inputmask();
    });
  </script>