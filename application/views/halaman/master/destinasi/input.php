  <form ng-submit="save(value)" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Destinasi">Input Destinasi</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/destinasi');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/destinasi');?>">Destinasi</a></li>
      <li class="active"><a href="#">Input Destinasi</a></li>
    </ul>
  </div>

<div class="content">

      <div class="col l3 s12 ta-l-s" align="right">Nama Destinasi :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.destinasi">
      </div>

</div>

  </form>