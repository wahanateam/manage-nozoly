<form ng-submit="save(value)" class="row">

<nav class="blue z-depth-0">
            <div class="nav-wrapper">
              <a href="#" class="brand-logo truncate" title="Input Data Travel">Input Data Travel</a>
              <ul class="right">
                <li><a href="<?php echo base_url('page/master/data_travel');?>"><span class="icon angle-left"></span> Kembali</a></li>
                <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
              </ul>
            </div>
          </nav>
          <div class="steps-container">
            <ul class="steps">
              <li><a href="#">Home</a></li>
              <li><a href="<?php echo base_url('page/master/data_travel');?>">Data Travel</a></li>
              <li class="active"><a href="#">Input Data Travel</a></li>
            </ul>
          </div>

<div class="content">
  <div class="row">
    <div class="col l3 s12 ta-l-s" align="right">Nama Travel :</div>
    <div class="input-field style-2 col l8 s12">
    <input type="text" ng-model="value.nama_travel">
    </div>
  </div>

  <div class="row">
    <div class="col l3 s12 ta-l-s" align="right">Alamat :</div>
    <div class="input-field style-1 col l8 s12">
      <textarea ng-model="value.alamat"></textarea>
    </div>
  </div>

  <div class="row">
    <div class="col l3 s12 ta-l-s" align="right">Status :</div>
    <div class="input-field style-2 col l8 s12">
      <div class="select-box">
      <select class="browser-default" ng-init="value.status=''" ng-model="value.status">
        <option value="">Pilih Status</option>
        <option value="Aktif">Aktif</option>
        <option value="Tidak Aktif">Tidak Aktif</option>
      </select>
      </div>
    </div>
  </div>

</div>
</form>