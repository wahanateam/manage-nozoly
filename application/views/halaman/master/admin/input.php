<form ng-submit="save(value)" ng-init="load_tambah()" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Jabatan">Input Administrator</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/admin');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
        <!-- <li><a ng-click="save(value)"><span class="icon diskette"></span> Simpan</a></li> -->
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/admin');?>">Administrator</a></li>
      <li class="active"><a href="#">Input Administrator</a></li>
    </ul>
  </div>

  <div class="content">
    <div class="col l3 s12 ta-l-s" align="right">Nama Lengkap :</div>
    <div class="input-field style-2 col l8 s12">
    <input type="text" ng-model="value.nama">
    </div>

    <div class="col l3 s12 ta-l-s" align="right">Username :</div>
    <div class="input-field style-2 col l8 s12">
    <input type="text" ng-model="value.username">
    </div>

    <div class="col l3 s12 ta-l-s" align="right">Password :</div>
    <div class="input-field style-2 col l8 s12">
    <input type="password" ng-model="value.password" password-strength="value.password">
    </div>

    <div class="col l3 s12 ta-l-s" align="right">Ulangi Password :
      <small ng-show="value.password">
      <div ng-show="value.password == value.re_password" class="green-text">Password Cocok</div> 
      <div ng-show="value.password != value.re_password" class="red-text">Password Tidak Cocok</div>
      </small>
    </div>
    <div class="input-field style-2 col l8 s12">
      <input type="password" ng-model="value.re_password">
    </div>

    <div class="col l3 s12 ta-l-s" align="right">Gambar :</div>
    <div class="input-field col l8 s12">
      <div ng-show="value.gambar" style="margin-bottom:10px;">
        <img ngf-thumbnail="value.gambar" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
      </div>
      <input type="hidden" ng-model="value.gambar">
      <div class="file-field input-field no-padding">
        <div class="btn z-depth-0 blue lighten-1">
          <span>Pas Photo</span>
          <input type="file" ngf-select ng-model="value.gambar" ngf-pattern="'image/*'" ngf-accept="'image/*'">
        </div>
        <div class="file-path-wrapper">
          <input class="file-path validate" type="text">
        </div>
      </div>
    </div>

    <div ng-show="value.id_admin">
        <?php 
          if ($this->session->userdata('main') == "1") {
        ?>          
    </div>

    <div class="col l3 s12 ta-l-s" align="right">Level :</div>
    <div class="input-field style-2 col l8 s12">
      <div class="select-box">
      <select class="browser-default" ng-init="value.id_level=''" ng-model="value.id_level">
        <option value="">Pilih Level</option>
        <option ng-repeat="r in ii.level" value="{{r.id_level}}"> {{r.nama_level}}</option>
      </select>
      </div>
    </div>

    <div ng-show="value.id_admin">
        <?php } ?>          
    </div>


    <div class="col l3 s12 ta-l-s" align="right">Status :</div>
    <div class="input-field style-2 col l8 s12">
      <div class="select-box">
      <select class="browser-default" ng-init="value.status=''" ng-model="value.status">
        <option value="">Pilih Status</option>
        <option>Aktif</option>
        <option>Tidak Aktif</option>
      </select>
      </div>
    </div>
  </div>
</form>
