  <form ng-submit="input_akses(value)" ng-init="load_tambah('active')" class="row">



  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Jabatan">Input Akses Admin</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/level');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/level');?>">Level Admin</a></li>
      <li class="active"><a href="#">Input Akses Admin - <b>{{value.nama_level}}</b></a></li>
    </ul>
  </div>
<div class="content">

<!-- 
 <div class="floatleft">
      <ul class="apps-container" ui-sortable="sortableOptions" ng-model="ii.menu" class="list">
        <li  ng-repeat="item in ii.menu" class="item">
        {{item.nama_menu}}
        <ul class="apps-container" ui-sortable="sortableOptions" ng-model="item.sub"  class="list">
          <li class="item" ng-repeat="item in item.sub"> --- {{item.nama_menu}}</li>
        </ul>
        </li>
      </ul>
  </div> -->

      <div class="col s12">
        <table>
        <thead>
          <tr>
              <th>Menu</th>
              <th>Akses</th>
          </tr>
        </thead>
        <tbody ng-repeat="r in ii.menu">

          <tr >
            <td><span class="icon angle-down"></span> {{r.nama_menu}} </td>
            <td>
              <div class="input-field col s12">
                    <input type="radio"  name="akses[{{r.id_menu_admin}}]" id="1[{{r.id_menu_admin}}]" ng-model="value.akses[r.id_menu_admin]" value="1" >
                    <label for="1[{{r.id_menu_admin}}]">Aktif</label>
                    <input type="radio"  name="akses[{{r.id_menu_admin}}]" id="0[{{r.id_menu_admin}}]" ng-model="value.akses[r.id_menu_admin]" value="0" > 
                    <label for="0[{{r.id_menu_admin}}]">Tidak Aktif</label>

              </div>


<!--                 <div class="switch" style="height: 25px;">
                  <label style="margin: 15px 0;left: 0;">
                    Tidak 
                    <input type="checkbox" ng-model="value.akses[r.id_menu_admin]" 
                                           ng-checked="value.akses[r.id_menu_admin] == 1"
                                           ng-true-value="1"
                                           ng-false-value="0"> {{value.akses[r.id_menu_admin]}}
                    <span class="lever"></span>
                    Ya
                  </label>
                </div> -->
            </td>
          </tr>
          <tr ng-repeat="key in r.sub">
            <td><span style="letter-spacing: -1px;margin: 0 5px 0 30px;">---</span> {{key.nama_menu}} </td>
            <td>
              <div class="input-field col s12">
                    <input type="radio"  name="akses[{{key.id_menu_admin}}]" id="1[{{key.id_menu_admin}}]" ng-model="value.akses[key.id_menu_admin]" value="1" >
                    <label for="1[{{key.id_menu_admin}}]">Aktif</label>
                    <input type="radio"  name="akses[{{key.id_menu_admin}}]" id="0[{{key.id_menu_admin}}]" ng-model="value.akses[key.id_menu_admin]" value="0" > 
                    <label for="0[{{key.id_menu_admin}}]">Tidak Aktif</label>

              </div>
            </td>
          </tr>
        </tbody>
      </table>
      </div>

</div>

  </form>