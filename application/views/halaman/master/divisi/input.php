  <form ng-submit="save(value)" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Divisi">Input Divisi</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/divisi');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/divisi');?>">Divisi</a></li>
      <li class="active"><a href="#">Input Divisi</a></li>
    </ul>
  </div>

<div class="content">

      <div class="col l3 s12 ta-l-s" align="right">Nama Divisi :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.nama_divisi">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Nominal Fee :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.nominal_fee">
      </div>



</div>

  </form>