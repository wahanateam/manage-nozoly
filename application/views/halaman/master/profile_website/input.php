  <form ng-submit="save(value)" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Profile Website">Input Profile Website</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/profile_website');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/master');?>">Profile Website</a></li>
      <li class="active"><a href="#">Input Profile Website</a></li>
    </ul>
  </div>

<div class="content">

      <div class="col s3" align="right">Nama Perusahaan :</div>
      <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.nama_perusahaan">
      </div>

      <div class="col s3" align="right">Judul Website :</div>
      <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.judul_web">
      </div>

      <div class="col s3" align="right">Deskripsi Website  :</div>
      <div class="input-field col s8">
      <textarea ng-model="value.des_web" ui-tinymce="tinymceOptions" name="des" class="form-control col-md-7 col-xs-12"></textarea>
      </div>
      
      <div class="col s3" align="right">Keyword Website :</div>
      <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.keyword_web">
      </div>

      <div class="col s3" align="right">Logo :</div>
      <div class="input-field col s8">
        <div ng-show="value.logo" style="margin-bottom:10px;">
          <img ngf-thumbnail="value.logo" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="value.logo">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Pas Photo</span>
            <input type="file" ngf-select ng-model="value.logo" ngf-pattern="'image/*'" ngf-accept="'image/*'">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">Logo 2 :</div>
      <div class="input-field col s8">
        <div ng-show="value.logodua" style="margin-bottom:10px;">
          <img ngf-thumbnail="value.logodua" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="value.logodua">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Pas Photo</span>
            <input type="file" ngf-select ng-model="value.logodua" ngf-pattern="'image/*'" ngf-accept="'image/*'">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

</div>

  </form>
