  <form ng-submit="save(value)" class="row" ng-init="load_tambah()">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Menu Admin">Input Menu Admin</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/menu_admin');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/menu_admin');?>">Menu Admin</a></li>
      <li class="active"><a href="#">Input Menu Admin</a></li>
    </ul>
  </div>
<div class="content">
      <div class="col l3 s12 ta-l-s" align="right">Submenu :</div>
      <div class="input-field style-2 col l8 s12">
        <div class="select-box">
        <select class="browser-default" ng-init="value.id_sub=''" ng-model="value.id_sub">
          <option value="">Pilih Submenu</option>
          <option ng-repeat="r in ii.sub" value="{{r.id_menu_admin}}"> {{r.nama_menu}}</option>
        </select>
        </div>
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Nama Menu Admin :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.nama_menu">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Judul Menu Admin :</div>
      <div class="input-field style-2 col l8 s12">
      <input type="text" ng-model="value.judul_menu">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Link :</div>
      <div class="input-field style-2 col l8 s12">
      <input type="text" ng-model="value.link">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Icon :</div>
      <div class="input-field style-2 col l8 s12">
      <input type="text" ng-model="value.icon">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Urutan :</div>
      <div class="input-field style-2 col l8 s12">
      <input type="text" ng-model="value.urutan">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Status :</div>
      <div class="input-field style-2 col l8 s12">
        <div class="select-box">
        <select class="browser-default" ng-init="value.status=''" ng-model="value.status">
          <option value="">Pilih Status</option>
          <option value="1">Aktif</option>
          <option value="0">Tidak Aktif</option>
        </select>
        </div>
      </div>
      
</div>
  </form>
