
  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Jabatan">Input Administrator</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/master/admin');?>"><span class="icon angle-left"></span> Kembali</a></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/master/admin');?>">Administrator</a></li>
      <li class="active"><a href="#">Input Administrator</a></li>
    </ul>
  </div>

<div class="content">
  <form ng-submit="save(value)" ng-init="load_tambah()" class="row">

<!--       <div class="col s3" align="right">Jabatan :</div>
      <div class="input-field style-2 col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.id_jabatan=''" ng-model="value.id_jabatan">
          <option value="">Pilih Jabatan</option>
          <option ng-repeat="r in ii.jabatan" value="{{r.id_jabatan}}"> {{r.nama_jabatan}}</option>
        </select>
        </div>
      </div> -->

      <div class="col s3" align="right">Nama Lengkap :</div>
      <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.nama">
      </div>

      <div class="col s3" align="right">Username :</div>
      <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.username">
      </div>

      <div class="col s3" align="right">Password :</div>
      <div class="input-field style-2 col s8">
      <input type="password" ng-model="value.password" password-strength="value.password">
      </div>

      <div class="col s3" align="right">Ulangi Password :
        <small ng-show="value.password">
        <div ng-show="value.password == value.re_password" class="green-text">Password Cocok</div> 
        <div ng-show="value.password != value.re_password" class="red-text">Password Tidak Cocok</div>
        </small>
      </div>
      <div class="input-field style-2 col s8">
      <input type="password" ng-model="value.re_password">
      </div>
      <div class="col s3" align="right">Gambar :</div>
      <div class="input-field col s8">
        <div ng-show="value.gambar" style="margin-bottom:10px;">
          <img ngf-thumbnail="value.gambar" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="value.gambar">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Pas Photo</span>
        <input type="file" ngf-select ng-model="value.gambar" ngf-pattern="'image/*'" ngf-accept="'image/*'">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">Level :</div>
      <div class="input-field style-2 col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.id_level=''" ng-model="value.id_level">
          <option value="">Pilih Level</option>
          <option ng-repeat="r in ii.level" value="{{r.id_level}}"> {{r.nama_level}}</option>
        </select>
        </div>
      </div>

      <div class="col s3" align="right">Status :</div>
      <div class="input-field style-2 col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.status=''" ng-model="value.status">
          <option value="">Pilih Status</option>
          <option>Aktif</option>
          <option>Tidak Aktif</option>
        </select>
        </div>
      </div>


      <div class="col s3" align="right"></div>
      <div class="input-field col s8" style="margin-top: 20px;">
        <button type="submit" class="btn-flat btn-large block white-text green darken-1 block waves-effect waves-light"><span class="icon download"></span> Simpan</button>
      </div>
  </form>
</div>
