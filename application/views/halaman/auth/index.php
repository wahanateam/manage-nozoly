<form action="<?php echo site_url('api/login');?>" method="post"  style="vertical-align: middle;">
  <div class="card-panel no-margin sidebar-login z-depth-0">
    <center>
      <!-- <h4 style="margin-bottom:0">Login</h4> -->
      <!-- <img src="<?php echo base_url('gudang/images/border.png') ?>" style="width:70%"> -->
      <img src="<?php echo base_url('gudang/images/kop surat-05.png') ?>">
      <?php echo $this->session->flashdata('message'); ?>
    </center>

    <div class="input-field">
      <i class="material-icons prefix">person</i>
      <input autofocus="" id="username" type="text" name="username">
      <label for="username">Username</label>
    </div>
    <div class="input-field">
      <i class="material-icons prefix">lock</i>
      <input id="password" type="password" name="password">
      <label for="password">Password</label>
    </div>

    <!-- <div class="input-field" style="margin-top: 30px;">
    </div> -->
  </div>
  <button type="submit" class="block btn-flat btn-large white-text blue darken-1 waves-effect waves-light login-btn" style="width:100%;">Masuk</button>
</form> 