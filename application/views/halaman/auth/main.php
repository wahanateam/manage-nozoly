<!DOCTYPE html>
<html ng-app="AppRoute">
<head>
	<title>Halaman Login</title>    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type=image/x-icon href="<?php echo img_url('favicon.png'); ?>">

	<?php 
		$this->load->view('_part/css-load');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('gudang/css/login.css');?>">
	
</head>
<script type="text/javascript">
	var ADMIN_URL = "<?php echo base_url(); ?>";
</script>
<body> 
<notifications-bar class="notifications" closeIcon="fa fa-times-circle"></notifications-bar>
<?php 
	$this->load->view($konten);
?>
<?php 
	$this->load->view('_part/javascript-load');
?>
<script type="text/javascript">
    // Validasi form
    $('form').valida();
    // Menambahkan validasi ke semua inputan
    $('form input, form select, form textarea').attr('required',true);
</script>
</body>
</html>