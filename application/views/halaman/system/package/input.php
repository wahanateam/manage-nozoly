  <form ng-submit="save(value)" ng-init="load_tambah('active')" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Jabatan">Input Paket Umrah</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/system/package');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
        <!-- <li><a ng-click="save(value)"><span class="icon diskette"></span> Simpan</a></li> -->
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/system/paket_umrah');?>">Paket Umrah</a></li>
      <li class="active"><a href="#">Input Paket Umrah</a></li>
    </ul>
  </div>

<div class="content">

  <div class="row">
    <div class="col s12">
      <ul class="tabs" tabsangular>
        <li class="tab col s3"><a href="#Umum">Umum</a></li>
        <li class="tab col s3"><a href="#Harga">Harga</a></li>
        <li class="tab col s3"><a href="#Fasilitas">Fasilitas</a></li>
        <li class="tab col s3"><a href="#Rute">Rute</a></li>
        <li class="tab col s3"><a href="#Keterangan">Keterangan</a></li>
      </ul>
    </div>

    <div id="Umum" class="col s12">
      <div class="card-panel z-depth-1">
      <div class="white black-text">
        <div class="row">
          <div class="col l3 s12 ta-l-s" align="right">Tahun</div>
          <div class="input-field style-2 col l5 s12">
            <input type="text" ng-model="value.tahun" name="tahun" class="form-control col-md-7 col-xs-12">
          </div>
        </div>

        <div class="row">
          <div class="col l3 s12 ta-l-s" align="right">Nama Paket</div>
          <div class="input-field style-2 col l5 s12">
            <input type="text" ng-model="value.nama_paket" name="nama_paket" class="form-control col-md-7 col-xs-12">
          </div>
        </div>

        <div class="row">
          <div class="col l3 s12 ta-l-s" align="right">Nama Type</div>
          <div class="input-field style-2 col l5 s12">
            <input type="text" ng-model="value.nama_type" name="nama_type" class="form-control col-md-7 col-xs-12">
          </div>
        </div>
        
        <div class="row">
          <div class="col l3 s12 ta-l-s" align="right">Kurs</div>
          <div class="input-field style-2 col l5 s12">
            <input type="text" name="kurs" class="form-control currency" ng-model="value.kurs" >
          </div>
        </div>

        <div class="row">
          <div class="col l3 s12 ta-l-s" align="right">Harga <small>(USD)</small>
          </div>
          <div class="input-field style-2 col l5 s12">
            <input type="text" name="harga" class="form-control currency" ng-model="value.harga" >
          </div>
        </div>
      </div>
    </div>
    </div>
    <div id="Harga" class="col s12">
      <div class="card-panel z-depth-1">
      <div class="white black-text">
          <div class="row">
            <div class="col l5 s12">
              <h5 class="col s12">Potongan Harga Kamar</h5>

              <div class="col s12">Triple <small>(USD)</small>
              </div>
              <div class="input-field style-2 col s12">
                <div class="input-group">
                  <input type="text" ng-model="value.triple" name="tripel" class="form-control col-md-7 col-xs-12 currency" >
                </div>
              </div>
              <div class="col s12">Quad <small>(USD)</small>
              </div>
              <div class="input-field style-2 col s12">
                <div class="input-group">
                <input type="text" ng-model="value.quad" name="quad" class="form-control col-md-7 col-xs-12 currency" >
                </div>
              </div>
              <h6>&nbsp;</h6>
            </div>
            <div class="col l5 s12">
              <h5 class="col s12">Biaya Tambahan</h5>  
              <div class="col s12">Biaya Pendaftaran <small>(RP)</small>
              </div>
              <div class="input-field style-2 col s12">
                <div class="input-group">
                  <input type="text" name="harga" class="form-control" ng-model="value.biaya_tambahan">
                </div>
              </div>
            </div>
          </div><!-- /.row -->
      </div>
    </div>
    </div>
    <div id="Fasilitas" class="col s12">
      <div class="card-panel z-depth-1">
        <div class="row">
          <div class="col l3 s12">
            <h5>Transportasi</h5>
            <div class="col 12 block">
              <a class="waves-effect waves-light btn modal-trigger block" ng-click="pilih_sesuatu('Pilih Bis','post_bus',value)">
                {{value.id_bus ? "Ganti Bis" : "Pilih Bis"}}
              </a>
              <div ng-show="value.id_bus">
                <div></div>
                <h6 class="uppercase">{{ii.bus.nama_bus}}</h6>
                <img class="responsive-img" width="100" src="<?php echo base_url('gudang/upload/bus/thumb/{{ii.bus.gambar_bus}}'); ?>">
              </div>

              <div class="spacer"></div>

              <a class="waves-effect waves-light btn modal-trigger block" ng-click="pilih_sesuatu('Pilih Maskapai','post_maskapai',value)">
                {{value.id_pesawat ? "Ganti Pesawat" : "Pilih Pesawat"}}
              </a>
              <div ng-show="value.id_pesawat">
                <div></div>
                <h6 class="uppercase">{{ii.pesawat.nama_pesawat}}</h6>
                <img class="responsive-img" width="100" src="<?php echo base_url('gudang/upload/pesawat/thumb/{{ii.pesawat.gambar_pesawat}}'); ?>">
              </div>
            </div>
          </div>
          <div class="col l3 s12">
            <h5>Hotel</h5>
            <div class="col 12 block">
              <a class="waves-effect waves-light btn modal-trigger block" ng-click="pilih_sesuatu('Pilih Hotel Madinah','post_hotel_madinah',value)">
              {{value.id_hotel_madinah ? "Ganti Hotel Madinah" : "Pilih Hotel Madinah"}}
              </a>
                <div ng-show="value.id_hotel_madinah">
                  <div></div>
                  <h6 class="uppercase">{{ii.hotel_madinah.nama_hotel}}</h6>
                <img class="responsive-img" width="100" src="<?php echo base_url('gudang/upload/hotel/thumb/{{ii.hotel_madinah.gambar_hotel}}'); ?>">
              </div>

              <div class="spacer"></div>
              <!--  -->
              <a class="waves-effect waves-light btn modal-trigger block" ng-click="pilih_sesuatu('Pilih Hotel Makkah','post_hotel_makkah',value)">
              {{value.id_hotel_makkah ? "Ganti Hotel Makkah" : "Pilih Hotel Makkah"}}
              </a>

                <div ng-show="value.id_hotel_makkah">
                  <div></div>
                  <h6 class="uppercase">{{ii.hotel_makkah.nama_hotel}}</h6>
                <img class="responsive-img" width="100" src="<?php echo base_url('gudang/upload/hotel/thumb/{{ii.hotel_makkah.gambar_hotel}}'); ?>">
              </div>

              <div class="spacer"></div>
              <!--  -->
              <a class="waves-effect waves-light btn modal-trigger block" ng-click="pilih_sesuatu('Pilih Hotel Jeddah','post_hotel_jeddah',value)">
              {{value.id_hotel_jeddah ? "Ganti Hotel Jeddah" : "Pilih Hotel Jeddah"}}
              </a>  
              <div ng-show="value.id_hotel_jeddah">
                <div></div>
                <h6 class="uppercase">{{ii.hotel_jeddah.nama_hotel}}</h6>
                <img class="responsive-img" width="100" src="<?php echo base_url('gudang/upload/hotel/thumb/{{ii.hotel_jeddah.gambar_hotel}}'); ?>">               
              </div>
              <!--  -->

            </div><!-- /.col -->
          </div>
          <div class="col l3 s12">
            <h5>Makanan</h5>
            <div class="col 12 block">
              <a class="waves-effect waves-light btn modal-trigger block" ng-click="pilih_sesuatu('Pilih Makanan','post_makanan',value)">
              {{value.id_makanan ? "Ganti Makanan" : "Pilih Makanan"}}
              </a>
                <div ng-show="value.id_makanan">
                  <div></div>
                  <h6 class="uppercase">{{ii.makanan.makanan}}</h6>
                <img class="responsive-img" width="100" src="<?php echo base_url('gudang/upload/makanan/thumb/{{ii.makanan.gambar_makanan}}'); ?>">
              </div>
            </div><!-- /.col -->
          </div>
          <div class="col l3 s12">
            <h5>Perlengkapan</h5>
            <div class="col 12 block">
              <a class="waves-effect waves-light btn modal-trigger block" ng-click="pilih_sesuatu('Pilih Perlengkapan','post_perlengkapan',value)">
              {{value.id_perlengkapan ? "Ganti Perlengkapan" : "Pilih Perlengkapan"}}
              </a>
                <div ng-show="value.id_perlengkapan">
                  <div></div>
                  <h6 class="uppercase">{{ii.perlengkapan.nama_perlengkapan}}</h6>
              </div>
            </div><!-- /.col -->
          </div>
        </div>
      </div>
    </div>
    <div id="Rute" class="col s12">
      <div class="card-panel z-depth-1">
      <div class="white black-text">
        
          <div class="row">
            <div class="col l2 s12 ta-l-s" align="right">Berangkat</div>
            <div class="col l3 s12">
              <select class="browser-default" ng-init="value.status_berangkat=''" name="berangkat_direct" ng-model="value.status_berangkat" class="form-control col-md-7 col-xs-12">
                <option value="">-Pilih Direct-</option>
                <option value="1">Transit</option>
                <option value="0">Langsung</option>
              </select>
            </div>
            <div class="input-field style-2 col l3 s12">
              <input type="text" name="berangkat" id="berangkat" placeholder="Contoh: JKT-MAD-MAK" ng-model="value.berangkat" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="row">
            <div class="col l2 s12 ta-l-s" align="right">Pulang</div>
            <div class="col l3 s12">
              <select class="browser-default" ng-init="value.status_pulang=''" name="pulang_direct" ng-model="value.status_pulang" class="form-control col-md-7 col-xs-12">
                <option value="">-Pilih Direct-</option>
                <option value="1">Transit</option>
                <option value="0">Langsung</option>
              </select>
            </div>
            <div class="input-field style-2 col l3 s12">
              <input type="text" name="pulang" id="pulang" placeholder="Contoh: JED-JKT " ng-model="value.pulang" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="row">
            <div class="col l2 s12 ta-l-s" align="right">Tampilkan :</div>
            <div class="col l3 s12">
            <div class="select-box">
              <select class="browser-default" ng-init="value.tampilkan=''" ng-model="value.tampilkan" class="form-control col-md-7 col-xs-12">
                <option value="">Pilih Tampilkan</option>
                <option value="1">Tampilkan</option>
                <option value="0">Tidak Ditampilkan</option>
              </select>
              </div>
            </div>
          </div>
        
      </div>
    </div>
    </div>
    <div id="Keterangan" class="col s12">
      <div class="card-panel z-depth-1 row">
        <div class="col s12">
          <ul class="tabs" tabsangular>
            <li class="tab col s3"><a href="#Deskripsi" class="active">Deskripsi</a></li>
            <li class="tab col s3"><a href="#Itinerary">Itinerary</a></li>
            <li class="tab col s3"><a href="#persyaratan">persyaratan</a></li>
            <li class="tab col s3"><a href="#akomodasi">akomodasi</a></li>
            <li class="tab col s3"><a href="#including">including excluding</a></li>
            <li class="tab col s3"><a href="#info">info  tambahan</a></li>
          </ul>
        </div>
        <div id="Deskripsi" class="col s12">
          <div class="white black-text" style="padding: 5px 15px;" align="center">Deskripsi</div>
          <textarea ng-model="value.deskripsi" ui-tinymce="tinymceOptions" class="form-control col-md-7 col-xs-12" placeholder="Deskripsi"></textarea>
        </div>
        <div id="Itinerary" class="col s12">
          <div class="white black-text" style="padding: 5px 15px;" align="center">Itinerary</div>
          <textarea ng-model="value.itinerary" ui-tinymce="tinymceOptions" class="form-control col-md-7 col-xs-12" placeholder="Itinerary"></textarea>
        </div>
        <div id="persyaratan" class="col s12">
          <div class="white black-text" style="padding: 5px 15px;" align="center">Persyaratan</div>
          <textarea ng-model="value.persyaratan" ui-tinymce="tinymceOptions" class="form-control col-md-7 col-xs-12" placeholder="Persyaratan"></textarea>
        </div>
        <div id="akomodasi" class="col s12">
          <div class="white black-text" style="padding: 5px 15px;" align="center">Akomodasi</div>
          <textarea ng-model="value.akomodasi" ui-tinymce="tinymceOptions" class="form-control col-md-7 col-xs-12" placeholder="Akomodasi"></textarea>
        </div>
        <div id="including" class="col s12">
          <div class="white black-text" style="padding: 5px 15px;" align="center">Including Excluding</div>
          <textarea ng-model="value.including" ui-tinymce="tinymceOptions" class="form-control col-md-7 col-xs-12" placeholder="Including Excluding"></textarea>
        </div>
        <div id="info" class="col s12">
          <div class="white black-text" style="padding: 5px 15px;" align="center">Info  Tambahan</div>
          <textarea ng-model="value.info_tambahan" ui-tinymce="tinymceOptions" class="form-control col-md-7 col-xs-12" placeholder="Info  Tambahan"></textarea>
        </div>
      </div>
    </div>

  </div>
  
</div>

</div><!-- /content -->
  </form>
  <script> 
  $(function() { 
    $('.currency').maskMoney({thousands:'.', decimal:'.', precision:'0'}); 
  }) 
</script>