  <form ng-submit="save(ii.edit_jadwal)" ng-init="load_tambah('active')" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Edit Jadwal">Edit Jadwal</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/jamaah_v2/data_jamaah');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
        <!-- <li><a ng-click="save(value)"><span class="icon diskette"></span> Simpan</a></li> -->
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/jamaah/data_jamaah');?>">Data Jamaah</a></li>
      <li class="active"><a href="#">Edit Jadwal </a></li>
    </ul>
  </div>
<div class="content">
  <div class="row">

    <div class="col s12">
      <div class="col s3" align="right">Jadwal Sekarang :</div>
      <div class="input-field style-2 col s8">
        {{ii.edit_jadwal.nama_paket}}  -  {{ii.edit_jadwal.tanggal}}

      </div>
      <div class="col s3" align="right">Edit Jadwal :</div>
      <div class="input-field style-2 col s4">
        <div class="select-box">
        <select class="browser-default" ng-init="ii.edit_jadwal.id_paket=''" ng-model="ii.edit_jadwal.id_paket" ng-change="pilihpaket(ii.edit_jadwal.id_paket)">
          <option value="">Pilih Paket</option>
          <option ng-repeat="r in ii.paket" value="{{r.id_paket}}"> {{r.nama_paket}}</option>
        </select>        
        </div>
      </div>
      <div class="input-field style-2 col s4"  ng-hide="loop_jadwal == ''">
        <div class="select-box" ng-show="loop_jadwal" >
        <select class="browser-default" ng-init="ii.edit_jadwal.id_jadwal_baru=''" ng-model="ii.edit_jadwal.id_jadwal_baru">
          <option value="">Pilih Jadwal</option>
          <option ng-repeat="r in loop_jadwal" value="{{r.id_jadwal}}"> {{r.nama_paket}} - {{r.tanggal}}</option>
        </select>        
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12">
      <h4>History</h4>
      <hr>
    </div>
     <div class="col s12">
       <table>
         <thead>
           <th>Tanggal</th>
           <th>Jadwal Sebelumnya</th>
           <th>Jadwal Baru</th>
         </thead>
         <tbody>
           <tr ng-repeat="r in ii.history_edit_jadwal">
             <td>{{r.tanggal}}</td>
             <td>{{r.nama_paket_sebelumnya}} - {{r.tanggal_sebelumnya}}</td>
             <td>{{r.nama_paket_sekarang}} - {{r.tanggal_sekarang}}</td>
           </tr>
         </tbody>
       </table>
     </div>
  </div>
</div>
</form>
