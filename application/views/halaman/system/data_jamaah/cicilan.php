  <form ng-submit="save_cicilan(ii.cicilan)" ng-init="load_tambah('active')" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Cicilan">Pembayaran</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/jamaah/data_jamaah');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
        <!-- <li><a ng-click="save(value)"><span class="icon diskette"></span> Simpan</a></li> -->
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/jamaah/data_jamaah');?>">Data Jamaah</a></li>
      <li class="active"><a href="#">Pembayaran </a></li>
    </ul>
  </div>
<div class="content">
  <div class="card-panel z-depth-0">
    <div class="row">
    <h5>Data Jamaah</h5>
      <div class="col s3" align="right">Nama Lengkap :</div>
      <div class="input-field style-2 col s8">
        <div>{{ii.cicilan.nama_lengkap}}</div>
      </div>

      <div class="col s3" align="right">Total Biaya :</div>
      <div class="input-field style-2 col s8">
        <div>{{ii.cicilan.harga}}</div>
      </div>
      <div class="col s3" align="right">Sisa Pembayaran :</div>
      <div class="input-field style-2 col s8">
        <div>{{ii.sisa_pembayaran}}</div>
      </div>
      </div>
  </div>
  <div class="card-panel z-depth-0">
      <div class="row">
      <div class="col s12"> <h5>Pembayaran</h5></div>

      <div class="col s3" align="right">Nominal :</div>
      <div class="input-field style-2 col s8">
        <input type="text" data-inputmask="'mask': '99.999.999'" ng-model="ii.cicilan.nominal_cicilan">
      </div>
      </div>
  </div>
  <div class="card-panel z-depth-0">
  <div class="row">
      <div class="col s12"> <h5>History Pembayaran</h5></div>
      <div class="col s12">
        <table>
          <thead>
            <th>Tanggal</th>
            <th>Nominal</th>
          </thead>
          <tbody>
            <tr ng-repeat="r in ii.history_cicilan">
              <td>{{r.tanggal}}</td>
              <td>{{r.nominal}}</td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
  </div>
    </div>
</div>
</div>
</form>
