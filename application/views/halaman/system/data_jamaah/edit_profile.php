  <form ng-submit="save(value)" ng-init="load_tambah()"  class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Edit Profile">Edit Profile</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/system/data_jamaah');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
        <!-- <li><a ng-click="save(value)"><span class="icon diskette"></span> Simpan</a></li> -->
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/system/data_jamaah');?>">Data Jamaah</a></li>
      <li class="active"><a href="#">Edit Profile </a></li>
    </ul>
  </div>
<div class="content">
  <div class="row">
    <div class="col s12" align="center" style="font-weight: bold;">INPUT DATA JAMAAH</div>
    <div class="col s12"><h6>&nbsp;</h6></div>

    <div class="col s12">
      <div class="col s3" align="right">Tipe Paket :</div>
      <div class="input-field style-2 col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.tipe_paket=''" ng-model="value.tipe_paket">
          <option value="">Tipe Paket</option>
          <option value="Quad">Quad</option>
          <option value="Triple">Triple</option>
          <option value="Double">Double</option>
        </select>        
        </div>
      </div>

      <div class="col s3 ta-l-s" align="right">Pilih Travel :</div>
      <div class="input-field style-2 col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.id_travel=''" ng-model="value.id_travel">
          <option value="">Pilih Travel</option>
          <option ng-repeat="r in ii.travel" value="{{r.id_travel}}">{{r.nama_travel}}</option>
        </select>        
        </div>
      </div>

      <div class="col s3" align="right">Nama Sesuai Pasport :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.nama_passport">
      </div>
      <div class="col s3" align="right">No Passport :</div>
      <div class="input-field style-2 col s8">
        <!-- <input type="text" data-inputmask="'mask': 'a-999999'" ng-model="value.no_passport"> -->
        <input type="text" ng-model="value.no_passport">
      </div>
      <div class="col s3" align="right">Tempat Dikeluarkan :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.tempat_dikeluarkan">
      </div>
      <div class="col s3" align="right">Masa Berlaku :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.masa_berlaku" data-inputmask="'mask': '99-99-9999'" placeholder="ex: 30-12-1990">
      </div>
      <div class="col s3" align="right">Nama Lengkap :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.nama_lengkap">
      </div>
      <div class="col s3" align="right">Nama Panggilan :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.nama_panggilan">
      </div>
      <div class="col s3" align="right">Tempat Lahir :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.tempat_lahir">
      </div>
      <div class="col s3" align="right">Tanggal Lahir :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.tanggal_lahir" data-inputmask="'mask': '99-99-9999'" placeholder="ex: 30-12-1990">
      </div>
      <div class="col s3" align="right">Keterangan Mahram :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.keterangan_mahram">
      </div>
      <div class="col s3" align="right">Jenis Kartu Identitas :</div>
      <div class="input-field col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.jenis_kartu_identitas=''" ng-model="value.jenis_kartu_identitas">
          <option value="">-Pilih Jenis Kartu-</option>
          <option>KTP</option>
          <option>SIM</option>
        </select>
        </div>
      </div>
      <div class="col s3" align="right">Nomor Kartu Identitas :</div>
       <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.nomor_kartu_identitas">
      </div>           
      <div class="col s3" align="right">Jenis Kelamin :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.jenis_kelamin">
      </div>            
      <div class="col s3" align="right">Nama Ayah :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.nama_ayah">
      </div>
      <div class="col s3" align="right">Status Perkawinan :</div>
      <div class="input-field col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.status_perkawinan=''" ng-model="value.status_perkawinan">
          <option value="">-Pilih Status Perkawinan-</option>
          <option>Lajang</option>
          <option>Menikah</option>
          <option>Janda</option>
          <option>Duda</option>
        </select>
        </div>
      </div>
      <div class="col s3" align="right">Jumlah Tanggungan :</div>
      <div class="input-field col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.jumlah_tanggungan=''" ng-model="value.jumlah_tanggungan">
          <option value="">-Pilih Jumlah-</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          <option>13</option>
          <option>14</option>
          <option>15</option>
        </select>
        </div>
      </div>
      <div class="col s3" align="right">Kewarganegaraan :</div>
      <div class="input-field col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.kewarganegaraan=''" ng-model="value.kewarganegaraan">
          <option value="">-Pilih Status-</option>
          <option>INDONESIA</option>
          <option>LAINNYA</option>
        </select>
        </div>
      </div>
      <div class="col s3" align="right">Pendidikan Terakhir :</div>
      <div class="input-field col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.pendidikan_terakhir=''" ng-model="value.pendidikan_terakhir">
          <option value="">-Pilih Pendidikan Terakhir-</option>
          <option>SD-SMP</option>
          <option>SMA</option>
          <option>D1-D3</option>
          <option>S1</option>
          <option>S2</option>
          <option>S3</option>
        </select>
        </div>
      </div>

      
        <div class="col s12" ng-controller="PilihprovinsiController">
          <div class="col s3" align="right">Provinsi & Kota / Kabupaten :</div>
          <div class="input-field style-2 col s4">
            <div class="select-box">
            <select class="browser-default" ng-init="value.id_provinsi=''" ng-model="value.id_provinsi" ng-change="pilihprovinsi(value.id_provinsi)">
              <option value="">Pilih Provinsi</option>
              <option ng-repeat="r in ii.provinsi" value="{{r.id_prov}}"> {{r.nama_prov}}</option>
            </select>        
            </div>
          </div>
          <div class="input-field style-2 col s4"  ng-hide="loop_kabupaten == ''">
            <div class="select-box" ng-show="loop_kabupaten" >
            <select class="browser-default" ng-init="value.id_kab=''" ng-model="value.id_kab">
              <option value="">Pilih Kota / Kabupaten</option>
              <option ng-repeat="r in loop_kabupaten" value="{{r.id_kab}}"> {{r.nama_kab}}</option>
            </select>        
            </div>
          </div>
        </div>


      <div class="col s3" align="right">Alamat Rumah Lengkap :</div>
       <div class="input-field col s8">
        <textarea ng-model="value.alamat_rumah"></textarea>
      </div>
      <div class="col s3" align="right">Alamat Kantor Lengkap :</div>
       <div class="input-field col s8">
        <textarea ng-model="value.alamat_kantor"></textarea>
      </div>
      <div class="col s3" align="right">No Handphone :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.no_telp">
      </div>
      <div class="col s3" align="right">Email :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.email">
      </div>
  <div class="col s3" align="right">Status Pekerjaan :</div>
      <div class="input-field col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.status_pekerjaan=''" ng-model="value.status_pekerjaan">
          <option value="">-Pilih Status-</option>
          <option>TETAP</option>
          <option>KONTRAK</option>
          <option>PARUH WAKTU</option>
        </select>
        </div>
      </div>
      <div class="col s3" align="right">Status Keluarga :</div>
      <div class="input-field style-2 col s8">
        <input type="text" ng-model="value.status_keluarga">
      </div>
<div class="col s3" align="right">Ukuran Baju :</div>
      <div class="input-field col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.size_baju=''" ng-model="value.size_baju">
          <option value="">-Pilih Ukuran-</option>
          <option>S</option>
          <option>M</option>
          <option>L</option>
          <option>XL</option>
          <option>XXL</option>
          <option>XXXL</option>
          <option>XXXXL</option>
        </select>
        </div>
      </div>
</div>
  </div>
  <h5>&nbsp;</h5>

  <div class="row">
    <div class="col s12" align="center" style="font-weight: bold;">DALAM KEADAAN DARURAT SIAPAKAH KERABAT ANDA YANG DAPAT KAMI HUBUNGI</div>
    <div class="col s12"><h6>&nbsp;</h6></div>

    <div class="col s3" align="right">Nama Lengkap :</div>
    <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.nama_kerabat">
    </div>
    <div class="col s3" align="right">Alamat Tinggal Lengkap :</div>
    <div class="input-field col s8">
      <textarea ng-model="value.alamat_kerabat"></textarea>
    </div>
    <div class="col s3" align="right">No Telepon :</div>
    <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.no_hp_kerabat">
    </div>
    <div class="col s3" align="right">Email :</div>
    <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.email_kerabat">
    </div>
  </div>
  <h5>&nbsp;</h5>

  <div class="row">
    <div class="col s12" align="center" style="font-weight: bold;text-transform: uppercase;">Jika Telah Menikah, Mohon Lengkapi Data Suami/Istri Anda</div>
    <div class="col s12"><h6>&nbsp;</h6></div>

    <div class="col s3" align="right">Nama Lengkap :</div>
    <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.nama_pasangan">
    </div>
    <div class="col s3" align="right">Tempat Lahir :</div>
    <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.tempat_lahir_pasangan">
    </div>
    <div class="col s3" align="right">Tanggal Lahir :</div>
    <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.tanggal_lahir_pasangan" data-inputmask="'mask': '99-99-9999'" placeholder="ex: 30-12-1990">
    </div>
    <div class="col s3" align="right">Kewarganegaraan :</div>
    <div class="input-field col s8">
      <div class="select-box">
      <select class="browser-default" ng-init="value.kewarganegaraan_pasangan=''" ng-model="value.kewarganegaraan_pasangan">
        <option value="">-Pilih Status-</option>
        <option>INDONESIA</option>
        <option>LAINNYA</option>
      </select>
      </div>
    </div>
    <div class="col s3" align="right">Jenis Kartu Identitas :</div>
    <div class="input-field col s8">
      <div class="select-box">
      <select class="browser-default" ng-init="value.jenis_kartu_pasangan=''" ng-model="value.jenis_kartu_pasangan">
        <option value="">-Pilih Jenis Kartu-</option>
        <option>KTP</option>
        <option>SIM</option>
      </select>
      </div>
    </div>
    <div class="col s3" align="right">Nomor Kartu Identitas :</div>
    <div class="input-field style-2 col s8">
      <input type="text" ng-model="value.nomor_kartu_pasangan">
    </div>      
    <div class="col s3" align="right">Alamat Lengkap :</div>
    <div class="input-field col s8">
      <textarea ng-model="value.alamat_pasangan"></textarea>
    </div> 

  </div>
</div>
</div>
</form>
    <script>
    $(document).ready(function() {
      $(":input").inputmask();
    });
  </script>