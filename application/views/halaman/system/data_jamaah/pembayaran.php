<form ng-submit="save(value)" ng-init="load_tambah('active')" class="row">
  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="History Pembayaran">History Pembayaran</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/jamaah_v2/data_jamaah');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
        <!-- <li><a ng-click="save(value)"><span class="icon diskette"></span> Simpan</a></li> -->
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/jamaah_v2/data_jamaah');?>">Data Jamaah</a></li>
      <li class="active"><a href="#">History Pembayaran </a></li>
    </ul>
  </div>
	<div class="content">
    <div class="">
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>Nominal</th>
            <th>Tanggal</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="r in ii.history_pembayaran">
            <td>{{$index+1}}</td>
            <td>{{r.nominal | currency}}</td>
            <td>{{r.tanggal | date}}</td>
          </tr>
        </tbody>
      </table>
		</div>
	</div>
</form>