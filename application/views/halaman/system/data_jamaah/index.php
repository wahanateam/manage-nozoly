
          <nav class="blue z-depth-0">
            <div class="nav-wrapper">
              <a href="#" class="brand-logo truncate" title="Data Jamaah">Data Jamaah</a>
              <ul class="right">
                <li><a href="<?php echo base_url('page/system/data_jamaah/input');?>"><span class="icon copy-file"></span> Tambah</a></li>
              </ul>
            </div>
          </nav>
          <div class="steps-container">
            <ul class="steps">
              <li><a href="#">Home</a></li>
              <li><a href="#">Jamaah</a></li>
              <li class="active"><a href="#">Data Jamaah</a></li>
            </ul>
            <div class="pencarian">
              <div class="search-form">
                <input id="text" type="text" placeholder="Pencarian.." ng-keyup="caridatatables(caridata)" ng-model="caridata">
                <span class="icon search"></span>
              </div>
              <button type="button" class="lanjutan btn btn-primary" ng-click="pencarianLanjutan_jamaahv2()"><span class="icon edit"></span></button>
            </div>
          </div>

<div class="content" ng-controller="DataJamaah">
<!--   <a class="tooltipped" href="<?php echo base_url('page/master/admin/input/'.$this->session->userdata('id_admin'))?>" data-position="bottom" data-delay="50" data-tooltip="Pengaturan Akun"><span class="icon config"></span></a> -->
  
  <table id="datatables" class="mdl-data-table">
      <thead>
          <tr class="blue lighten-1 white-text">
                <th>No.</th>
                <th>Nama Jamaah</th>
                <th>Nama Paket</th>
                <th>Jadwal Keberangkatan</th>
                <th>Action</th>
          </tr>
      </thead>
  </table>
</div>