  <form class="row" ng-init="load_tambah('active')" enctype="multipart/form-data">
  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Dokumen">Dokumen</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/jamaah_v2/data_jamaah');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <!-- <li><a ng-click="save(ii.dokumen)"><span class="icon diskette"></span> Simpan</a></li> -->
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/jamaah_v2/data_jamaah');?>">Data Jamaah</a></li>
      <li class="active"><a href="#">Dokumen </a></li>
      <!-- <small class="red white-text right">* Data boleh dikosongkan jika belum lengkap.</small> -->
    </ul>
  </div>
<div class="content">
  <div class="row">
    <div class="col s12">
      <div class="col s3" align="right">KTP :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.ktp" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.ktp" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.ktp">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
            <input type="file" ngf-select ng-model="ii.dokumen.ktp" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'ktp')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">KK :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.kk" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.kk" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.kk">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
        <input type="file" ngf-select ng-model="ii.dokumen.kk" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'kk')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">Passport :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.passport" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.passport" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.passport">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
        <input type="file" ngf-select ng-model="ii.dokumen.passport" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'passport')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">Buku Kuning :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.buku_kuning" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.buku_kuning" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.buku_kuning">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
        <input type="file" ngf-select ng-model="ii.dokumen.buku_kuning" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'buku_kuning')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">Akte Lahir :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.akte_lahir" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.akte_lahir" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.akte_lahir">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
        <input type="file" ngf-select ng-model="ii.dokumen.akte_lahir" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'akte_lahir')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">Ijazah :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.ijazah" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.ijazah" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.ijazah">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
            <input type="file" ngf-select ng-model="ii.dokumen.ijazah" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'ijazah')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">Buku Nikah :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.buku_nikah" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.buku_nikah" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.buku_nikah">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
        <input type="file" ngf-select ng-model="ii.dokumen.buku_nikah" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'buku_nikah')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">Foto :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.foto" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.foto" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.foto">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
        <input type="file" ngf-select ng-model="ii.dokumen.foto" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'foto')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

      <div class="col s3" align="right">KTP Ortu :</div>
      <div class="input-field col s8">
        <div ng-show="ii.dokumen.ktp_ortu" style="margin-bottom:10px;">
          <img alt="" ngf-thumbnail="ii.dokumen.ktp_ortu" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
        </div>
        <input type="hidden" ng-model="ii.dokumen.ktp_ortu">
        <div class="file-field input-field col s12 no-padding">
          <div class="btn z-depth-0 blue lighten-1">
            <span>Photo</span>
            <input type="file" ngf-select ng-model="ii.dokumen.ktp_ortu" ngf-pattern="'image/*'" ngf-accept="'image/*'" ng-change="save_dokumen(ii.dokumen,'ktp_ortu')">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

    </div>
</div>
</div>

</form>
