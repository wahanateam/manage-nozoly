  <form ng-submit="save(value)" ng-init="load_tambah()" class="row">

  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Jabatan">Input Pesawat</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/system/pesawat');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
        <!-- <li><a ng-click="save(value)"><span class="icon diskette"></span> Simpan</a></li> -->
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/system/pesawat');?>">Pesawat</a></li>
      <li class="active"><a href="#">Input Pesawat</a></li>
    </ul>
  </div>

<div class="content">

<!--       <div class="col s3" align="right">Jabatan :</div>
      <div class="input-field style-2 col s8">
        <div class="select-box">
        <select class="browser-default" ng-init="value.id_jabatan=''" ng-model="value.id_jabatan">
          <option value="">Pilih Jabatan</option>
          <option ng-repeat="r in ii.jabatan" value="{{r.id_jabatan}}"> {{r.nama_jabatan}}</option>
        </select>
        </div>
      </div> -->

      <div class="col l3 s12 ta-l-s" align="right">Nama Pesawat :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.nama_pesawat">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Keterangan :</div>
      <div class="input-field col l8 s12">
        <textarea ng-model="value.keterangan" ui-tinymce="tinymceOptions" name="keterangan" class="form-control col-md-7 col-xs-12"></textarea>
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Gambar :</div>
      <div class="input-field col l8 s12">
        <div class="btn btn-info" ngf-select ng-model="value.gambar" ngf-pattern="'image/*'"    ngf-accept="'image/jpeg,image/x-png'" ngf-multiple="true" ><span class="fa fa-camera"></span> Select</div>
        <div>Gambar harus memiliki dimensi 800 x 800 untuk hasil maximal</div>
      </div>

      <div class="col l3 s12 ta-l-s" align="right">&nbsp;</div>
      <div class="input-field col l8 s12">
        <ul class="gallery-preview">
          <li  ng-repeat="f in value.gambar" style="font:smaller">
            <img ngf-thumbnail="f || '/thumb.jpg'" ngf-size="{height:86 , quality: 0.9}">
          </li>
        </ul>
      </div>

      <div ng-if="value.id_pesawat">
        <div class="col l3 s12 ta-l-s" ng-init="load_tambah('active')">
          <div class="input-field col l8 s12" ng-repeat="r in ii.gambar">
            <div class="gambar-kotak" >
              <img ng-src="<?php echo base_url('gudang/upload/pesawat/{{r.gambar}}');?>" class="img-responsive" preload-image  default-image="<?php echo base_url('gudang/upload/no_image.jpg');?>" fallback-image="<?php echo base_url('gudang/upload/no_image.jpg');?>"  style="width:90px;">
              <a ng-click="deletegambarmodal($index,r.id_gambar)" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</a>
            </div>
          </div>
        </div>
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Logo :</div>
      <div class="input-field col l8 s12">
       <div ng-show="value.logo" style="margin-bottom:10px;">
         <img ngf-thumbnail="value.logo" style="width:150px" ngf-size="{width: 150, height:200 , quality: 0.9}">
       </div>
      <div class="btn btn-info" ngf-select ng-model="value.logo" ngf-pattern="'image/*'"
      ngf-accept="'image/*'"><span class="fa fa-camera"></span> Select</div>
      Logo wajib memiliki dimensi 130 x 130 untuk hasil maximal
     </div>
<!-- 

      <div class="col s3" align="right"></div>
      <div class="input-field col s8" style="margin-top: 20px;">
        <button type="submit" class="btn-flat btn-large block white-text green darken-1 block waves-effect waves-light"><span class="icon download"></span> Simpan</button>
      </div> -->
</div>
  </form>

  <div id="modalDelete" class="modal modal-fixed-footer" style="width:30%;height:300px">
    <div class="modal-content" align="center">
      <h4>Konfirmasi Hapus </h4>
      <p>Apakah Anda yakin akan hapus gambar ini ?</p>
      <span class="icon trash" style="font-size:6em;margin-top:10px;"></span>
    </div>
    <div class="modal-footer" align="center">
      <a href="#!" class=" modal-action modal-close waves-effect btn-flat" style="float:none">Batal</a>
      <a ng-click="deletegambar()" class="blue darken-1 white-text modal-action modal-close waves-effect btn-flat" style="float:none"><span class="icon check"></span> Setuju</a>
    </div>
  </div>