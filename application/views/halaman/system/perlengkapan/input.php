  <form ng-submit="save(value)" class="row">
  <nav class="blue z-depth-0">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo truncate" title="Input Perlengkapan">Input Perlengkapan</a>
      <ul class="right">
        <li><a href="<?php echo base_url('page/system/perlengkapan');?>"><span class="icon angle-left"></span> Kembali</a></li>
        <li class=" waves-effect waves-light"><button type="submit" class="transparent btn z-depth-0"><span class="icon diskette"></span> Simpan</button></li>
      </ul>
    </div>
  </nav>
  <div class="steps-container">
    <ul class="steps">
      <li><a href="/">Home</a></li>
      <li><a href="<?php echo base_url('page/system/perlengkapan');?>">Perlengkapan</a></li>
      <li class="active"><a href="#">Input Perlengkapan</a></li>
    </ul>
  </div>

<div class="content">
      <div class="col l3 s12 ta-l-s" align="right">Nama Perlengkapan :</div>
      <div class="input-field style-2 col l8 s12">
        <input type="text" ng-model="value.nama_perlengkapan">
      </div>

      <div class="col l3 s12 ta-l-s" align="right">Perlengkapan :</div>
      <div class="input-field col l8 s12">
        <input type="checkbox" class="filled-in" id="tas" ng-model="value.tas" ng-true-value="'1'" ng-false-value="'0'" />
        <label for="tas">Tas Selempang / Tas Dokumen</label>
        <br>
        <input type="checkbox" class="filled-in" id="koper_bagasi" ng-model="value.koper_bagasi" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="koper_bagasi">koper bagasi</label>
        <br>
        <input type="checkbox" class="filled-in" id="koper_kabin" ng-model="value.koper_kabin" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="koper_kabin">koper kabin</label>
        <br>
        <input type="checkbox" class="filled-in" id="jaket" ng-model="value.jaket" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="jaket">jaket</label>
        <br>
        <input type="checkbox" class="filled-in" id="baju_koko" ng-model="value.baju_koko" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="baju_koko">baju koko</label>
        <br>
        <input type="checkbox" class="filled-in" id="ihrom" ng-model="value.ihrom" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="ihrom">kain ihrom</label>
        <br>
        <input type="checkbox" class="filled-in" id="abaya" ng-model="value.abaya" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="abaya">abaya</label>
        <br>
        <input type="checkbox" class="filled-in" id="buku_doa" ng-model="value.buku_doa" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="buku_doa">buku doa</label>
        <br>
        <input type="checkbox" class="filled-in" id="buku_panduan" ng-model="value.buku_panduan" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="buku_panduan">buku panduan</label>
        <br>
        <input type="checkbox" class="filled-in" id="buku_itinerary" ng-model="value.buku_itinerary" ng-true-value="'1'" ng-false-value="'0'"/>
        <label for="buku_itinerary">buku itinerary</label>
      </div>

  </form>
</div>


