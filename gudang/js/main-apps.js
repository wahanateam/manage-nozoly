var AppController = angular.module('AppController', []);

var ipurl = "http://api.wahanahajiumrah.com/" ;

AppController.controller('CmsController',['$scope','Upload','$routeParams','$http','$location','notifications','$filter','$rootScope','$route','toasty','$parse',
  function($scope,Upload,$routeParams,$http,$location,notifications,filter,$rootScope,$route,toasty,$parse){
$scope.value = {};

  $rootScope.gantijudul = function (argument) {
  document.title = argument;
  }
  $scope.sortableOptions = {
    placeholder: "item",
    connectWith: ".apps-container",
    items: "> li"
  };

  // $scope.mCustomScrollbar = {
  //   "autoHideScrollbar":true,
  //   theme:"dark"
  // };

      // Validasi form
      // $('form').valida();
      // Menambahkan validasi ke semua inputan
      // $('form input, form select, form textarea').attr('required',true);
          // $(":input").inputmask();
      // $('.content').addClass('mCustomScrollbar');
   
  $rootScope.getClass = function (path) {
    var current = $location.absUrl();
    return (current === path) ? 'sidebar-active' : '';
  }

  $scope.tinymceOptions = {
    onChange: function(e) {
      // put logic here for keypress and cut/paste changes
    },
    inline: false,
    height: 300,
    plugins: [
         "advlist autolink link image lists charmap  preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking fullscreen",
         "table contextmenu directionality emoticons paste textcolor  code"
   ],
    toolbar: 'insertfile undo redo | styleselect fontsizeselect | bold italic | link image | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify',
    skin: 'lightgray',
    theme : 'modern',
    external_filemanager_path:"/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: {"filemanager" : ADMIN_URL+"/filemanager/plugin.min.js"
    }
  };

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;
 var directURL   = 'page/' + $routeParams.target + "/"+$routeParams.kondisi;
 var cariurl = $location.search();
    if(cariurl.id_page != null){
      var id_page_plus ="?id_page="+cariurl.id_page; 
    }else{
      var id_page_plus =""; 
    }

 $scope.cari = cariurl.cari;

  var datatables = $('#datatables').DataTable({
 
        "processing": true, //Feature control the processing indicator.
        "filter" : false,
        "bLengthChange" : false,
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "ajax": {
            "url": templateUrl+id_page_plus,
            "type": "POST"
        },
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
            "className": 'mdl-data-table__cell--non-numeric'
        },
        ]
 
    });
  
 $rootScope.caridatatables = function(data){
   datatables.ajax.url(templateUrl+'?cari='+data).load();
 }

    if($routeParams.id == null){
      if ($routeParams.target  == "home" && $routeParams.kondisi  == "dashboard") {
        $http.get(templateUrl).success(function (response) {

              // angular.forEach(response.pending, function(value, key) {
              //   this.push(value.tanggal);
              // }, tanggal);

                $scope.chartOptions = {
                    title: {
                        text: 'Analitik Pengunjung'
                    },
                    xAxis: {
                        categories: response.anality_artikel.tanggal,
                    },

                    series: [{
                        name:'Artikel',
                        data: response.anality_artikel.artikel.hit
                    },{
                        name:'Catatan Ustad',
                        data: response.anality_artikel.custad.hit
                    },{
                        name:'Tanya Ustad',
                        data: response.anality_artikel.tustad.hit
                    }],
                };

                $scope.chartorderOptions = {
                    title: {
                        text: 'Analitik Order per jadwal keberangkatan'
                    },
                    xAxis: {
                        categories: response.anality_pendaftaran.tanggal,
                    },

                    series: [
                    {
                        name:'Pending',
                        data: response.anality_pendaftaran.pending.hit
                    },
                    {
                        name:'Lunas Pendaftaran',
                        data: response.anality_pendaftaran.lunas_pendaftaran.hit
                    },
                    {
                        name:'Lunas',
                        data: response.anality_pendaftaran.lunas.hit
                    },
                    {
                        name:'Kadaluwarsa',
                        data: response.anality_pendaftaran.kadaluwarsa.hit
                    },
                    ],
                };
                $scope.chartorderdutaOptions = {
                    title: {
                        text: 'Analitik Order per jadwal keberangkatan by Duta'
                    },
                    xAxis: {
                        categories: response.anality_pendaftaran_duta.tanggal,
                    },

                    series: [

                    {
                        name:'Lunas Pendaftaran',
                        data: response.anality_pendaftaran_duta.lunas_pendaftaran.hit
                    },
                    {
                        name:'Lunas',
                        data: response.anality_pendaftaran_duta.lunas.hit
                    },
                    ],
                };
                $scope.chartordercompareOptions = {
                  chart: {
                      type: 'pie'
                    },
                    title: {
                        text: 'Analitik Order'
                    },

                    series: [{
                            name: 'Jamaah',
                            colorByPoint: true,
                            data: [{
                                name: 'Pendaftaran Melalui Duta',
                                // color: '#000',
                                y: parseInt(response.anality_compare_order.duta)
                                
                            }, {
                                name: 'Pendaftaran Langsung',
                                // color: '#90ed7d',
                                y: parseInt(response.anality_compare_order.langsung)
                            }]
                          }],
                        };

        console.log("home")

        });

      }else if($routeParams.target  == "jamaah" && $routeParams.kondisi  == "rekap"){
        console.log("ini jamaah")
      }
    }else{
      $http({
          method: 'GET',
          url: templateUrl+'/'+$routeParams.id
        }).then(function successCallback(response) {
            $scope.value = response.data;
          }, function errorCallback(response) {
            
          });
    }



  $scope.load_tambah = function(i){
    if (i == "active") {
      var $plusID = "?id="+$routeParams.id;
    }else{
      var $plusID = "";
    }
    $http({
          method: 'GET',
          url: templateUrl+'_tambahan'+$plusID,
          cache: false
        }).then(function successCallback(response) {
            $scope.ii = response.data;

            $scope.deletegambarmodal = function(index,id_delete){    
                $('#modalGambarDelete').openModal();   
                $rootScope.deletegambar = function(){    
                           $http({
                             method: 'DELETE',
                             url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_gambar_delete/'+id_delete
                           }).then(function successCallback(r) {
                                 $('#modalGambarDelete').closeModal();
                                 $('#modalSub').closeModal();
                                  $scope.ii.gambar.splice(index, 1);
                             }, function errorCallback(r) {
                                  datatables.ajax.reload();
                             });
                }
            }


            
             

          }, function errorCallback(response) {
            
          });
  }


        //  $( document ).on( "click", "#gambarDelete", function() {
        //      $('#myModal').modal('show');    
        //      var id_delete = $(this).attr('id-delete');
        //             $rootScope.deletedatatables = function(){             
        //               $http({
        //                 method: 'DELETE',
        //                 url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_gambar_delete/'+id_delete
        //               }).then(function successCallback(response) {
        //                     
        //                      $scope.loop_harga_rate.result.splice(index, 1);
        //                 }, function errorCallback(response) {
        //                      datatables.ajax.reload();
        //                 });
        //              }
        // });
  $scope.load_addgallery = function (argument) {
      $http({
          method: 'GET',
          url: templateUrl+'/'+cariurl.id_page
        }).then(function successCallback(response) {
            $scope.value = response.data;
            console.log(response.data);
          }, function errorCallback(response) {
            
          });
  }
    
    $('#datatables tbody').on('click', '#DomListSub', function () {
     $('#modalSub').openModal();    
             var id_menu_admin = $(this).attr('id-menu-admin');
                      $http({
                        method: 'get',
                        url: ADMIN_URL+'api/master/sub_menu_admin?id='+id_menu_admin
                      }).then(function successCallback(response) {
                          $rootScope.sub = response.data;
                             // datatables.ajax.reload();
                        }, function errorCallback(response) {
                             // datatables.ajax.reload();
                        });
    });

   /*    $( document ).on( "click", "#DomListSub", function() {
             $('#modalSub').openModal();    
             var id_menu_admin = $(this).attr('id-menu');
                      $http({
                        method: 'get',
                        url: ADMIN_URL+'api/master/sub_menu_admin?id='+id_menu_admin
                      }).then(function successCallback(response) {
                          $scope.sub = response.data;
                             // datatables.ajax.reload();
                        }, function errorCallback(response) {
                             // datatables.ajax.reload();
                        });
        });*/

  $scope.save = function (argument) {
     tinyMCE.triggerSave();
      // var $btn = $("#myButton").button('loading');
       Upload.upload({ 
          url: templateUrl+'_post',
          data: argument
        }).then(function (response) {
/*
            notifications.showSuccess({
                message: response.data.messages,
                hideDelay: 3500, //ms
                hide: true //bool
             });*/

              toasty.success({
                title: "Input Sukses",
                msg: response.data.messages,
              });

                // $btn.button('reset');
                if(response.data.url != undefined){
                  console.log('ada url');
                   window.location.href = response.data.url;
                }else{
                   $location.path(directURL);
                }
            }, function (response) {
              toasty.error({
                title: "Input Gagal",
                msg: response.data.messages,
              });
            /*
                // $btn.button('reset');
                notifications.showError({
                    message: response.data.messages,
                    hideDelay: 3500, //ms
                    hide: true //bool
                 });*/
          });
        }
       $('#datatables tbody').on( "click", "#DomStatus", function() {
             var id = $(this).attr('id-status');
             var status = $(this).attr('status');
                      $http({
                        method: 'post',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&status='+status
                      }).then(function successCallback(response) {
                              toasty.info({
                                title: "Update Sukses",
                                msg: response.data.messages,
                              });
                             datatables.ajax.reload();
                        }, function errorCallback(response) {
                             datatables.ajax.reload();
                        });
        });
       $('#datatables tbody').on( "click", "#DomBatal", function() {
            $('#modalBatal').openModal();
             var id = $(this).attr('id-status');
             var id_order_detail = $(this).attr('id-order');
             $rootScope.ubahstatusbatal = function(){     
                      $http({
                        method: 'post',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&id_order_detail='+id_order_detail
                      }).then(function successCallback(response) {
                              toasty.info({
                                title: "Update Sukses",
                                msg: response.data.messages,
                              });
                             datatables.ajax.reload();
                        }, function errorCallback(response) {
                             datatables.ajax.reload();
                        });
            }
        });
       $('#datatables tbody').on( "click", "#DomDelete", function() {
             $('#modalDelete').openModal();
             var id_delete = $(this).attr('id-delete');
                    $rootScope.deletedatatables = function(){             
                      $http({
                        method: 'DELETE',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_delete
                      }).then(function successCallback(response) {
                              $('#modalSub').closeModal();

                              toasty.info({
                                title: "Delete Sukses",
                                msg: response.data.messages,
                              });

                             datatables.ajax.reload();
                        }, function errorCallback(response) {
                             datatables.ajax.reload();
                        });
                     }
        });
$scope.gantistatus = function(id,status) {
            $http({
                    method: 'post',
                    url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&status='+status
                  }).then(function successCallback(response) {
                          toasty.info({
                            title: "Update Sukses",
                            msg: response.data.messages,
                          });
                          $route.reload();
                        }, function errorCallback(response) {
                        });
}

  $scope.save_dokumen = function (argument,namafield) {
     tinyMCE.triggerSave();
      // var $btn = $("#myButton").button('loading');
       Upload.upload({ 
          url: templateUrl+'_dokumen_post?field='+namafield,
          data: argument
        }).then(function (response) {
              toasty.success({
                title: "Input Sukses",
                msg: response.data.messages,
              });

                if(response.data.url != undefined){
                  console.log('ada url');
                   // window.location.href = response.data.url;
                }else{
                   // $location.path(directURL);
                }
            }, function (response) {
              toasty.error({
                title: "Input Gagal",
                msg: response.data.messages,
              });
          });
        }      

  $scope.save_cicilan = function (argument) {
     tinyMCE.triggerSave();
      // var $btn = $("#myButton").button('loading');
       Upload.upload({ 
          url: templateUrl+'_cicilan_post',
          data: argument
        }).then(function (response) {
              toasty.success({
                title: "Input Sukses",
                msg: response.data.messages,
              });

                if(response.data.url != undefined){
                  console.log('ada url');
                   // window.location.href = response.data.url;
                  $route.reload();
                }else{
                   $location.path(directURL);
                }
            }, function (response) {
              toasty.error({
                title: "Input Gagal",
                msg: response.data.messages,
              });
          });
        }      

$scope.closemodal = function() {
    $('#modalSub').closeModal();
}
$scope.refresh = function() {
  // console.log('fuck');
  $route.reload();
}
$scope.pilih_sesuatu = function(judul,senddata,value){
  $rootScope.judul_modal_transportasi = judul;    
                     $http({
                        method: 'POST',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+senddata
                      }).then(function successCallback(response) {
                          $rootScope.datapaketselect = response.data;
                                      $rootScope.pilihfitur = function(id,$type){
                                       /*   oke = {
                                            $type : id
                                          };*/   $http({
                                                    method: 'GET',
                                                    url: ADMIN_URL+'api/' + $routeParams.target+'/'+senddata+'_hasil?id='+id
                                                  }).then(function successCallback(response) {       
                                                        var key = "id_"+$type;
                                                        var oke = {};
                                                        oke[key] = id;
                                                        angular.extend(value,oke);
                                                          toasty.info({
                                                            title: "Pilih "+$type,
                                                            msg: "Berhasil Memilih "+ $type,
                                                          });

                                                         var vardinamis = "ii."+$type;
                                                         var vardinamis = $parse(vardinamis);
                                                         vardinamis.assign($scope, response.data); 

                                                       // console.log($scope.hotel_madinah); 
                                                    $('#modalpaketselect').closeModal();
                                                    }, function errorCallback(response) {
                                                          toasty.info({
                                                            title: "Pilih "+$type,
                                                            msg: "Gagal Memilih "+ $type+", Silakan coba lagi",
                                                          });
                                                    $('#modalpaketselect').closeModal();
                                                    });
                                      }
                   }, function errorCallback(response) {

                        });
 $('#modalpaketselect').openModal();
}

  $scope.ustadModal = function(id_delete) {
    $('#ustadModal').openModal();
      $rootScope.ustaddelete = function() {
       $http({
         method: 'DELETE',
         url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_delete
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Delete Sukses",
                 msg: response.data.messages,
               });
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
     }
  };
  $scope.editpertanyaan = function(id) {
    $http({
           method: 'get',
           url: ADMIN_URL+'api/halaman_depan/tanya_ustad/'+id
         }).then(function successCallback(response) {
             $rootScope.pertanyaan = response.data;
             $('#komenModal').openModal();
           });
      $rootScope.komen = function(argument) {
       $http({
         method: 'post',
         url: templateUrl+'_komen_post',
         data: argument
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Update Sukses",
                 msg: response.data.messages,
               });
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
     }
  };

  $scope.editkomen = function(id) {
    $http({
           method: 'get',
           url: ADMIN_URL+'api/halaman_depan/tanya_ustad/'+id
         }).then(function successCallback(response) {
             $rootScope.pertanyaan = response.data;
             $('#komenModal').openModal();
           });
      $rootScope.komen = function(argument) {
       $http({
         method: 'post',
         url: templateUrl+'_komen_post',
         data: argument
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Update Sukses",
                 msg: response.data.messages,
               });
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
     }
  };
  $scope.gantistatus = function(id,status) {
       $http({
         method: 'post',
         url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&status='+status
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Update Sukses",
                 msg: response.data.messages,
               });
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
  };
  $scope.status = function(index,id,status) {
            console.log(index);
            $http({
              method: 'POST',
              url: templateUrl+'_status?id='+id+'&status='+status,
            }).then(function successCallback(response) {
                
              $scope.status = response.data.status;

              notifications.showSuccess({
                message: response.data.messages,
                hideDelay: 3500, //ms
                hide: true //bool
             });
              }, function errorCallback(response) {

              });
  };
  $scope.pilih_hotel_tambahan = function (argument) {
      $http({
          method: 'GET',
          url: ADMIN_URL+'api/paket/hotel_tambahan?id='+argument,
        }).then(function successCallback(response) {
            $scope.loop_hotel_tambahan = response.data;
        }, function errorCallback(response) {
              
        });
  }
  $scope.pilihpaket = function (argument) {
      $http({
          method: 'GET',
          url: ADMIN_URL+'api/jamaah/get_jadwal?id='+argument,
        }).then(function successCallback(response) {
            $scope.loop_jadwal = response.data;
        }, function errorCallback(response) {
              
        });
  }
  $scope.uploadalbum = function(value) {
    console.log(value);
     $scope.notif = [];
     Upload.upload({ 
              url: templateUrl+'_post', 
              data: { 
                      gambar    : $scope.files, 
                      id_album  : value.id_album, 
                      tanggal   : value.tanggal, 
                      judul     : value.judul 
                    } 
          }).then(function (resp) { 
              $route.reload();
              $scope.notif = resp.data; 
              datatables.ajax.reload(); 

              toasty.success({
                title: "Input Sukses",
                msg: resp.data
              });

          }, function (resp) { 
           //   console.log('Error status: ' + resp.status); 
          }); 
    };  
  $scope.uploadvideo = function(value) {
    console.log(value);
     $scope.notif = [];
     Upload.upload({ 
              url: templateUrl+'_post', 
              data: value
          }).then(function (resp) { 
              $route.reload();
              // $scope.notif = resp.data; 
              datatables.ajax.reload(); 

              toasty.success({
                title: "Input Sukses",
                msg: resp.data
              });

          }, function (resp) { 
           //   console.log('Error status: ' + resp.status); 
          }); 
    }; 

$scope.input_akses = function (argument) {
       Upload.upload({ 
          url: templateUrl+'akses_post',
          data: argument
        }).then(function (response) {
                  $location.path(directURL);
            }, function (response) {
                // $btn.button('reset');
                notifications.showError({
                    message: response.data.messages,
                    hideDelay: 3500, //ms
                    hide: true //bool
                 });
          });
  }
        $scope.absen_masuk = function(id_admin){
            $http({
                method: 'POST',
                url: ipurl+'post/absen_masuk',
                data: {id_admin:id_admin}
            }).then(function successCallback(r){
                $route.reload();
              toasty.success({
                title: "Input Sukses",
                msg: r.data.messages,
              });
            }, function errorCallback(r){
              toasty.error({
                title: "Input Gagal",
                msg: r.data.messages,
              });
                //$scope.notif = r.data.error;
            });
        }

        $scope.absen_keluar = function(id_admin){
            $http({
                method: 'POST',
                url: ipurl+'post/absen_keluar',
                data: {id_admin:id_admin}
            }).then(function successCallback(r){
                $route.reload();
              toasty.success({
                title: "Input Sukses",
                msg: r.data.messages,
              });
            }, function errorCallback(r){
              toasty.error({
                title: "Input Gagal",
                msg: r.data.messages,
              });
                //$scope.notif = r.data.error;
            });
        }

        $scope.cek_absen = function(id_admin){
        $http({
            method : "GET",
            url    : ipurl+"get/cek_absen/"+id_admin
        }).then(function mySucces(response) {
            $scope.status = response.data.status;
        });
        $http({
            method : "GET",
            url    : ipurl+"get/cek_absen_pulang/"+id_admin
        }).then(function mySucces(response) {
            $scope.status_pulang = response.data.status;
        });
        console.log($scope.status);
        console.log($scope.status_pulang);
        }

            $scope.deleteorderotomatis = function(){    
                $('#modalOrderDelete').openModal();   
                $rootScope.deleteorderotomatis_modal = function(){    
                           $http({
                             method: 'DELETE',
                             url: ADMIN_URL+"api/umrah_sistem/deleteorderotomatis_delete/"
                           }).then(function successCallback(r) {
                                 $('#modalOrderDelete').closeModal();
                                  datatables.ajax.reload();
                             }, function errorCallback(r) {
                                  datatables.ajax.reload();
                             });
                }
            }

      $scope.modalAlumni = function (argument) {
        $('#modalAlumni').openModal();
      $rootScope.tambahalumni = function () {
            // console.log(templateUrl+'_import_post');
             $http({
                method : "POST",
                url    : ADMIN_URL+"api/jamaah/data_alumni_post/"
              }).then(function (response) {
                 $route.reload();
                  // $scope.notif = resp.data; 
                  datatables.ajax.reload(); 
                  toasty.success({
                    title: "Input Sukses",
                    msg: response.data.messages
                  });

                }, function (response) {
                   // $btn.button('reset');
                   notifications.showError({
                       message: response.data.messages,
                       hideDelay: 3500, //ms
                       hide: true //bool
                    });
                });
        }

      }


      $scope.modalImport = function (argument) {
        $('#modalImport').openModal();
        $http({
          method: 'GET',
          url: ADMIN_URL+'api/jamaah/get_jadwalimport',
        }).then(function successCallback(response) {
          $rootScope.jadwalimport = response.data;
        }, function errorCallback(response) {
              
        });


      $rootScope.import = function (arg) {
            console.log(arg);
            // console.log(templateUrl+'_import_post');
             Upload.upload({ 
                url: templateUrl+'_import_post',
                data: arg
              }).then(function (response) {
                        console.log(response);
                 $route.reload();
                 toasty.success({
                    title: "Input Sukses",
                    msg: response.data.messages
                  });
                        // $location.path(directURL);
                  }, function (response) {
                      // $btn.button('reset');
                      notifications.showError({
                          message: response.data.messages,
                          hideDelay: 3500, //ms
                          hide: true //bool
                       });
                });
        }

      }

  }]);

AppController.controller('sidebarmenu', ['$scope','$location', function($scope,$location) {
 
$scope.changelink = function(ling){
   var path = $location.path();
   $('li').removeClass('current-page');
  var waedan = $('#sidebar-menu').find('a[name="'+ling+'"]').parent('li').addClass('current-page');
   console.log(waedan);
  
  $location.path(ling);
}

$(function () {
    var URL = window.location,
        $BODY = $('body'),
        $SIDEBAR_MENU = $('#sidebar-menu'),
        $MENU_TOGGLE = $('#menu_toggle');
        $SIDEBAR_FOOTER = $('.sidebar-footer');
        $LEFT_COL = $('.left_col');
    $SIDEBAR_MENU.find('li ul').slideUp();
    $SIDEBAR_MENU.find('li').removeClass('active');

    $SIDEBAR_MENU.find('li').on('click', function(ev) {
        var link = $('a', this).attr('href');


            if ($(this).is('.active')) {
                $(this).removeClass('active');
                $('ul', this).slideUp();
            } else {
                $SIDEBAR_MENU.find('li').removeClass('active');
                $SIDEBAR_MENU.find('li ul').slideUp();
                
                $(this).addClass('active');
                $('ul', this).slideDown();
            }
      
    });

    $MENU_TOGGLE.on('click', function() {
        console.log('wa');
        if ($BODY.hasClass('nav-md')) {
            $BODY.removeClass('nav-md').addClass('nav-sm');
            $LEFT_COL.removeClass('scroll-view').removeAttr('style');
            $SIDEBAR_FOOTER.hide();

            if ($SIDEBAR_MENU.find('li').hasClass('active')) {
                $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
            }
        } else {
            $BODY.removeClass('nav-sm').addClass('nav-md');
            $SIDEBAR_FOOTER.show();

            if ($SIDEBAR_MENU.find('li').hasClass('active-sm')) {
             
                $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
            }
        }
    });

    // check active menu
    $SIDEBAR_MENU.find('a[name="' + URL.pathname + '"]').parent('li').addClass('current-page');

    $SIDEBAR_MENU.find('a').filter(function () {  
    console.log(this.name); 
        return this.name == URL.pathname;
    }).parent('li').addClass('current-page').parent('ul').slideDown().parent().addClass('active');
});
}]);

AppController.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

AppController.directive('myPagination', function() {
  return {
    restrict: 'E',
    scope: {
      data: '=length',
      activemenu: '=active'
    },
    templateUrl: ADMIN_URL+'myPagination.html'

  };
});

AppController.directive('ngdatepicker', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {      
      element.pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 100, // Creates a dropdown of 15 years to control year
        format: 'dd/mm/yyyy'   
      });
    }
  };
});

AppController.directive('ngsidenav', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {      
      element.sideNav({
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true
      });
    }
  };
});

AppController.directive('dropdownangular', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {   
          element.dropdown({
          inDuration: 300,
          outDuration: 225,
          constrain_width: false, // Does not change width of dropdown to that of the activator
          hover: true, // Activate on hover
          gutter: 0, // Spacing from edge
          belowOrigin: true, // Displays dropdown below the button
          alignment: 'left' // Displays dropdown with edge aligned to the left of button
        });
    }
  };
});

AppController.directive('tabsangular', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {   
          element.tabs({});
    }
  };
});

AppController.directive('hcChart', function () {
return {
    restrict: 'E',
    template: '<div></div>',
    replace: true,

    link: function (scope, element, attrs) {

        scope.$watch(function () { return attrs.chart; }, function () {

            if (!attrs.chart) return;

            var charts = JSON.parse(attrs.chart);

            $(element[0]).highcharts(charts);

        });
    }
};
});

AppController.directive('ngmask', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {   
          element.inputmask();
    }
  };
});

AppController.directive('select2', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {   
          element.select2();
    }
  };
});

AppController.controller('RekapController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

        $http({
        method : "GET",
        url    : ADMIN_URL+'api/jamaah/rekap'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });

        $scope.cari = function (argument) {
          if (argument.rekap == "1") {
              $http({
                method : "GET",
                url    : ADMIN_URL+'api/jamaah/rekap_cari?id_jadwal='+argument.id_jadwal
              }).then(function mySucces(response) {
                $scope.rekap_jamaah = response.data;
              });
          }else{

          }
          // $http({
          //   method : "GET",
          //   url    : ADMIN_URL+'api/umrah_sistem/rekap_cari'
          // }).then(function mySucces(response) {
          //   $scope.rekap = response.data;
          // });

        $http.get(ADMIN_URL+'api/jamaah/analitik_rekap?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res);
              // angular.forEach(response.pending, function(value, key) {
              //   this.push(value.tanggal);
              // }, tanggal);

                $scope.chartOptions = {
                  chart: {
                      type: 'pie'
                    },
                    title: {
                        text: 'Analitik Rekap '+res.tanggal
                    },

                    series: [{
                            name: 'Jamaah',
                            colorByPoint: true,
                            data: [{
                                name: 'Zamrud',
                                // color: '#000',
                                y: parseInt(res.zamrud)
                                
                            }, {
                                name: 'Ruby',
                                // color: '#90ed7d',
                                y: parseInt(res.ruby)
                            }, {
                                name: 'Sapphire',
                                // color: '#90ed7d',
                                y: parseInt(res.sapphire)
                            }]
                          }],
                        };
        });


        }


    }]);

AppController.controller('RekapAbsenController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;


        $scope.cari = function (argument) {
              $http({
                method : "GET",
                url    : ADMIN_URL+'api/absen/rekap_cari?tahun='+argument.tahun+'&bulan='+argument.bulan
              }).then(function mySucces(response) {
                $scope.rekapabsen = response.data.result;
              });


              $http({
                method : "GET",
                url    : ADMIN_URL+'api/absen/rekap_analitik?tahun='+argument.tahun+'&bulan='+argument.bulan
              }).then(function mySucces(response) {
                $scope.chartRekapabsenOptions = {
                      title: {
                          text: 'Analitik Rekap Fee Absen'
                      },
                      xAxis: {
                          categories: response.data.rekap_analitik.nama,
                      },
                      series: [{
                          name:'Fee',
                          data: response.data.rekap_analitik.fee
                      }],
                  };
              

              });

        }


    }]);

AppController.controller('NotifController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
   var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

              $http({
                method : "GET",
                url    : ADMIN_URL+'api/home/notif'
              }).then(function mySucces(response) {
                console.log(response.data);
                $scope.ultah        = response.data.alumni;
                $scope.tanggalultah = response.data.tanggalultah;
              });

    }]);