var AppController = angular.module('AppController', []);

var ipurl = "http://api.wahanahajiumrah.com/" ;

AppController.controller('CmsController',['$scope','Upload','$routeParams','$http','$location','notifications','$filter','$rootScope','$route','toasty','$parse',
  function($scope,Upload,$routeParams,$http,$location,notifications,filter,$rootScope,$route,toasty,$parse){
$scope.value = {};

  $rootScope.gantijudul = function (argument) {
  document.title = argument;
  }
  $scope.sortableOptions = {
    placeholder: "item",
    connectWith: ".apps-container",
    items: "> li"
  };

  // $scope.mCustomScrollbar = {
  //   "autoHideScrollbar":true,
  //   theme:"dark"
  // };

      // Validasi form
      // $('form').valida();
      // Menambahkan validasi ke semua inputan
      // $('form input, form select, form textarea').attr('required',true);
          // $(":input").inputmask();
      // $('.content').addClass('mCustomScrollbar');
   
  $rootScope.getClass = function (path) {
    var current = $location.absUrl();
    return (current === path) ? 'sidebar-active' : '';
  }

  $scope.tinymceOptions = {
    onChange: function(e) {
      // put logic here for keypress and cut/paste changes
    },
    inline: false,
    height: 300,
    plugins: [
         "advlist autolink link image lists charmap  preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking fullscreen",
         "table contextmenu directionality emoticons paste textcolor  code"
   ],
    toolbar: 'insertfile undo redo | styleselect fontsizeselect | bold italic | link image | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify',
    skin: 'lightgray',
    theme : 'modern',
    external_filemanager_path:"/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: {"filemanager" : ADMIN_URL+"gudang/js/plugins/responsivefilemanager/plugin.min.js"
    }
  };

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;
 var directURL   = 'page/' + $routeParams.target + "/"+$routeParams.kondisi;
 var cariurl = $location.search();
    if(cariurl.id_page != null){
      var id_page_plus ="?id_page="+cariurl.id_page; 
    }else{
      var id_page_plus =""; 
    }

 $scope.cari = cariurl.cari;

  var datatables = $('#datatables').DataTable({
 
        "processing": true, //Feature control the processing indicator.
        "filter" : false,
        "bLengthChange" : false,
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "ajax": {
            "url": templateUrl+id_page_plus,
            "type": "POST"
        },
        "language": {
          "emptyTable"  : "Data kosong",
          "zeroRecords" : "Tidak ada data yang dapat ditampilkan"
        },
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
            "className": 'mdl-data-table__cell--non-numeric'
        },
        ]
 
    });
  
 $rootScope.caridatatables = function(data, field = "cari"){
   datatables.ajax.url(templateUrl+'?'+field+'='+data).load();
 }
    if($routeParams.id == null){
      if ($routeParams.target  == "home" && $routeParams.kondisi  == "dashboard") {
        $http.get(templateUrl).success(function (response) {

              // angular.forEach(response.pending, function(value, key) {
              //   this.push(value.tanggal);
              // }, tanggal);

                $scope.chartOptions = {
                    title: {
                        text: 'Analitik Pengunjung'
                    },
                    xAxis: {
                        categories: response.anality_artikel.tanggal,
                    },

                    series: [{
                        name:'Artikel',
                        data: response.anality_artikel.artikel.hit
                    },{
                        name:'Catatan Ustad',
                        data: response.anality_artikel.custad.hit
                    },{
                        name:'Tanya Ustad',
                        data: response.anality_artikel.tustad.hit
                    }],
                };

                $scope.chartorderOptions = {
                    title: {
                        text: 'Analitik Order per jadwal keberangkatan'
                    },
                    xAxis: {
                        categories: response.anality_pendaftaran.tanggal,
                    },

                    series: [
                    {
                        name:'Pending',
                        data: response.anality_pendaftaran.pending.hit
                    },
                    {
                        name:'Lunas Pendaftaran',
                        data: response.anality_pendaftaran.lunas_pendaftaran.hit
                    },
                    {
                        name:'Lunas',
                        data: response.anality_pendaftaran.lunas.hit
                    },
                    {
                        name:'Kadaluwarsa',
                        data: response.anality_pendaftaran.kadaluwarsa.hit
                    },
                    ],
                };
                $scope.chartorderdutaOptions = {
                    title: {
                        text: 'Analitik Order per jadwal keberangkatan by Duta'
                    },
                    xAxis: {
                        categories: response.anality_pendaftaran_duta.tanggal,
                    },

                    series: [

                    {
                        name:'Lunas Pendaftaran',
                        data: response.anality_pendaftaran_duta.lunas_pendaftaran.hit
                    },
                    {
                        name:'Lunas',
                        data: response.anality_pendaftaran_duta.lunas.hit
                    },
                    ],
                };
                $scope.chartordercompareOptions = {
                  chart: {
                      type: 'pie'
                    },
                    title: {
                        text: 'Analitik Order'
                    },

                    series: [{
                            name: 'Jamaah',
                            colorByPoint: true,
                            data: [{
                                name: 'Pendaftaran Melalui Duta',
                                // color: '#000',
                                y: parseInt(response.anality_compare_order.duta)
                                
                            }, {
                                name: 'Pendaftaran Langsung',
                                // color: '#90ed7d',
                                y: parseInt(response.anality_compare_order.langsung)
                            }]
                          }],
                        };

        console.log("home")

        });

      }else if($routeParams.target  == "jamaah" && $routeParams.kondisi  == "rekap"){
        console.log("ini jamaah")
      }
    }else{
      $http({
          method: 'GET',
          url: templateUrl+'/'+$routeParams.id
        }).then(function successCallback(response) {
            $scope.value = response.data;
          }, function errorCallback(response) {
            
          });
    }



  $scope.load_tambah = function(i){
    if (i == "active") {
      var $plusID = "?id="+$routeParams.id;
    }else{
      var $plusID = "";
    }
    $http({
          method: 'GET',
          url: templateUrl+'_tambahan'+$plusID,
          cache: false
        }).then(function successCallback(response) {
            $scope.ii = response.data;

            $scope.deletegambarmodal = function(index,id_delete){    
                $('#modalGambarDelete').openModal();   
                $rootScope.deletegambar = function(){    
                           $http({
                             method: 'DELETE',
                             url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_gambar_delete/'+id_delete
                           }).then(function successCallback(r) {
                                 $('#modalGambarDelete').closeModal();
                                 $('#modalSub').closeModal();
                                  $scope.ii.gambar.splice(index, 1);
                             }, function errorCallback(r) {
                                  datatables.ajax.reload();
                             });
                }
            }

          }, function errorCallback(response) {
            
          });
  }


        //  $( document ).on( "click", "#gambarDelete", function() {
        //      $('#myModal').modal('show');    
        //      var id_delete = $(this).attr('id-delete');
        //             $rootScope.deletedatatables = function(){             
        //               $http({
        //                 method: 'DELETE',
        //                 url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_gambar_delete/'+id_delete
        //               }).then(function successCallback(response) {
        //                     
        //                      $scope.loop_harga_rate.result.splice(index, 1);
        //                 }, function errorCallback(response) {
        //                      datatables.ajax.reload();
        //                 });
        //              }
        // });
  $scope.load_addgallery = function (argument) {
      $http({
          method: 'GET',
          url: templateUrl+'/'+cariurl.id_page
        }).then(function successCallback(response) {
            $scope.value = response.data;
            console.log(response.data);
          }, function errorCallback(response) {
            
          });
  }
    
    $('#datatables tbody').on('click', '#DomListSub', function () {
     $('#modalSub').openModal();    
             var id_menu_admin = $(this).attr('id-menu-admin');
                      $http({
                        method: 'get',
                        url: ADMIN_URL+'api/master/sub_menu_admin?id='+id_menu_admin
                      }).then(function successCallback(response) {
                          $rootScope.sub = response.data;
                             // datatables.ajax.reload();
                        }, function errorCallback(response) {
                             // datatables.ajax.reload();
                        });
    });

    $('#datatables tbody').on('click', '#DomListJadwal', function () {
     $('#modalSubJadwal').openModal();
             var id_paket = $(this).attr('id-paket');
             var nama_paket = $(this).attr('nama-paket');
                      $http({
                        method: 'get',
                        url: ADMIN_URL+'api/umrah_sistem/jadwal_by_paket?id='+id_paket
                      }).then(function successCallback(response) {
                          $rootScope.jadwal_by_paket = response.data;
                          $rootScope.nama_paket = nama_paket;
                             // datatables.ajax.reload();
                        }, function errorCallback(response) {
                             // datatables.ajax.reload();
                        });
    

    });



  $scope.save = function (argument) {
     tinyMCE.triggerSave();
      // var $btn = $("#myButton").button('loading');
       Upload.upload({ 
          url: templateUrl+'_post',
          data: argument
        }).then(function (response) {
/*
            notifications.showSuccess({
                message: response.data.messages,
                hideDelay: 3500, //ms
                hide: true //bool
             });*/

              toasty.success({
                title: "Input Sukses",
                msg: response.data.messages,
              });

                // $btn.button('reset');
                if(response.data.url != undefined){
                  console.log('ada url');
                   window.location.href = response.data.url;
                }else{
                   $location.path(directURL);
                }
            }, function (response) {
              toasty.error({
                title: "Input Gagal",
                msg: response.data.messages,
              });
            /*
                // $btn.button('reset');
                notifications.showError({
                    message: response.data.messages,
                    hideDelay: 3500, //ms
                    hide: true //bool
                 });*/
          });
        }

      $scope.DomStatus = function (id,status) {

             // var id = $(this).attr('id-status');
             // var status = $(this).attr('status');
                      $http({
                        method: 'post',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&status='+status
                      }).then(function successCallback(response) {
                              toasty.info({
                                title: "Update Sukses",
                                msg: response.data.messages,
                              });
                              $('#modalSubJadwal').closeModal();
                             datatables.ajax.reload();
                              // $('#modalSubJadwal').openModal();

                        }, function errorCallback(response) {
                             datatables.ajax.reload();
                        });
       }


       $('#datatables tbody').on( "click", "#DomStatus", function() {
             var id = $(this).attr('id-status');
             var status = $(this).attr('status');
                      $http({
                        method: 'post',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&status='+status
                      }).then(function successCallback(response) {
                              toasty.info({
                                title: "Update Sukses",
                                msg: response.data.messages,
                              });
                             datatables.ajax.reload();
                        }, function errorCallback(response) {
                             datatables.ajax.reload();
                        });
        });
       $('#datatables tbody').on( "click", "#DomBatal", function() {
            $('#modalBatal').openModal();
             var id = $(this).attr('id-status');
             var id_order_detail = $(this).attr('id-order');
             $rootScope.ubahstatusbatal = function(){     
                      $http({
                        method: 'post',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&id_order_detail='+id_order_detail
                      }).then(function successCallback(response) {
                              toasty.info({
                                title: "Update Sukses",
                                msg: response.data.messages,
                              });
                             datatables.ajax.reload();
                        }, function errorCallback(response) {
                             datatables.ajax.reload();
                        });
            }
        });
       $('#datatables tbody').on( "click", "#DomDelete", function() {
             $('#modalDelete').openModal();
             var id_delete = $(this).attr('id-delete');
                    $rootScope.deletedatatables = function(){             
                      $http({
                        method: 'DELETE',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_delete
                      }).then(function successCallback(response) {
                              $('#modalSub').closeModal();
                              $('#modalSubJadwal').closeModal();

                              toasty.info({
                                title: "Delete Sukses",
                                msg: response.data.messages,
                              });

                             datatables.ajax.reload();
                        }, function errorCallback(response) {
                             datatables.ajax.reload();
                        });
                     }
        });


  $scope.DomDelete = function(id_delete) {
    $('#modalDelete').openModal();
      $rootScope.deletedatatables = function() {
       $http({
         method: 'DELETE',
         url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_delete
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Delete Sukses",
                 msg: response.data.messages,
               });
                              $('#modalSubJadwal').closeModal();
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
     }
  };

       $('tbody').on( "click", "#HariDelete", function() {
             $('#modalDelete').openModal();
             var id_delete = $(this).attr('id-delete');
                    $rootScope.deletedatatables = function(){             
                      $http({
                        method: 'DELETE',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_delete
                      }).then(function successCallback(response) {
                              $('#modalSub').closeModal();

                              toasty.info({
                                title: "Delete Sukses",
                                msg: response.data.messages,
                              });
                             $route.reload();
                        }, function errorCallback(response) {
                             $route.reload();
                        });
                     }
        });


       $('tbody').on( "click", "#DetailDelete", function() {
        console.log("fuckinglol");
             $('#modalDelete').openModal();
             var id_delete = $(this).attr('id-delete');
                    $rootScope.deletedatatables = function(){             
                      $http({
                        method: 'DELETE',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_delete
                      }).then(function successCallback(response) {
                              $('#modalSub').closeModal();

                              toasty.info({
                                title: "Delete Sukses",
                                msg: response.data.messages,
                              });
                             $route.reload();
                        }, function errorCallback(response) {
                             $route.reload();
                        });
                     }
        });

       $('tbody').on( "click", "#DeleteKeberangkatan", function() {
        console.log("fuckinglol");
             $('#modalDelete').openModal();
             var id_delete = $(this).attr('id-delete');
             var id_jamaah = $(this).attr('id-jamaah');
                    $rootScope.deletedatatables = function(){             
                      $http({
                        method: 'DELETE',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/keberangkatan_deletnya_delete/'+id_delete+'?id_jamaah='+id_jamaah
                      }).then(function successCallback(response) {
                              $('#modalSub').closeModal();

                              toasty.info({
                                title: "Delete Sukses",
                                msg: response.data.messages,
                              });
                             $route.reload();
                        }, function errorCallback(response) {
                             $route.reload();
                        });
                     }

        });
                  


       $('tbody').on( "click", "#HariDelete", function() {
             $('#modalDelete').openModal();
             var id_delete = $(this).attr('id-delete');
                    $rootScope.deletedatatables = function(){             
                      $http({
                        method: 'DELETE',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_delete
                      }).then(function successCallback(response) {
                              $('#modalSub').closeModal();

                              toasty.info({
                                title: "Delete Sukses",
                                msg: response.data.messages,
                              });
                             $route.reload();
                        }, function errorCallback(response) {
                             $route.reload();
                        });
                     }
        });

$scope.gantistatus = function(id,status) {
            $http({
                    method: 'post',
                    url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&status='+status
                  }).then(function successCallback(response) {
                          toasty.info({
                            title: "Update Sukses",
                            msg: response.data.messages,
                          });
                          $route.reload();
                        }, function errorCallback(response) {
                        });
}

  $scope.save_dokumen = function (argument,namafield) {
     tinyMCE.triggerSave();
      // var $btn = $("#myButton").button('loading');
       Upload.upload({ 
          url: templateUrl+'_dokumen_post?field='+namafield,
          data: argument
        }).then(function (response) {
              toasty.success({
                title: "Input Sukses",
                msg: response.data.messages,
              });

                if(response.data.url != undefined){
                  console.log('ada url');
                   // window.location.href = response.data.url;
                }else{
                   // $location.path(directURL);
                }
            }, function (response) {
              toasty.error({
                title: "Input Gagal",
                msg: response.data.messages,
              });
          });
        }      

  $scope.save_cicilan = function (argument) {
     tinyMCE.triggerSave();
      // var $btn = $("#myButton").button('loading');
       Upload.upload({ 
          url: templateUrl+'_cicilan_post',
          data: argument
        }).then(function (response) {
              toasty.success({
                title: "Input Sukses",
                msg: response.data.messages,
              });

                if(response.data.url != undefined){
                  console.log('ada url');
                   // window.location.href = response.data.url;
                  $route.reload();
                }else{
                   $location.path(directURL);
                }
            }, function (response) {
              toasty.error({
                title: "Input Gagal",
                msg: response.data.messages,
              });
          });
        }     

  $scope.save_keberangkatan = function (argument) {
     tinyMCE.triggerSave();
      // var $btn = $("#myButton").button('loading');
       Upload.upload({ 
          url: templateUrl+'_keberangkatan_post',
          data: argument
        }).then(function (response) {
              toasty.success({
                title: "Input Sukses",
                msg: response.data.messages,
              });

                if(response.data.url != undefined){
                  console.log('ada url');
                   // window.location.href = response.data.url;
                  $route.reload();
                }else{
                   $location.path(directURL);
                }
            }, function (response) {
              toasty.error({
                title: "Input Gagal",
                msg: response.data.messages,
              });
          });
        }    

  $scope.save_kwitansi = function (argument) {
     tinyMCE.triggerSave();
      // var $btn = $("#myButton").button('loading');
       Upload.upload({ 
          url: templateUrl+'_kwitansi_post',
          data: argument
        }).then(function (response) {
              toasty.success({
                title: "Input Sukses",
                msg: response.data.messages,
              });

                if(response.data.url != undefined){
                  console.log('ada url');
                   // window.location.href = response.data.url;
                  $route.reload();
                }else{
                   $location.path(directURL);
                }
            }, function (response) {
              toasty.error({
                title: "Input Gagal",
                msg: response.data.messages,
              });
          });
        }      


$scope.closemodal = function() {
    $('#modalSub').closeModal();
    $('#modalSubJadwal').closeModal();
}
$scope.refresh = function() {
  // console.log('fuck');
  $route.reload();
}
$scope.pilih_sesuatu = function(judul,senddata,value){
  $rootScope.judul_modal_transportasi = judul;    
                     $http({
                        method: 'POST',
                        url: ADMIN_URL+'api/' + $routeParams.target+'/'+senddata
                      }).then(function successCallback(response) {
                          $rootScope.datapaketselect = response.data;
                                      $rootScope.pilihfitur = function(id,$type){
                                       /*   oke = {
                                            $type : id
                                          };*/   $http({
                                                    method: 'GET',
                                                    url: ADMIN_URL+'api/' + $routeParams.target+'/'+senddata+'_hasil?id='+id
                                                  }).then(function successCallback(response) {       
                                                        var key = "id_"+$type;
                                                        var oke = {};
                                                        oke[key] = id;
                                                        angular.extend(value,oke);
                                                          toasty.info({
                                                            title: "Pilih "+$type,
                                                            msg: "Berhasil Memilih "+ $type,
                                                          });

                                                         var vardinamis = "ii."+$type;
                                                         var vardinamis = $parse(vardinamis);
                                                         vardinamis.assign($scope, response.data); 

                                                       // console.log($scope.hotel_madinah); 
                                                    $('#modalpaketselect').closeModal();
                                                    }, function errorCallback(response) {
                                                          toasty.info({
                                                            title: "Pilih "+$type,
                                                            msg: "Gagal Memilih "+ $type+", Silakan coba lagi",
                                                          });
                                                    $('#modalpaketselect').closeModal();
                                                    });
                                      }
                   }, function errorCallback(response) {

                        });
 $('#modalpaketselect').openModal();
}

  $scope.ustadModal = function(id_delete) {
    $('#ustadModal').openModal();
      $rootScope.ustaddelete = function() {
       $http({
         method: 'DELETE',
         url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_delete
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Delete Sukses",
                 msg: response.data.messages,
               });
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
     }
  };




  $scope.editpertanyaan = function(id) {
    $http({
           method: 'get',
           url: ADMIN_URL+'api/halaman_depan/tanya_ustad/'+id
         }).then(function successCallback(response) {
             $rootScope.pertanyaan = response.data;
             $('#komenModal').openModal();
           });
      $rootScope.komen = function(argument) {
       $http({
         method: 'post',
         url: templateUrl+'_komen_post',
         data: argument
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Update Sukses",
                 msg: response.data.messages,
               });
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
     }
  };

  $scope.editkomen = function(id) {
    $http({
           method: 'get',
           url: ADMIN_URL+'api/halaman_depan/tanya_ustad/'+id
         }).then(function successCallback(response) {
             $rootScope.pertanyaan = response.data;
             $('#komenModal').openModal();
           });
      $rootScope.komen = function(argument) {
       $http({
         method: 'post',
         url: templateUrl+'_komen_post',
         data: argument
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Update Sukses",
                 msg: response.data.messages,
               });
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
     }
  };
  $scope.gantistatus = function(id,status) {
       $http({
         method: 'post',
         url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_status?id='+id+'&status='+status
       }).then(function successCallback(response) {
               toasty.info({
                 title: "Update Sukses",
                 msg: response.data.messages,
               });
              $route.reload();
         }, function errorCallback(response) {
              $route.reload();
         });
  };
  $scope.status = function(index,id,status) {
            console.log(index);
            $http({
              method: 'POST',
              url: templateUrl+'_status?id='+id+'&status='+status,
            }).then(function successCallback(response) {
                
              $scope.status = response.data.status;

              notifications.showSuccess({
                message: response.data.messages,
                hideDelay: 3500, //ms
                hide: true //bool
             });
              }, function errorCallback(response) {

              });
  };
  $scope.pilih_hotel_tambahan = function (argument) {
      $http({
          method: 'GET',
          url: ADMIN_URL+'api/paket/hotel_tambahan?id='+argument,
        }).then(function successCallback(response) {
            $scope.loop_hotel_tambahan = response.data;
        }, function errorCallback(response) {
              
        });
  }
  $scope.pilihpaket = function (argument) {
      $http({
          method: 'GET',
          url: ADMIN_URL+'api/system/get_jadwal?id='+argument,
        }).then(function successCallback(response) {
            $scope.loop_jadwal = response.data;
        }, function errorCallback(response) {
              
        });
  }
  // $scope.pilihprovinsi = function (argument) {
  //     $http({
  //         method: 'GET',
  //         url: ADMIN_URL+'api/system/get_kabupaten?id='+argument,
  //       }).then(function successCallback(response) {
  //           $scope.loop_kabupaten = response.data;
  //       }, function errorCallback(response) {
              
  //       });
  // }

  $scope.pilihjadwaljamaah = function (argument) {
      $http({
          method: 'GET',
          url: ADMIN_URL+'api/pembayaran/get_jadwal?id='+argument,
        }).then(function successCallback(response) {
            $scope.loop_jadwalkeberangkatan = response.data;
        }, function errorCallback(response) {

 $scope.pilih_nama_penerima = function (argument) {
         console.log("LOL");
   $http({
       method: 'GET',
       url: ADMIN_URL+'api/surat/get_jamaah?id_jamaah='+argument,
     }).then(function successCallback(response) {
        if (response.data.nomor_kartu_identitas == null || response.data.nomor_kartu_identitas == "null" || response.data.nomor_kartu_identitas == "" ) {
          $rootScope.nik_penerima = "-";
        }else{
          $rootScope.nik_penerima = response.data.nomor_kartu_identitas;
        }
        $rootScope.tempat_lahir_penerima = response.data.tempat_lahir;
        $rootScope.tanggal_lahir_penerima = response.data.tanggal_lahir;
        $rootScope.alamat_penerima = response.data.alamat_rumah;
     }, function errorCallback(response) {
     });
 }

  // $scope.pilihpaketkeberangkatan = function (argument) {
  //     $http({
  //         method: 'GET',
  //         url: ADMIN_URL+'api/jamaah/get_jadwal?id='+argument,
  //       }).then(function successCallback(response) {
  //           $scope.loop_jadwal_keberangkatan = response.data;
  //       }, function errorCallback(response) {
              
        });
  }
  $scope.uploadalbum = function(value) {
    console.log(value);
     $scope.notif = [];
     Upload.upload({ 
              url: templateUrl+'_post', 
              data: { 
                      gambar    : $scope.files, 
                      id_album  : value.id_album, 
                      tanggal   : value.tanggal, 
                      judul     : value.judul 
                    } 
          }).then(function (resp) { 
              $route.reload();
              $scope.notif = resp.data; 
              datatables.ajax.reload(); 

              toasty.success({
                title: "Input Sukses",
                msg: resp.data
              });

          }, function (resp) { 
           //   console.log('Error status: ' + resp.status); 
          }); 
    };  
  $scope.uploadvideo = function(value) {
    console.log(value);
     $scope.notif = [];
     Upload.upload({ 
              url: templateUrl+'_post', 
              data: value
          }).then(function (resp) { 
              $route.reload();
              // $scope.notif = resp.data; 
              datatables.ajax.reload(); 

              toasty.success({
                title: "Input Sukses",
                msg: resp.data
              });

          }, function (resp) { 
           //   console.log('Error status: ' + resp.status); 
          }); 
    }; 

$scope.input_akses = function (argument) {
       Upload.upload({ 
          url: templateUrl+'akses_post',
          data: argument
        }).then(function (response) {
                  $location.path(directURL);
            }, function (response) {
                // $btn.button('reset');
                notifications.showError({
                    message: response.data.messages,
                    hideDelay: 3500, //ms
                    hide: true //bool
                 });
          });
  }
        $scope.absen_masuk = function(id_admin){
            $http({
                method: 'POST',
                url: ipurl+'post/absen_masuk',
                data: {id_admin:id_admin}
            }).then(function successCallback(r){
                $route.reload();
              toasty.success({
                title: "Input Sukses",
                msg: r.data.messages,
              });
            }, function errorCallback(r){
              toasty.error({
                title: "Input Gagal",
                msg: r.data.messages,
              });
                //$scope.notif = r.data.error;
            });
        }

        $scope.absen_keluar = function(id_admin){
            $http({
                method: 'POST',
                url: ipurl+'post/absen_keluar',
                data: {id_admin:id_admin}
            }).then(function successCallback(r){
                $route.reload();
              toasty.success({
                title: "Input Sukses",
                msg: r.data.messages,
              });
            }, function errorCallback(r){
              toasty.error({
                title: "Input Gagal",
                msg: r.data.messages,
              });
                //$scope.notif = r.data.error;
            });
        }

        $scope.cek_absen = function(id_admin){
        $http({
            method : "GET",
            url    : ipurl+"get/cek_absen/"+id_admin
        }).then(function mySucces(response) {
            $scope.status = response.data.status;
        });
        $http({
            method : "GET",
            url    : ipurl+"get/cek_absen_pulang/"+id_admin
        }).then(function mySucces(response) {
            $scope.status_pulang = response.data.status;
        });
        console.log($scope.status);
        console.log($scope.status_pulang);
        }

            $scope.deleteorderotomatis = function(){    
                $('#modalOrderDelete').openModal();   
                $rootScope.deleteorderotomatis_modal = function(){    
                           $http({
                             method: 'DELETE',
                             url: ADMIN_URL+"api/umrah_sistem/deleteorderotomatis_delete/"
                           }).then(function successCallback(r) {
                                 $('#modalOrderDelete').closeModal();
                                  datatables.ajax.reload();
                             }, function errorCallback(r) {
                                  datatables.ajax.reload();
                             });
                }
            }

      // $scope.modalAlumni = function (argument) {
      // $('#modalAlumni').openModal();
      // $rootScope.tambahalumni = function () {
      //       // console.log(templateUrl+'_import_post');
      //        $http({
      //           method : "POST",
      //           url    : ADMIN_URL+"api/jamaah/data_alumni_post/"
      //         }).then(function (response) {
      //            $route.reload();
      //             // $scope.notif = resp.data; 
      //             datatables.ajax.reload(); 
      //             toasty.success({
      //               title: "Input Sukses",
      //               msg: response.data.messages
      //             });

      //           }, function (response) {
      //              // $btn.button('reset');
      //              notifications.showError({
      //                  message: response.data.messages,
      //                  hideDelay: 3500, //ms
      //                  hide: true //bool
      //               });
      //           });
      //     }
      //   }

      $scope.number = 5;
      $scope.getNumber = function(num) {
        // console.log(num)

        $scope.loopjamaah =  new Array(parseInt(num));   
      }


      // $scope.tambahJamaah = function (argument) {

      // var jumlah = 2;
      // $rootScope.tesaja = new Array(2);
        // console.log('fuck');
      // $('#modaltambahJamaah').openModal();
      // $rootScope.tambahjamaahdaftar = function () {
      //        $http({
      //           method : "POST",
      //           url    : ADMIN_URL+"api/system/ubah_alumni_post/"
      //         }).then(function (response) {
      //            $route.reload();
      //             datatables.ajax.reload(); 
      //             toasty.success({
      //               title: "Input Sukses",
      //               msg: response.data.messages
      //             });

      //           }, function (response) {
      //              notifications.showError({
      //                  message: response.data.messages,
      //                  hideDelay: 3500, //ms
      //                  hide: true //bool
      //               });
      //           });
      //     }
        // }



      $scope.ubahAlumni = function (argument) {
      $('#modalUbahAlumni').openModal();
      $rootScope.rubahalumni = function () {
            // console.log(templateUrl+'_import_post');
             $http({
                method : "POST",
                url    : ADMIN_URL+"api/system/ubah_alumni_post/"
              }).then(function (response) {
                 $route.reload();
                  // $scope.notif = resp.data; 
                  datatables.ajax.reload(); 
                  toasty.success({
                    title: "Input Sukses",
                    msg: response.data.messages
                  });

                }, function (response) {
                   // $btn.button('reset');
                   notifications.showError({
                       message: response.data.messages,
                       hideDelay: 3500, //ms
                       hide: true //bool
                    });
                });
          }
        }

      $scope.modalRekap = function (id_cicilan) {
        console.log(id_cicilan);
        $('#modalRekapfee').openModal();

        $rootScope.rekapfeeDelete = function() {
         $http({
           method: 'DELETE',
           url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_delete/'+id_cicilan
         }).then(function successCallback(response) {
                 toasty.info({
                   title: "Delete Sukses",
                   msg: response.data.messages,
                 });
                $route.reload();
           }, function errorCallback(response) {
                $route.reload();
           });
          }

      }

      $scope.cekkonfirmasi = function(idorder){    
        $http({
            method : "GET",
            url    : ADMIN_URL+'api/' + $routeParams.target+'/cek_order?id_order='+idorder
        }).then(function mySucces(response) {
            // $scope.status = response.data.status;
        });
      }



      $scope.konfirmasiordermodal = function(idorder,idduta){    
          $('#modalKonfirmasiOrder').openModal();   
          $rootScope.konfirmasiOrder = function(){    
                     $http({
                       method: 'POST',
                       url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_konfirmasi?id='+idorder+'&id_duta='+idduta
                     }).then(function successCallback(r) {
                           $('#modalKonfirmasiOrder').closeModal();
                          $route.reload();
                       }, function errorCallback(r) {
                           $('#modalKonfirmasiOrder').closeModal();
                           $route.reload();
                       });
          }
      }



      $scope.pencarianLanjutan = function (id) {
        $('#'+id).openModal();
      }

      $scope.pencarianLanjutan_jadwalpaket = function (id) {
        $('#cari_jadwalpaket').openModal();
        $http({
            method : "GET",
            url    : ADMIN_URL+'api/umrah_sistem/get_type',
        }).then(function mySucces(response) {
            $rootScope.loop_type = response.data;
        });

        $rootScope.cari_jadwalpaket = function(val) {
          datatables.ajax.url(templateUrl+'?type='+val).load();
        }
      }

      $scope.pencarianLanjutan_artikel = function (id) {
        $('#cari_artikel').openModal();
        $http({
            method : "GET",
            url    : ADMIN_URL+'api/halaman_depan/get_kategori',
        }).then(function mySucces(response) {
            $rootScope.loop_kategori = response.data;
        });

        $rootScope.cari_kategori = function(val) {
          datatables.ajax.url(templateUrl+'?id_kategori='+val).load();
        }
      }

      $scope.pencarianLanjutan_jamaah = function (id) {
        $('#cari_jamaah').openModal();
        $http({
            method : "GET",
            url    : ADMIN_URL+'api/jamaah/get_cari',
        }).then(function mySucces(response) {
            // console.log(response.data.jadwal);
            // console.log(response.data.paket);
            $rootScope.loop_jadwal = response.data.jadwal;
            $rootScope.loop_paket = response.data.paket;
        });

        $rootScope.cari_jamaah = function(val) {
          console.log(val.jadwal);
          if(val.jadwal != null){
            datatables.ajax.url(templateUrl+'?id_jadwal='+val.jadwal).load();
          }else if(val.paket != null){
            datatables.ajax.url(templateUrl+'?nama_paket='+val.paket).load();
          }else if(val.paket != null && val.jadwal != null){
            datatables.ajax.url(templateUrl+'?nama_paket='+val.paket+'&id_jadwal='+val.id_jadwal).load();
          }
        }
      }

      $scope.pencarianLanjutan_jamaahv2 = function (id) {
        $('#cari_jamaahv2').openModal();
        $http({
            method : "GET",
            url    : ADMIN_URL+'api/system/get_cari',
        }).then(function mySucces(response) {
            // console.log(response.data.jadwal);
            // console.log(response.data.paket);
            $rootScope.loop_jadwal = response.data.jadwal;
            $rootScope.loop_paket = response.data.paket;
        });

        $rootScope.cari_jamaahv2 = function(val) {
          console.log(val.jadwal);
          if(val.jadwal != null){
            datatables.ajax.url(templateUrl+'?tanggal='+val.jadwal).load();
          }else if(val.paket != null){
            datatables.ajax.url(templateUrl+'?nama_paket='+val.paket).load();
          }else if(val.paket != null && val.jadwal != null){
            datatables.ajax.url(templateUrl+'?nama_paket='+val.paket+'&tanggal='+val.id_jadwal).load();
          }
        }
      }

      $scope.pencarianLanjutan_alumniv2 = function (id) {
        $('#cari_alumniv2').openModal();
        $http({
            method : "GET",
            url    : ADMIN_URL+'api/alumni_v2/get_cari',
        }).then(function mySucces(response) {
            // console.log(response.data.jadwal);
            // console.log(response.data.paket);
            $rootScope.loop_jadwal = response.data.jadwal;
            $rootScope.loop_paket = response.data.paket;
        });

        $rootScope.cari_alumniv2 = function(val) {
          console.log(val.jadwal);
          if(val.jadwal != null){
            datatables.ajax.url(templateUrl+'?tanggal='+val.jadwal).load();
          }else if(val.paket != null){
            datatables.ajax.url(templateUrl+'?nama_paket='+val.paket).load();
          }else if(val.paket != null && val.jadwal != null){
            datatables.ajax.url(templateUrl+'?nama_paket='+val.paket+'&tanggal='+val.id_jadwal).load();
          }
        }
      }
      
      $scope.modalImport = function (argument) {
        $('#modalImport').openModal();
        $http({
          method: 'GET',
          url: ADMIN_URL+'api/jamaah/get_jadwalimport',
        }).then(function successCallback(response) {
          $rootScope.jadwalimport = response.data;
        }, function errorCallback(response) {
              
        });


      $rootScope.import = function (arg) {
            console.log(arg);
            // console.log(templateUrl+'_import_post');
             Upload.upload({ 
                url: templateUrl+'_import_post',
                data: arg
              }).then(function (response) {
                        console.log(response);
                 $route.reload();
                 toasty.success({
                    title: "Input Sukses",
                    msg: response.data.messages
                  });
                        // $location.path(directURL);
                  }, function (response) {
                      // $btn.button('reset');
                      notifications.showError({
                          message: response.data.messages,
                          hideDelay: 3500, //ms
                          hide: true //bool
                       });
                });
        }
      }

      $scope.cari_alamat = function (argument) {
        console.log(argument.alamat);
        if (argument.alamat == "" || argument.alamat == undefined) {
            $scope.data =  null;
        }else{

        $http({
          method: 'GET',
          url: ADMIN_URL+'api/pencarian/alamat?alamat='+argument.alamat,
        }).then(function successCallback(response) {
            $scope.data =  response.data;
        }, function errorCallback(response) {
              
        });
        
        }

      };

      $scope.cari_tanggal = function (argument) {
        if (argument.bulan == "" || argument.bulan == undefined) {
            $scope.data =  null;
        }else{
            $http({
              method: 'GET',
              url: ADMIN_URL+'api/pencarian/tanggal?bulan='+argument.bulan,
            }).then(function successCallback(response) {
                $scope.data =  response.data;
            }, function errorCallback(response) {
                  
            });
        }
      
      };

      $scope.cari_passport = function (argument) {
            $http({
              method: 'GET',
              url: ADMIN_URL+'api/pencarian/passport?bulan='+argument.bulan+'&tahun='+argument.tahun,
            }).then(function successCallback(response) {
                $scope.data =  response.data;
            }, function errorCallback(response) {
                  
            });
      
      };

      $scope.getJamaahperKamar = function (argument) {
            // $http({
            //   method: 'GET',
            //   url: ADMIN_URL+'api/lainnya/room_jamaah?id=998',
            // }).then(function successCallback(response) {
            //     // return response.data;
            // }, function errorCallback(response) {
                  
            // });
      }
      // $scope.getNumber = function(num) {
      //     var data = [];
      //     var iii = parseInt(num);
      //     for (var i = iii - 1; i >= 0; i--) {
      //       data.push(i);
      //     }
      //     return data;   
      // }


      // $scope.divtoimage = function (argument) {
      //   // console.log("TEST123");

      //   var svgElements = $("#canvasnya").find('svg');
      
      //   //replace all svgs with a temp canvas
      //   svgElements.each(function() {
      //     var canvas, xml;
      
      //     // canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
      //     $.each($(this).find('[style*=em]'), function(index, el) {
      //       $(this).css('font-size', getStyle(el, 'font-size'));
      //     });
      
      //     canvas = document.createElement("canvas");
      //     canvas.className = "screenShotTempCanvas";
      //     //convert SVG into a XML string
      //     xml = (new XMLSerializer()).serializeToString(this);
      
      //     // Removing the name space as IE throws an error
      //     xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');
      
      //     //draw the SVG onto a canvas
      //     canvg(canvas, xml);
      //     $(canvas).insertAfter(this);
      //     //hide the SVG element

            
      //     $(this).attr('class', 'tempHide');
      //     $(this).hide();
      //   });
      //   $(".content").animate({zoom: '50%'}, "slow");
      //   html2canvas($("#canvasnya"), {
      //     onrendered: function(canvas) {
      //       var imgsrc = canvas.toDataURL("image/png");
      //       console.log(imgsrc);
      //       $('<img src="' + imgsrc + '" />').appendTo('#canvasnya');
      //     }
      //   });


      // };





  }]);

AppController.controller('sidebarmenu', ['$scope','$location', function($scope,$location) {
 
$scope.changelink = function(ling){
   var path = $location.path();
   $('li').removeClass('current-page');
  var waedan = $('#sidebar-menu').find('a[name="'+ling+'"]').parent('li').addClass('current-page');
   console.log(waedan);
  
  $location.path(ling);
}

$(function () {
    var URL = window.location,
        $BODY = $('body'),
        $SIDEBAR_MENU = $('#sidebar-menu'),
        $MENU_TOGGLE = $('#menu_toggle');
        $SIDEBAR_FOOTER = $('.sidebar-footer');
        $LEFT_COL = $('.left_col');
    $SIDEBAR_MENU.find('li ul').slideUp();
    $SIDEBAR_MENU.find('li').removeClass('active');

    $SIDEBAR_MENU.find('li').on('click', function(ev) {
        var link = $('a', this).attr('href');


            if ($(this).is('.active')) {
                $(this).removeClass('active');
                $('ul', this).slideUp();
            } else {
                $SIDEBAR_MENU.find('li').removeClass('active');
                $SIDEBAR_MENU.find('li ul').slideUp();
                
                $(this).addClass('active');
                $('ul', this).slideDown();
            }
      
    });

    $MENU_TOGGLE.on('click', function() {
        console.log('wa');
        if ($BODY.hasClass('nav-md')) {
            $BODY.removeClass('nav-md').addClass('nav-sm');
            $LEFT_COL.removeClass('scroll-view').removeAttr('style');
            $SIDEBAR_FOOTER.hide();

            if ($SIDEBAR_MENU.find('li').hasClass('active')) {
                $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
            }
        } else {
            $BODY.removeClass('nav-sm').addClass('nav-md');
            $SIDEBAR_FOOTER.show();

            if ($SIDEBAR_MENU.find('li').hasClass('active-sm')) {
             
                $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
            }
        }
    });

    // check active menu
    $SIDEBAR_MENU.find('a[name="' + URL.pathname + '"]').parent('li').addClass('current-page');

    $SIDEBAR_MENU.find('a').filter(function () {  
    console.log(this.name); 
        return this.name == URL.pathname;
    }).parent('li').addClass('current-page').parent('ul').slideDown().parent().addClass('active');
});
}]);

AppController.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

AppController.directive('myPagination', function() {
  return {
    restrict: 'E',
    scope: {
      data: '=length',
      activemenu: '=active'
    },
    templateUrl: ADMIN_URL+'myPagination.html'

  };
});

AppController.directive('ngdatepicker', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {      
      element.pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 100, // Creates a dropdown of 15 years to control year
        format: 'dd/mm/yyyy'   
      });
    }
  };
});

AppController.directive('ngsidenav', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {      
      element.sideNav({
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true
      });
    }
  };
});

AppController.directive('dropdownangular', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {   
          element.dropdown({
          inDuration: 300,
          outDuration: 225,
          constrain_width: false, // Does not change width of dropdown to that of the activator
          hover: true, // Activate on hover
          gutter: 0, // Spacing from edge
          belowOrigin: true, // Displays dropdown below the button
          alignment: 'left' // Displays dropdown with edge aligned to the left of button
        });
    }
  };
});

AppController.directive('tabsangular', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {   
          element.tabs({});
    }
  };
});

AppController.directive('hcChart', function () {
return {
    restrict: 'E',
    template: '<div></div>',
    replace: true,

    link: function (scope, element, attrs) {

        scope.$watch(function () { return attrs.chart; }, function () {

            if (!attrs.chart) return;

            var charts = JSON.parse(attrs.chart);

            $(element[0]).highcharts(charts);

        });
    }
};
});

AppController.directive('ngmask', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {   
          element.inputmask();
    }
  };
});

AppController.directive('select2', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {   
          element.select2();
    }
  };
});

AppController.controller('RekapController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

        $http({
        method : "GET",
        url    : ADMIN_URL+'api/jamaah/rekap'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });

        $scope.rekap_jamaah     = '';
        $scope.rekap_umur       = '';
        $scope.rekap_kamar      = '';
        $scope.rekap_kelamin    = '';
        $scope.rekap_pekerjaan  = '';
        $scope.rekap_perlengkapan  = '';
        
        $scope.cari = function (argument) {
          $scope.rekap_jamaah     = '';
          $scope.rekap_umur       = '';
          $scope.rekap_kamar      = '';
          $scope.rekap_kelamin    = '';
          $scope.rekap_pekerjaan  = '';
          $scope.rekap_perlengkapan  = '';
          

          if (argument.rekap == "1") {
            $http({
              method : "GET",
              url    : ADMIN_URL+'api/jamaah/rekap_cari?id_jadwal='+argument.id_jadwal
            }).then(function mySucces(response) {
              $scope.rekap_jamaah = response.data;
            });

            $http.get(ADMIN_URL+'api/jamaah/analitik_rekap?tanggal='+argument.id_jadwal).success(function (res) {
                  // angular.forEach(response.pending, function(value, key) {
                  //   this.push(value.tanggal);
                  // }, tanggal);

                  console.log(res.list);
                    $scope.chartOptions = {
                      chart: {
                          type: 'pie'
                        },
                        title: {
                            text: 'Analitik Rekap '+res.tanggal
                        },

                        series: [{
                                name: 'Jamaah',
                                colorByPoint: true,
                                data: res.list
                              }],
                            };
            });

          }else if(argument.rekap == "2"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/jamaah/rekap_cari?id_jadwal="+argument.id_jadwal
            }).then(function mySucces(response){
              $scope.rekap_kamar = response.data;
            });

            $http.get(ADMIN_URL+'api/jamaah/analitik_kamar?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Kamar '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }else if(argument.rekap == "3"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/jamaah/rekap_cari?id_jadwal="+argument.id_jadwal+"&field=umur&sort=desc"
            }).then(function mySucces(response){
              $scope.rekap_umur = response.data;
            });

            $http.get(ADMIN_URL+'api/jamaah/analitik_umur?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Umur '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }else if(argument.rekap == "4"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/jamaah/rekap_cari?id_jadwal="+argument.id_jadwal
            }).then(function mySucces(response){
              $scope.rekap_kelamin = response.data;
            });

            $http.get(ADMIN_URL+'api/jamaah/analitik_kelamin?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Jenis Kelamin '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }else if(argument.rekap == "5"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/jamaah/rekap_cari?id_jadwal="+argument.id_jadwal
            }).then(function mySucces(response){
              $scope.rekap_pekerjaan = response.data;
            });

            $http.get(ADMIN_URL+'api/jamaah/analitik_pekerjaan?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Jenis Pekerjaan '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }else if(argument.rekap == "6"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/jamaah/rekap_perlengkapan?id_jadwal="+argument.id_jadwal
            }).then(function mySucces(response){
              $scope.rekap_perlengkapan = response.data;
            });

            $http.get(ADMIN_URL+'api/jamaah/analitik_perlengkapan?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Jenis Perlengkapan '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }
          // $http({
          //   method : "GET",
          //   url    : ADMIN_URL+'api/umrah_sistem/rekap_cari'
          // }).then(function mySucces(response) {
          //   $scope.rekap = response.data;
          // });

        }


    }]);


AppController.controller('MenuController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
   var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

              $http({
                method : "GET",
                url    : ADMIN_URL+'menu/get_menu'
              }).then(function mySucces(response) {
                console.log(response.data);
                $scope.menu = response.data;
              });

              $scope.cari = function(argument) {
                  $http({
                  method : "GET",
                  url    : ADMIN_URL+'menu/cari_menu?cari='+argument
                }).then(function mySucces(response) {
                  $scope.menu = response.data;
                });
              }
    }]);


AppController.controller('ReportController',['$window','$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($window,$scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

        $http({
        method : "GET",
        url    : ADMIN_URL+'api/jamaah/rekap'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });

        $scope.print_toexcle = function(a){
          $window.open(ADMIN_URL+'v/exclemanifest?tanggal=', '_blank');
        }
        $scope.cari = function (argument) {
          // $window.open(ADMIN_URL+'v/reportmanifest?tanggal='+argument.id_jadwal, '_blank');
          $http({
            method : "GET",
            url    : ADMIN_URL+'v/exclemanifest?tanggal='+argument.id_jadwal
            }).then(function mySucces(response) {
            $window.open(ADMIN_URL+'reprotexcle/'+response.data, '_blank');
            });
        }


    }]);
AppController.controller('RekapAbsenController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

        // $http({
        // method : "GET",
        // url    : ADMIN_URL+'api/jamaah/rekap'
        // }).then(function mySucces(response) {
        //     $scope.jadwal = response.data.jadwal;
        // });

        $scope.cari = function (argument) {
              $http({
                method : "POST",
                url    : ADMIN_URL+'api/absen/rekap_cari/',
                data   : argument
              }).then(function mySucces(response) {
                console.log(response);
                // $scope.rekap_jamaah = response.data;
              });
        }

        $scope.cari = function (argument) {
              $http({
                method : "GET",
                url    : ADMIN_URL+'api/absen/rekap_cari?tahun='+argument.tahun+'&bulan='+argument.bulan
              }).then(function mySucces(response) {
                $scope.rekapabsen = response.data.result;
              });


              $http({
                method : "GET",
                url    : ADMIN_URL+'api/absen/rekap_analitik?tahun='+argument.tahun+'&bulan='+argument.bulan
              }).then(function mySucces(response) {
                $scope.chartRekapabsenOptions = {
                      title: {
                          text: 'Analitik Rekap Fee Absen'
                      },
                      xAxis: {
                          categories: response.data.rekap_analitik.nama,
                      },
                      series: [{
                          name:'Fee',
                          data: response.data.rekap_analitik.fee
                      }],
                  };
              

              });

        }


    }]);

AppController.controller('NotifController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
   var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

              $http({
                method : "GET",
                url    : ADMIN_URL+'api/home/notif'
              }).then(function mySucces(response) {
                console.log(response.data.tanggalpassport);
                $scope.ultah            = response.data.alumni;
                $scope.passport         = response.data.passport;
                $scope.log              = response.data.log;
              });

    }]);

AppController.controller('PilihprovinsiController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
   var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

              $http({
                  method: 'GET',
                  url: ADMIN_URL+'api/system/get_kabupaten?id=',
                }).then(function successCallback(response) {
                    $scope.loop_kabupaten = response.data;
                }, function errorCallback(response) {
                      
                });


          $scope.pilihprovinsi = function (argument) {
              $http({
                  method: 'GET',
                  url: ADMIN_URL+'api/system/get_kabupaten?id='+argument,
                }).then(function successCallback(response) {
                    $scope.loop_kabupaten = response.data;
                }, function errorCallback(response) {
                      
                });
          }

    }]);

AppController.controller('DetailPencarian',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
   var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;
   $scope.alamat = $routeParams.alamat;

              $http({
                method : "GET",
                url    : ADMIN_URL+'api/pencarian/detail_alamat?alamat='+$routeParams.alamat
              }).then(function mySucces(response) {
                console.log(response.data);
                $scope.data = response.data;
              });

    }]);

AppController.controller('DataAlumni',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
    var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;
        $http({
        method : "GET",
        url    : ADMIN_URL+'api/jamaah/tanggal_alumni'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });
  }]);

AppController.controller('DataJamaah',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
        $http({
        method : "GET",
        url    : ADMIN_URL+'api/system/tanggal_jamaah'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });
  }]);


AppController.controller('DataDuta',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
        $http({
        method : "GET",
        url    : ADMIN_URL+'api/duta/tanggal_duta'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });
  }]);

AppController.controller('InvoiceCostume',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
    
    $scope.yangbertanggungjawab = function(id,id_ganti){
        $http({
          method: 'post',
          data   : {id_pembayaran : id , id_ganti : id_ganti},
          url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_changebertangungjawab'
        }).then(function successCallback(response) {
              toasty.success({
                title: "Update Sukses",
                msg: response.data.messages,
              });
              $route.reload();
          }, function errorCallback(response) {

          });
    }

    $scope.tambah_data_invoice = function(argument){
         $http({
          method: 'post',
          data   : argument,
          url: ADMIN_URL+'api/' + $routeParams.target+'/'+$routeParams.kondisi+'_add'
        }).then(function successCallback(response) {
               window.location.href = response.data.url;
          }, function errorCallback(response) {

          });
    }


  }]);

AppController.controller('RekapFeeDivisiController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
  var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

    $http.get(ADMIN_URL+'api/lainnya/rekap_fee_divisi').success(function (res) {
      console.log(res.list);
    $scope.chartOptions = {
      chart: {
      type: 'column'
    },
    title: {
        text: 'Analitik Fee Divisi'
    },
    xAxis: {
        type: 'category',
        labels: {
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Population (millions)'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Population in 2008:  <b>Rp. {point.y} </b>'
    },
    series: [{
        name: 'Population',
        data: res.list,
        dataLabels: {
            enabled: true,
            format: 'RP. {point.y}', // one decimal
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
    };
    });
  }]);

AppController.controller('MultipleSelect',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
        var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;
        console.log($routeParams.id);
        
        $http({
        method : "GET",
        url    : ADMIN_URL+'api/umrah_sistem/get_destinasi'
        }).then(function mySucces(response) {
            $scope.people = response.data;
        });

        $http({
        method : "GET",
        url    : ADMIN_URL+'api/umrah_sistem/get_destinasi_by_id?id='+$routeParams.id
        }).then(function mySucces(response) {
            $scope.value.destinasi = response.data;
        });

    }]);

AppController.controller('PrintController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){

  var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

        $http({
        method : "GET",
        url    : ADMIN_URL+'api/cetak/cetak'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });


  $scope.id_card        = '';

  $scope.cari = function (argument) {
    $scope.id_card        = '';
  
    $http({
      method : "GET",
      url    : ADMIN_URL+'api/cetak/cari?id_jadwal='+argument.id_jadwal
    }).then(function mySucces(response) {
      $scope.rekap_jamaah = '';
      $scope.id_card      = response.data;
    });


  };

  }]);


AppController.controller('RekapDuaController',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

        $http({
        method : "GET",
        url    : ADMIN_URL+'api/rekap/rekap'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });

        $scope.rekap_jamaah        = '';
        $scope.rekap_umur          = '';
        $scope.rekap_kamar         = '';
        $scope.rekap_kelamin       = '';
        $scope.rekap_pekerjaan     = '';
        $scope.rekap_perlengkapan  = '';
        
        $scope.cari = function (argument) {
          $scope.rekap_jamaah     = '';
          $scope.rekap_umur       = '';
          $scope.rekap_kamar      = '';
          $scope.rekap_kelamin    = '';
          $scope.rekap_pekerjaan  = '';
          $scope.rekap_perlengkapan  = '';
          

          if (argument.rekap == "1") {
            $http({
              method : "GET",
              url    : ADMIN_URL+'api/rekap/rekap_cari?id_jadwal='+argument.id_jadwal
            }).then(function mySucces(response) {
              $scope.rekap_jamaah = response.data;
            });

            $http.get(ADMIN_URL+'api/rekap/analitik_rekap?tanggal='+argument.id_jadwal).success(function (res) {
                    $scope.chartOptions = {
                      chart: {
                          type: 'pie'
                        },
                        title: {
                            text: 'Analitik Rekap2 '+res.tanggal
                        },

                        series: [{
                                name: 'Jamaah',
                                colorByPoint: true,
                                data: res.list
                              }],
                            };
            });

          }else if(argument.rekap == "2"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/rekap/rekap_cari?id_jadwal="+argument.id_jadwal
            }).then(function mySucces(response){
              $scope.rekap_kamar = response.data;
            });

            $http.get(ADMIN_URL+'api/rekap/analitik_kamar?tanggal='+argument.id_jadwal).success(function (res) {
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Kamar '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }else if(argument.rekap == "3"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/rekap/rekap_cari?id_jadwal="+argument.id_jadwal+"&field=umur&sort=desc"
            }).then(function mySucces(response){
              $scope.rekap_umur = response.data;
            });

            $http.get(ADMIN_URL+'api/rekap/analitik_umur?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Umur '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }else if(argument.rekap == "4"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/rekap/rekap_cari?id_jadwal="+argument.id_jadwal
            }).then(function mySucces(response){
              $scope.rekap_kelamin = response.data;
            });

            $http.get(ADMIN_URL+'api/rekap/analitik_kelamin?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Jenis Kelamin '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }else if(argument.rekap == "5"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/rekap/rekap_cari?id_jadwal="+argument.id_jadwal
            }).then(function mySucces(response){
              $scope.rekap_pekerjaan = response.data;
            });

            $http.get(ADMIN_URL+'api/rekap/analitik_pekerjaan?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Jenis Pekerjaan '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }else if(argument.rekap == "6"){
            $http({
              method: "GET",
              url: ADMIN_URL+"api/rekap/rekap_perlengkapan?id_jadwal="+argument.id_jadwal
            }).then(function mySucces(response){
              $scope.rekap_perlengkapan = response.data;
            });

            $http.get(ADMIN_URL+'api/rekap/analitik_perlengkapan?tanggal='+argument.id_jadwal).success(function (res) {
              console.log(res.list);
              $scope.chartOptions = {
                chart: {
                    type: 'pie'
                  },
                  title: {
                      text: 'Analitik Jenis Perlengkapan '+res.tanggal
                  },

                  series: [{
                          name: 'Jamaah',
                          colorByPoint: true,
                          data: res.list
                        }],
              };
            });
          }
        }
    }]);

AppController.controller('ReportDuaController',['$window','$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
  function($window,$scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){

 var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;

        $http({
        method : "GET",
        url    : ADMIN_URL+'api/report/rekap'
        }).then(function mySucces(response) {
            $scope.jadwal = response.data.jadwal;
        });

        $scope.print_toexcle = function(a){
          $window.open(ADMIN_URL+'v/exclemanifest?tanggal=', '_blank');
        }

        $scope.cari = function (argument) {
          // $window.open(ADMIN_URL+'v/reportmanifest?tanggal='+argument.id_jadwal, '_blank');
          $http({
            method : "GET",
            url    : ADMIN_URL+'v/exclemanifest?tanggal='+argument.id_jadwal
            }).then(function mySucces(response) {
            $window.open(ADMIN_URL+'reprotexcle/'+response.data, '_blank');
            });
        }

        $scope.roomlist = function (argument) {
          // $window.open(ADMIN_URL+'v/reportmanifest?tanggal='+argument.id_jadwal, '_blank');
          $http({
            method : "GET",
            url    : ADMIN_URL+'v/excleroomlist?tanggal='+argument.id_jadwal
            }).then(function mySucces(response) {
            $window.open(ADMIN_URL+'reprotexcle/'+response.data, '_blank');
            });
        }

        $scope.tiket = function (argument) {
          // $window.open(ADMIN_URL+'v/reportmanifest?tanggal='+argument.id_jadwal, '_blank');
          $http({
            method : "GET",
            url    : ADMIN_URL+'v/exclemanifest?tanggal='+argument.id_jadwal
            }).then(function mySucces(response) {
            $window.open(ADMIN_URL+'reprotexcle/'+response.data, '_blank');
            });
        }


    }]);



// AppController.controller('DataAlumniV2',['$scope','$http','$location','notifications','$rootScope','$routeParams','$route','toasty',
//   function($scope,$http,$location,notifications,$rootScope,$routeParams,$route,toasty){
//     var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;
//         $http({
//         method : "GET",
//         url    : ADMIN_URL+'api/alumni_v2/paket'
//         }).then(function mySucces(response) {
//             $scope.paket = response.data.jadwal;
//         });
//   }]);

AppController.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});