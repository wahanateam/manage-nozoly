var AppController = angular.module('AppController', []);
AppController.controller('CmsController',['$scope','$confirm','$routeParams','$http','$location','notifications','$filter',
  function($scope,$confirm,$routeParams,$http,$location,notifications,filter){

  $scope.tinymceOptions = {
    onChange: function(e) {
      // put logic here for keypress and cut/paste changes
    },
    inline: false,
    height: 300,
    plugins: [
         "advlist autolink link image lists charmap  preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
         "table contextmenu directionality emoticons paste textcolor  code"
   ],
    skin: 'lightgray',
    theme : 'modern',
    external_filemanager_path:"/filemanager/",
   filemanager_title:"Responsive Filemanager" ,
   external_plugins: {"filemanager" : "/filemanager/plugin.min.js"
    }
  };



    var templateUrl = ADMIN_URL+'api/' + $routeParams.target + "/"+$routeParams.kondisi;
    var directURL   = 'page/' + $routeParams.target + "/"+$routeParams.kondisi;
    var limitpage  = '10';
    var str = $location.url();
    var cariurl = $location.search();
    if(cariurl.id_page != null){
      var id_page_plus ="&id_page="+cariurl.id_page; 
    }else{
      var id_page_plus =""; 
    }
      if(cariurl.cari == null){
       var kitacari = '';
      }else{
       var kitacari = cariurl.cari;
      }
    var parser = parseInt(str.substring(str.lastIndexOf("/") + 1, str.length));
    if(isNaN(parser)){
        $scope.url_pag = 1;
    }else{
        $scope.url_pag = parser;
    }
    $scope.noplus  = ($scope.url_pag-1)*limitpage+1;
    $scope.cari = cariurl.cari;
    $scope.remove_search = function(){
      $location.search('cari', null); 
      $location.path(directURL);
    }
    $scope.dicari = function(iii){
      console.log(iii);
      if (iii == "") {
        var url_kondisi = templateUrl+'?limit='+limitpage+id_page_plus+'&page='+$scope.url_pag+'&cari='+iii;
      }else{
        var url_kondisi = templateUrl+'?limit='+limitpage+id_page_plus+'&page=&cari='+iii;
      }

       $http({
          method: 'GET',
          url: url_kondisi
        }).then(function successCallback(response) {
          $scope.loop = response.data.result;
          $scope.length_pag = [];
            var ii = Math.ceil(response.data.count/limitpage);
            for (i = 1; i <= ii; i++) {
                $scope.length_pag.push({num: i ,
                                        url: ADMIN_URL+'page/'+$routeParams.target+'/'+$routeParams.kondisi+'/hal/'+i+'?cari='+iii+id_page_plus});
            }
          console.log(response.data.count);
          }, function errorCallback(response) {

          });
  }
  if($routeParams.fung == null){
    console.log('view');
        $http({
          method: 'GET',
          url: templateUrl+'?limit='+limitpage+'&page='+$scope.url_pag+id_page_plus+'&cari='+kitacari
        }).then(function successCallback(response) {
          $scope.loop = response.data.result;
          $scope.length_pag = [];
            var ii = Math.ceil(response.data.count/limitpage);
            for (i = 1; i <= ii; i++) {
                $scope.length_pag.push({num: i ,
                                        url: ADMIN_URL+'page/'+$routeParams.target+'/'+$routeParams.kondisi+'/hal/'+i+'?cari='+kitacari+id_page_plus});
            }
          console.log(response.data.count);
          }, function errorCallback(response) {

          });
  }else{
    if($routeParams.id == null){

    }else{
      $http({
          method: 'GET',
          url: templateUrl+'/'+$routeParams.id
        }).then(function successCallback(response) {
            $scope.value = response.data;
            console.log(response.data);
          }, function errorCallback(response) {
            
          });
    }
  }
  
  $scope.load_tambah = function(i = null){
    console.log($routeParams.id);
    if (i == "active") {
      var $plusID = "?id="+$routeParams.id;
    }else{
      var $plusID = "";
    }
    $http({
          method: 'GET',
          url: templateUrl+'_tambahan'+$plusID
        }).then(function successCallback(response) {
            $scope.ii = response.data;
            console.log(response.data);
          }, function errorCallback(response) {
            
          });
  }
  $scope.load_addgallery = function (argument) {
      $http({
          method: 'GET',
          url: templateUrl+'/'+cariurl.id_page
        }).then(function successCallback(response) {
            $scope.value = response.data;
            console.log(response.data);
          }, function errorCallback(response) {
            
          });
  }
  $scope.save = function (argument) {
     tinyMCE.triggerSave();
      var $btn = $("#myButton").button('loading');
      $http({
          method: 'POST',
          url: templateUrl+'_post',
          data: argument
        }).then(function successCallback(response) {
            notifications.showSuccess({
                message: response.data.messages,
                hideDelay: 3500, //ms
                hide: true //bool
             });
                $btn.button('reset');
                if(response.data.url != undefined){
                  console.log('ada url');
                   window.location.href = response.data.url;
                }else{
                   $location.path(directURL);
                }
            }, function errorCallback(response) {
                $btn.button('reset');
                notifications.showError({
                    message: response.data.messages,
                    hideDelay: 3500, //ms
                    hide: true //bool
                 });
          });
  }


  $scope.delete = function(index,id,text = null) {
      if(text == null){
          var txt = 'Apakah anda yakin akan menhapus item ini ?';
      }else{
          var txt = 'Apakah anda yakin akan menghapus '+text+' ?';
      }
      $confirm({text: txt, title: 'Konfirmasi', ok: 'Yes', cancel: 'No'})
        .then(function() {
          //$scope.deletedConfirm = 'Deleted';
            $http({
              method: 'DELETE',
              url: templateUrl+'_delete?id='+id,
            }).then(function successCallback(response) {
                 $scope.loop.splice(index, 1);
                  notifications.showSuccess({
                message: response.data.messages,
                hideDelay: 3500, //ms
                hide: true //bool
             });
              }, function errorCallback(response) {

              });
        });
  };

  $scope.status = function(index,id,status) {
            console.log(index);
            $http({
              method: 'POST',
              url: templateUrl+'_status?id='+id+'&status='+status,
            }).then(function successCallback(response) {
                
              $scope.status = response.data.status;

              notifications.showSuccess({
                message: response.data.messages,
                hideDelay: 3500, //ms
                hide: true //bool
             });
              }, function errorCallback(response) {

              });

  };

  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

  $scope.toggleMin = function() {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  $scope.toggleMin();

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  
  $scope.format = $scope.formats[0];
  $scope.altInputFormats = ['dd.MM.yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }





  }]);


AppController.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

AppController.directive('myPagination', function() {
  return {
    restrict: 'E',
    scope: {
      data: '=length',
      activemenu: '=active'
    },
    templateUrl: ADMIN_URL+'myPagination.html'

  };
});

AppController.directive('tanggaljadwalberangkat', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {      
          element.datepicker({
              language: 'en',
              autoClose: true,
              dateFormat : "yyyy-mm-dd",
              onSelect: function onSelect(fd, date) {
                scope.value = {
                  tanggal : fd
                };
              }
            }); 
    }
  };
});
AppController.directive('tanggaljadwalpulang', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {      
          element.datepicker({
              language: 'en',
              autoClose: true,
              dateFormat : "yyyy-mm-dd",
              onSelect: function onSelect(fd, date) {
                scope.value = {
                  tanggal_pulang : fd
                };
              }
            }); 
    }
  };
});