var VarApproute = angular.module('AppRoute', ['AppController','ngSanitize','ui.select','angular-loading-bar','ngFileUpload','ngRoute','ngNotificationsBar','ui.tinymce', 'ui.bootstrap','angular-preload-image','angular-toasty','ui.sortable']);
VarApproute.config(function ($httpProvider) {
  $httpProvider.defaults.transformRequest = function(data) {
    if (data === undefined)
      return data;

    var fd = new FormData();
    angular.forEach(data, function(value, key) {
      if (value instanceof FileList) {
        if (value.length == 1) {
          fd.append(key, value[0]);
        } else {
          angular.forEach(value, function(file, index) {
            fd.append(key + '_' + index, file);
          });
        }
      } else {
        fd.append(key, value);
      }
    });

    return fd;
  }

  $httpProvider.defaults.headers.post['Content-Type'] = undefined;
});

VarApproute.filter('marge_json', function() {
  return function(input,ii) {
    return ii;
  };
});

VarApproute.config(['$routeProvider','$locationProvider','toastyConfigProvider',
  function($routeProvider,$locationProvider,toastyConfigProvider) {
    
    toastyConfigProvider.setConfig({
      showClose: true,
      clickToClose: true,
      timeout: 5000,
      sound: true,
      html: true,
      shake: false,
      theme: "material"
    });
    
    $routeProvider.
      when('/page/:target', {
        templateUrl: function(url){
          return ADMIN_URL+'router/'+url.target+'/index';
        }
      }).when('/page/:target/:kondisi', {
        templateUrl: function(url){
          return ADMIN_URL+'router/'+url.target+'/'+url.kondisi;
        },controller: 'CmsController'
      }).when('/page/:target/:kondisi/hal/:page', {
        templateUrl: function(url){
          return ADMIN_URL+'router/'+url.target+'/'+url.kondisi;
        },controller: 'CmsController'
      }).when('/page/:target/:kondisi/:fung', {
        templateUrl: function(url){
          return ADMIN_URL+'router/'+url.target+'/'+url.kondisi+'/'+url.fung;
        },controller: 'CmsController'
      }).when('/page/:target/:kondisi/:fung/:id', {
        templateUrl: function(url){
           return ADMIN_URL+'router/'+url.target+'/'+url.kondisi+'/'+url.fung;
        },controller: 'CmsController'
      })
      
      $locationProvider.html5Mode({
                 enabled: true,
                 requireBase: false
          });

  }]);
