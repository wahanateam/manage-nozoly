var compressor = require('node-minify');
new compressor.minify({
  type: 'sqwish',
  fileIn: ['css/dataTables.material.min.css', 'css/jquery.mCustomScrollbar.css', 'css/angular-toasty.min.css', 'css/loading-bar.min.css', 'css/core.css', 'css/icon.css'],
  fileOut: 'css-dist/custom-build.min.css',
  callback: function(err, min){
    console.log(err);
  }
});
/*
new compressor.minify({
  type: 'uglifyjs',
  fileIn: 'js/apps/script.js',
  fileOut: 'js/apps-dist/script.min.js',
  callback: function(err, min){
    console.log(err);
  }
});

new compressor.minify({
  type: 'uglifyjs',
  fileIn: 'js/apps/load.js',
  fileOut: 'js/apps-dist/load.min.js',
  callback: function(err, min){
    console.log(err);
  }
});

new compressor.minify({
  type: 'uglifyjs',
  fileIn: 'js/angularjs/angular-preload-image.min.js',
  fileOut: 'js/apps-dist/angular-preload-image.min.js',
  callback: function(err, min){
    console.log(err);
  }
});*/